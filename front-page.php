<?php 

get_header();

$featured_area = get_page_by_path('headline');
$button_text = get_post_meta($featured_area->ID, 'button_text', true);
$button_link = get_post_meta($featured_area->ID, 'button_link', true);

$args = array(
	'taxonomy'		=> 'pet_types',
	'hide_empty'	=> false,
	'exclude'		=> array(30,33)
);

$pet_type_terms = get_terms($args);

?>

<div class="ybd-sb-header-banner-wrapper">
	<div class="ybd-sb-banner-wrapper" style="background-image: url(<?php echo get_the_post_thumbnail_url($featured_area, 'full'); ?>)"></div>
	<div class="ybd-sb-banner-cta-wrapper">
		<h1><?php echo $featured_area->post_title; ?></h1>
		<p><?php echo $featured_area->post_content; ?></p>
		<a href="<?php echo $button_link; ?>" class="btn ybd-sb-btn-green"><?php echo $button_text; ?></a>
	</div>
</div>

<div class="container ybd-sb-find-container">
	<div class="row">
		<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12" id="ybd-find-pet-col-left">
			<a href="<?php echo home_url() . '/type/dog/' ?>" class="ybd-sb-find-pet-button">
				<img src="<?php echo get_stylesheet_directory_uri() . '/img/dog-icon.png'; ?>" alt="Outline of a dog">
				<span>Find a dog</span>
			</a>
		</div>
		<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12" id="ybd-find-pet-col-middle">
			<a href="<?php echo home_url() . '/type/cat/' ?>" class="ybd-sb-find-pet-button">
				<img src="<?php echo get_stylesheet_directory_uri() . '/img/cat-icon.png'; ?>" alt="Outline of a cat">
				<span>Find a cat</span>
			</a>
		</div>
		<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" id="ybd-find-pet-col-right">
			<a href="#" class="ybd-sb-find-pet-button dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
				<img src="<?php echo get_stylesheet_directory_uri() . '/img/rabbit-icon.png'; ?>" alt="Outline of a rabbit">
				<span>Find other pets <i class="fas fa-chevron-down"></i></span>
			</a>
			<ul class="dropdown-menu">
				<?php foreach ($pet_type_terms as $term) { ?>
					<?php echo '<li><a href="' . home_url() . '/type/' . $term->slug . '">' . $term->name . '</a></li>'; ?>
				<?php } ?>
			</ul>
		</div>
	</div>
</div>

<div id="primary" class="content-area">

	<div class="container">
		<?php get_template_part('partials/featured', 'pets'); ?>
	</div>

	<br /><br />

	<div class="container">
		<div class="row" id="pets-near-me">
			<div class="col-xs-12 text-center">
				<h2 class="ybd-sb-h2">Pets Near <span class="location-text"></span></h2>
			</div>

			<div class="container">
				<div class="row" id="frontpage-query-pets"><!--ajax--></div>
		</div>
	</div>

	<?php get_template_part('partials/css', 'spinner'); ?>

	<br /><br />

	<button type="submit" name="filter" value="search" class="btn ajax-load-more-button ybd-sb-btn-green" style="display:none">See More Pets</button>

	<br />

	<?php  get_template_part('partials/recently', 'viewed-pets'); ?>

	<br /><br />

<div class="row" id="learn-more">
	<div class="col-xs-12 text-center">
		<h2 class="ybd-sb-h2">Learn More</h2>
	</div>
	<div class="col-xs-12">
		<div class="ybd-learn-more-carousel dale-carousel owl-carousel owl-theme">			
			<?php get_template_part('partials/news', 'card'); ?>
		</div>
	</div>
</div>

<br /><br />

</div><!-- #primary -->


<?php get_footer(); ?>