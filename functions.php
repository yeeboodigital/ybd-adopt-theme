<?php
/**
 * BC SPCA Adopt Theme
 *
 * @package WordPress
 * @since 1.0.0
 */

// Include the functions for all the ajax magic
require_once(dirname( __FILE__ ) . '/ajax-functions.php');

// Include the functions for all the remote posts
require_once(dirname( __FILE__ ) . '/remote-functions.php');

add_action('init', 'ybd_theme_activator');

function ybd_theme_activator(){
	// Add Thumbnail Support
	add_theme_support( 'post-thumbnails' );

	// Remove junk in the WP Header
	remove_action('wp_head', 'wp_generator');
	remove_action('wp_head', 'rsd_link');
	remove_action('wp_head', 'wlwmanifest_link');
}

add_action( 'wp_enqueue_scripts', 'ybd_theme_styles' );

function ybd_theme_styles() {
	 
	// HEADER FOOTER COPIED CSS
	wp_enqueue_style( 'ybd-header-footer', get_stylesheet_directory_uri() . '/css/ybd-sb-theme-styles.css', '', wp_get_theme()->get('Version') );
	
	// BOOTSTRAP MULTISELECT
	wp_enqueue_style( 'ybd-multiselect', get_stylesheet_directory_uri() . '/css/bootstrap-multiselect.css', '', wp_get_theme()->get('Version') );

	// MAIN CSS
	wp_enqueue_style( 'main-stylesheet', get_bloginfo( 'stylesheet_directory' ) . '/style.css', '', wp_get_theme()->get('Version') );
}

add_action( 'wp_enqueue_scripts', 'ybd_theme_scripts' );

function ybd_theme_scripts() {
	wp_register_script( 'ybd_font_awesome_kit', get_stylesheet_directory_uri() . '/js/font-awesome-kit.js', null, wp_get_theme()->get('Version') );
	wp_enqueue_script( 'ybd_font_awesome_kit');

	wp_register_script( 'ybd_sb_theme_js', get_stylesheet_directory_uri() . '/js/ybd-sb-header-footer.js', array('ybd_theme_js', 'jquery'), wp_get_theme()->get('Version') );
	wp_enqueue_script( 'ybd_sb_theme_js');

	wp_register_script( 'ybd_sb_bootsrap_multiselect', get_stylesheet_directory_uri() . '/js/bootstrap-multiselect.js', array('ybd_theme_js', 'jquery'), wp_get_theme()->get('Version') );
	wp_enqueue_script( 'ybd_sb_bootsrap_multiselect');

	wp_register_script( 'ybd_theme_js', get_stylesheet_directory_uri() . '/js/ybd-theme-js.js', array('jquery'), wp_get_theme()->get('Version') );

	// For front-end ajax goodness
	wp_localize_script( 'ybd_theme_js', 'profile_ajax', array(
		'ajaxurl' => admin_url( 'admin-ajax.php' ),
		'security' => wp_create_nonce( 'profile_ajax' ),
		'userLocation'	=> array(
			'city'		=> null,
			'province'	=> null,
			'latitude'	=> null,
			'longitude'	=> null,
		)
	));

	wp_enqueue_script( 'ybd_theme_js' );

	global $wp_query;

	$current_user = wp_get_current_user();
	 
	// register the load more script
	wp_register_script( 'ybd_sb_loadOnScroll', get_stylesheet_directory_uri() . '/js/loadOnScroll.js', array( 'jquery' ), wp_get_theme()->get('Version') );
	
	wp_localize_script( 'ybd_sb_loadOnScroll', 'ybd_sb_loadmore_params', array(
		'ajaxurl' 		=> site_url() . '/wp-admin/admin-ajax.php',
		'posts' 		=> json_encode( $wp_query->query_vars ),
		'current_page' 	=> 0,
		'canBeLoaded' 	=> true,
		'current_id'	=> get_the_id(),
		'current_user_id'	=> $current_user->ID,
		'current_user_email'=> $current_user->user_email
	) );
	
	wp_enqueue_script( 'ybd_sb_loadOnScroll' );	
}

add_action('after_setup_theme', 'ybd_remove_admin_bar');

function ybd_remove_admin_bar() {
	if (!current_user_can('administrator') && !is_admin()) {
		show_admin_bar(false);
	}
}

// if ( $GLOBALS['pagenow'] === 'wp-login.php' && ! empty( $_REQUEST['action'] ) && $_REQUEST['action'] === 'register' ) {
if ( $GLOBALS['pagenow'] === 'wp-login.php' ) {
	add_filter('login_redirect', function(){
		return '/profile/';
	});
}

/**
 * Fetch the custom admin ad link and image
 *
 * @return Array an array consisting of the ad_link and ad_image
 */
function ybd_get_admin_ad(){
	$options = get_option('ybd_settings');

	if ( !empty($options['ad_url']) && !empty($options['ad_image'])) {
		return array(
			'ad_link' => $options['ad_url'],
			'ad_image'	=> $options['ad_image']
		);
	}
	return array(
		'ad_link' => '',
		'ad_image'	=> ''
	);
}

/**
 * Create the page title for each section of the site dynamically
 *
 * @return void
 */
function ybd_dynamic_title(){
	$title = 'BC SPCA';
	$sep = ' | ';
	$name = get_bloginfo('name');

	if (is_home() || is_front_page())
		$title = $name . $sep . get_bloginfo('description');

	if (is_single() || is_page())
		$title = 'Pet: ' . wp_title($sep, false, 'right') . $name;

		if (is_page('profile'))
		$title = 'User Profile' . $sep . $name;

	if (is_category())
		$title = single_cat_title('', false) . $sep . $name;

	if (is_day())
		$title = 'Post for the day ' . get_the_date('j F, Y') . $sep . $name;

	if (is_month())
		$title = 'Post for the month ' . get_the_date('F, Y') . $sep . $name;

	if (is_year())
		$title = 'Post for the year ' . get_the_date('Y') . $sep . $name;
		
	return $title;
}

/**
 * Used in the header to provide an image for Facebook and Twitter sharing
 *
 * @return Object an object which represents the first attachment image for a pet
 */
function ybd_get_first_attached_image(){
	$post_id = get_the_ID();
	$media = get_attached_media('image', $post_id);
	if ( !empty( $media ) ) {
		$obj = current($media);
		$attachment = wp_get_attachment_image_src($obj->ID, 'full');
		// ybd_debug_log($attachment);
		return (object) array(
			'url'		=> $attachment[0],
			'width'		=> $attachment[1],
			'height'	=> $attachment[2]
		);
	}
	return (object) array(
		'url'		=> 'https://adopt.spca.bc.ca/wp-content/uploads/2019/10/banner-image.jpg',
		'width'		=> '',
		'height'	=> ''
	);;
}

/**
 * Modify the main query that runs for posts to show pets instead
 *
 * @param {Object} $query
 * @return void
 */
add_action('pre_get_posts', 'ybd_pets_posts_on_front_page', 10, 1);

function ybd_pets_posts_on_front_page($query) {
	if ( !is_admin() && $query->is_main_query() && is_home() ) {
		$query->set( 'post_status', array('publish'));
		$query->set( 'post_type', array('pets') );
		$query->set( 'posts_per_page', 1 );
		$query->set( 'update_post_meta_cache', false );
		$query->set( 'update_post_term_cache', false );
		$query->set( 'no_found_rows', true );
	}
}

/**
 * Modify the taxonomy query to reflect filtered settings (if any)
 *
 * @param {Object} $query	a WP_Query object
 * @return void
 */

function ybd_sb_change_category_pets_args( $query ) {
	if ( ( is_post_type_archive('pets') || is_tax() ) && !is_admin() && $query->is_main_query() ) {
		
		$query->set( 'post_status', array('publish'));
		$query->set( 'posts_per_page', 1 );
		$query->set( 'update_post_meta_cache', false );
		$query->set( 'update_post_term_cache', false );		
	
		// Start the taxonomy queries
		$taxquery = $query->get( 'tax_query' );
		if ( !is_array( $taxquery ) ) {
			$taxquery = array();
		}
		// $taxquery = array('relation' => 'AND');

		if ( isset( $_GET['breed'] ) ) {
			$taxquery[] = array(
				'taxonomy' => 'pet_breed',
				'field' => 'slug',
				'terms' => $_GET['breed']
			);
		}

		if ( isset( $_GET['gender'] ) && $_GET['gender'] !== 'any' ) {
			$taxquery[] = array(
				'taxonomy' => 'pet_sex',
				'field'    => 'slug',
				'terms'    => array( $_GET['gender'] ),
			);
		}

		$query->set( 'tax_query', $taxquery );

		// Setup the complex meta query
		$has_meta_query = false;
		$metaquery = array( 'relation' => 'AND' );

		// Start the filter meta queries
		
		// Age
		if ( isset( $_GET['age'] ) && $_GET['age'] !== 'any' ) {
			$has_meta_query = true;
			$start_weeks = 0;
			$end_weeks = 2000;
			switch($_GET['age']) {
				// 0-1yr
				case 'young':
					$end_weeks = 52;
					break;
				// 1-3yrs
				case 'young-adult':
					$start_weeks = 53;
					$end_weeks = 156;
					break;
				// 3-8yrs
				case 'adult':
					$start_weeks = 157;
					$end_weeks = 416;
					break;
				// 8yrs+
				case 'senior':
					$start_weeks = 417;
					break;						
			}
			
			$metaquery[] = array(
				'key' 		=> '_base_pets_age_weeks',
				'value'    	=> array( $start_weeks, $end_weeks ),
				'type'		=> 'NUMERIC',
				'compare'   => 'BETWEEN'
			);
		}

		// Compatibility
		if ( isset( $_GET['compat'] ) && $_GET['compat'] !== 'any' ) {
			$has_meta_query = true;
			$compat_query = array('relation' => 'AND');
			foreach ( $_GET['compat'] as $compat ) {
				// ybd_debug_log($compat);
				$compat_query[] = array(
					'key' 		=> $compat,
					'value'    	=> 'true',
					'compare'   => '=',
				);
			}
			array_push( $metaquery, $compat_query );
		}

		// Location
		if (isset($_GET['location']) && $_GET['location'] !== 'any') {
			$has_meta_query = true;
			$location_query = array('relation' => 'OR');
			foreach ( $_GET['location'] as $location ) {
				$location_obj = get_page_by_title($location, OBJECT, 'locations');
				$api_id = ybd_get_shelter_id_from_wpid($location_obj->ID);
				$location_query[] = array(
					'key' 		=> '_base_pets_location_id',
					'value'    	=> $api_id,
					'compare'   => '=',
				);
			}
			array_push($metaquery, $location_query);
		}

		// Declawed
		if ( isset( $_GET['declawed'] ) && $_GET['declawed'] !== 'any' ) {
			$has_meta_query = true;
			$declawed_query = array(
				'key' 		=> '_base_pets_declawed',
				'value'    	=> 'Yes',
			);
			array_push( $metaquery, $declawed_query );
		}

		// NOT Bonded
		if ( isset( $_GET['bonded'] ) && $_GET['bonded'] == 'no' ) {
			$has_meta_query = true;
			$not_bonded_query = array(
				'key' 		=> '_base_pets_bonded',
				'value'    	=> 'no',
				'compare'	=> '='
			);
			array_push( $metaquery, $not_bonded_query );
		}

		// YES Bonded
		if ( isset($_GET['bonded']) && $_GET['bonded'] == 'yes' ) {
			$has_meta_query = true;
			$bonded_query = array(
				'key' 		=> '_base_pets_bonded',
				'value'    	=> 'yes',
				'compare'	=> '='
			);
			array_push($metaquery, $bonded_query);
		}

		// Weight
		if ( isset( $_GET['weight'] ) && $_GET['weight'] !== 'any' ) {
			$has_meta_query = true;
			$start_weight = 0;
			$end_weight = 300;
			switch($_GET['weight']) {
				case 'class-1':
					$end_weight = 7;
				break;
				case 'class-2':
					$start_weight = 0;
					$end_weight = 9;
				break;
				case 'class-3':
					$start_weight = 0;
					$end_weight = 11.5;
				break;
				case 'class-4':
					$start_weight = 0;
					$end_weight = 14;
				break;
				case 'class-5':
					$start_weight = 0;
					$end_weight = 20.5;
				break;
				case 'class-6':
					$start_weight = 21;
					$end_weight = 28;
				break;
				case 'class-7':
					$start_weight = 28;
					$end_weight = 45;
				break;
				case 'class-8':
					$start_weight = 45;
				break;
			}
			
			$age_query = array(
				'key' 		=> '_base_pets_weight',
				'value'    	=> array( $start_weight, $end_weight ),
				'type'		=> 'NUMERIC',
				'compare'   => 'BETWEEN'
			);
			array_push( $metaquery, $age_query );
		}

		// Animal ID
		if ( isset( $_GET['animal_id'] ) && $_GET['animal_id'] !== '' ) {
			$has_meta_query = true;
			$compat_query = array(
				'key' 		=> '_base_pets_animal_id',
				'value'    	=> $_GET['animal_id'],
			);
			array_push( $metaquery, $compat_query );
		}

		if ( $has_meta_query ) {
			$query->set( 'meta_query', $metaquery );
		}

		// Name Search
		if ( isset( $_GET['pet_name'] ) && $_GET['pet_name'] !== '' ) {
			$query->set( 'title', $_GET['pet_name'] );
		}
	}
}

add_action( 'pre_get_posts', 'ybd_sb_change_category_pets_args', 10, 1 );

/**
 * Sets up an array of compatibility data for a pet
 *
 * @param Number 	$post_id	A valid pet ID
 * @return Array 	An array of compat data for a pet
 */
function ybd_compatibility_helper($post_id){
	$noCats = get_post_meta( $post_id, '_base_pets_compatibility_no_cats', true );
	$noDogs = get_post_meta( $post_id, '_base_pets_compatibility_no_dogs', true );
	$specialNeeds = get_post_meta( $post_id, '_base_pets_compatibility_special_needs', true );
	$noSmallChildren = get_post_meta( $post_id, '_base_pets_compatibility_no_small_children', true );
	$notCompatWithLivestock = get_post_meta( $post_id, '_base_pets_compatibility_not_compat_livestock', true );
	$notGoodWithStrangers = get_post_meta( $post_id, '_base_pets_compatibility_not_good_with_strangers', true );
	$houseTrained = get_post_meta( $post_id, '_base_pets_compatibility_house_trained', true );
	$okWithCats = get_post_meta( $post_id, '_base_pets_compatibility_ok_with_cats', true );
	$okWithDogs = get_post_meta( $post_id, '_base_pets_compatibility_ok_with_dogs', true );
	$specialFee = get_post_meta( $post_id, '_base_pets_compatibility_special_fee', true );
	$longtermResident = get_post_meta( $post_id, '_base_pets_compatibility_longterm_resident', true );
	$featuredAnimal = get_post_meta( $post_id, '_base_pets_compatibility_featured_animal', true );
	$staffPick = get_post_meta( $post_id, '_base_pets_compatibility_staff_pick', true );
	$livedWithKids = get_post_meta( $post_id, '_base_pets_compatibility_lived_with_kids', true );
	$goodWithKids = get_post_meta( $post_id, '_base_pets_compatibility_good_with_kids', true );
	$indoorOnly = get_post_meta( $post_id, '_base_pets_compatibility_indoor_only', true );
	$indoorOutdoor = get_post_meta( $post_id, '_base_pets_compatibility_indoor_outdoor', true );

	$helper_values = array(
		'no_cats'					=> $noCats,
		'no_dogs'					=> $noDogs,
		'special_needs'				=> $specialNeeds,
		'no_small_children'			=> $noSmallChildren,
		'not_compat_livestock'		=> $notCompatWithLivestock,
		'not_good_with_strangers'	=> $notGoodWithStrangers,
		'house_trained'				=> $houseTrained,
		'ok_with_cats'				=> $okWithCats,
		'ok_with_dogs'				=> $okWithDogs,
		'special_fee'				=> $specialFee,
		'longterm_resident'			=> $longtermResident,
		'featured_animal'			=> $featuredAnimal,
		'staff_pick'				=> $staffPick,
		'lived_with_kids'			=> $livedWithKids,
		'good_with_kids'			=> $goodWithKids,
		'indoor_only'				=> $indoorOnly,
		'indoor_outdoor'			=> $indoorOutdoor,
	);

	return $helper_values;
}

/**
 * Used for custom single pet navigation of pets
 *
 * @param 	Array $all_posts	An array of posts to use to determine which pets are previous or next
 * @return 	Array 				An array with the filtered next and previous pet links
 */
function ybd_get_adjacent_pets($all_posts, $post_id) {

	$len = count($all_posts);
    $np = null;
	$pp = null;
	
    if ($len > 1) {
        for ($i=0; $i < $len; $i++) {
            if ($all_posts[$i]->ID === $post_id) {
                if (array_key_exists($i-1, $all_posts)) {
					$pp = $all_posts[$i-1];
                } else {
                    $new_key = $len-1;
                    $pp = $all_posts[$new_key];

                    while ($pp->ID === $post_id) {
                        $new_key -= 1;
                        $pp = $all_posts[$new_key];
                    }
                }

                if (array_key_exists($i+1, $all_posts)) {
                    $np = $all_posts[$i+1];
                } else {
                    $new_key = 0;
                    $np = $all_posts[$new_key];

                    while ($pp->ID === $post_id) {
                        $new_key += 1;
                        $np = $all_posts[$new_key];
                    }
                }

                break;
            }
        }
	}

    return array('next' => $np, 'prev' => $pp);
}

/**
 * Efficent way to get all pet IDs for a pet type
 *
 * @param string $pet_type
 * @return Array An array of pet ids
 */
function ybd_get_all_pet_ids($pet_type){

	// delete_transient($pet_type . '_pet_ids');
	if ( false === ( $query = get_transient( $pet_type . '_pet_ids' ) ) ) {

		$taxquery = array('relation' => 'AND');

		$type_query = array(
			'taxonomy' => 'pet_types',
			'field' => 'slug',
			'terms' => $pet_type
		);
		
		array_push( $taxquery, $type_query );

		$args = array(
			'post_status'		=> 'publish',		
			'post_type'			=> 'pets',
			'posts_per_page'	=> -1,
			'tax_query'			=> $taxquery,
			'fields'			=> 'ids',
			'no_found_rows'		=> true,
		);

		$query = new WP_Query($args);

		set_transient( $pet_type . '_pet_ids', $query, 2 * HOUR_IN_SECONDS );
	}

	// ybd_debug_log($query);

	// An array of post ids
	return $query->posts;
}

/**
 * Efficent helper to get  total pets available count
 *
 * @return Number	The total number of published pets available
 */
function ybd_get_all_pets_count(){
	// delete_transient('pet_count');
	if ( false === ( $pet_count = get_transient( 'pet_count' ) ) ) {
		$args = array(
			'post_status'		=> 'publish',		
			'post_type'			=> 'pets',
			'posts_per_page'	=> -1,
			'fields'			=> 'ids',
			'no_found_rows'		=> true,
		);
	
		$query = new WP_Query($args);

		$pet_count = $query->post_count;

		set_transient( 'pet_count', $pet_count, 15 * MINUTE_IN_SECONDS );
	}
	return $pet_count;	
}

/**
 * Helper to get the current pet term name for a page view
 *
 * @return String	The name of the current term being viewed
 */
function ybd_current_term_name(){
	$term = 'pets';
	$plural = '';

	global $wp_query;

	if ( $wp_query->post_count != 1 ) {
		$plural = 's';
	}

	if ( !empty($wp_query->query['pet_types']) ) {
		$term = str_replace('-', ' ', $wp_query->query['pet_types']);
	}

	return ucwords($term);
}

function ybd_get_pet_from_animal_id($animal_id){
	$args = array(
		'post_status'		=> 'publish',
		'post_type'			=> 'pets',
		'no_found_rows'		=> true,
		'meta_query' => array(
			array(
				'key' => '_base_pets_animal_id',
				'value' => $animal_id,
				'compare' => '=',
			)
		)
	);
	$query = new WP_Query($args);
	
	// Will return the first found WP Post object
	if ( !empty( $query->posts ) ) {
		return $query->posts[0];
	}

	return false;
}

function ybd_get_location_by_slug( $slug ) {
	$posts = get_posts( array(
		'name' => $slug,
		'posts_per_page' => 1,
		'post_type' => 'locations'
	));

	return $posts[0];
}

function get_animal_type_js_ids($animal_type){
	$shopify_ids = array();
	switch ($animal_type) {
	case 'dog':
		$shopify_ids = array(
			'component'	=>	1573603606294,
			'id'		=>	4346040483936,
		);
		break;
	case 'cat':
		$shopify_ids = array(
			'component'	=>	1573603473471,
			'id'		=>	4346114539616,
		);
		break;
	case 'farm-animal':
		$shopify_ids = array(
			'component'	=>	1573603561878,
			'id'		=>	4346122797152,
		);
		break;
	case 'rabbit':
		$shopify_ids = array(
			'component'	=>	1573603533486,
			'id'		=>	4346130858080,
		);
		break;
	// Critter
	default:
		$shopify_ids = array(
			'component'	=>	1573603500448,
			'id'		=>	4346133971040,
		);
		break;
	}

	return $shopify_ids;
}

function ybd_featured_pets($num = -1){
	$featured_args = array(
		'post_type' 		=> 'pets',
		'post_status'		=> 'publish',
		'order' 			=> 'DESC',
		'posts_per_page' 	=> $num,
		'meta_key'			=> '_base_pets_compatibility_featured_animal',
		'meta_value'		=> 'true'
	);
			
	

	if ( false === ( $featured_query = get_transient( 'featured_query' ) ) ) {
		$featured_query = new WP_Query( $featured_args );
		set_transient( 'featured_query', $featured_query, 4 * HOUR_IN_SECONDS );
	}

	return $featured_query;
}

// Add Page Slug Body Class
function ybd_add_slug_to_body( $classes ) {
	global $post;

	$current_user = wp_get_current_user();

	if ( !empty($current_user) ) {
		$lo_status = get_user_meta($current_user->ID, '_base_user_luminate_is_subscribed', true);
		if ( !empty( $lo_status ) && $lo_status == 'true' ) {
			$classes[] = 'lo-subscribed-true';
		} else {
			$classes[] = 'lo-subscribed-false';
		}
	}

	if ( isset( $post ) ) {
		$classes[] = 'page-' . $post->post_name;
	}

	if ( is_user_logged_in() ) {
		$saved_searches = get_user_meta(get_current_user_id(), '_base_search_group', true);
		if ( !empty($saved_searches)) {
			$classes[] = 'has-saved-searches';
		}
	}

	return $classes;
}

add_action('body_class', 'ybd_add_slug_to_body');

function ybd_home_link(){
	if (is_home() ) {
		echo '<i class="fas fa-home"></i>';
	} else {
		echo '<a href="/"><i class="fas fa-home"></i></a>';
	}
}

function ybd_get_weight_class($class){
	switch ($class) {
		case 'class-1':
			return '< 7 kg';
			break;
		case 'class-2':
			return '< 9 kg';
			break;
		case 'class-3':
			return '< 11.5 kg';
			break;
		case 'class-4':
			return '< 14 kg';
			break;
		case 'class-5':
			return '< 20.5 kg';
			break;
		case 'class-6':
			return '21-28 kg';
			break;
		case 'class-7':
			return '28-45 kg';
			break;
		case 'class-8':
			return '45+ kg';
			break;
	}	
}

function ybd_plural_pet_type($pet_type){
	switch ($pet_type) {
		case 'dog':
			return 'dogs';
			break;
		case 'cat':
			return 'cats';
			break;
		case 'farm-animal':
			return 'farm animals';
			break;
		case 'fish':
			return 'fish';
			break;
		case 'bird-exotic':
			return 'exotic birds';
			break;
		case 'rabbit':
			return 'rabbits';
			break;
		case 'reptile-exotic':
			return 'exotic reptiles';
			break;
		case 'small-animal':
			return 'small animals';
			break;
		case 'small-animal-exotic':
			return 'exotic small animals';
			break;			
		}
}

function ybd_pet_type_breeds($pet_type = null, $show_all = true){
	$args = array(
		'post_type' 		=> 'pets',
		'post_status'		=> 'publish',
		'posts_per_page' 	=> -1,
		'fields'			=> 'ids',
		'no_found_rows'		=> true,
		'tax_query'			=> array(
			array(
			'taxonomy' => 'pet_types',
			'field' => 'slug',
			'terms' => $pet_type
			)
		)
	);
			
	$query = new WP_Query( $args );

	return wp_get_object_terms($query->posts, 'pet_breed');
}

function ybd_clip_text($text = '', $max_length = 155){
	$text = wp_filter_nohtml_kses($text);
	if ( strlen($text) > $max_length ){
		$offset = ( $max_length - 3 ) - strlen($text);
		$text = substr($text, 0, strrpos($text, ' ', $offset)) . '...';
	}
	return $text;
}

function ybd_random_numbers_from_range($number = 1, $range = 10) {
	$random_number_array = range(0, $range);
	shuffle($random_number_array );
	$random_number_array = array_slice($random_number_array ,0, $number);
	return $random_number_array;
}

/**
 * Check the current pet for whether to show a status flag or not, and which one to show
 *
 * @param [type] $post_id
 * @return void
 */
function ybd_get_flag_img($post_id){
	
	$status = get_post_meta( $post_id, '_base_pets_status_id', true );
	$long_term = get_post_meta( $post_id, '_base_pets_compatibility_longterm_resident', true );
	$staff_pick = get_post_meta( $post_id, '_base_pets_compatibility_staff_pick', true );
	$special_needs = get_post_meta( $post_id, '_base_pets_compatibility_special_needs', true );
	$bonded = get_post_meta( $post_id, '_base_pets_bonded_with', true );

	// In Foster
	if ( $status == '92' ) {
		return 'in-foster-flag.png';
	}
	// Long-Term Resident
	if ( $long_term == 'true' ) {
		return 'long-term-flag.png';
	}

	// Staff Pick
	if ( $staff_pick == 'true' ) {
		return 'staff-pick-flag.png';
	}
	
	// Special Needs
	if ( $special_needs == 'true' ) {
		return 'special-needs-flag.png';
	}

	// Bonded Pair
	if ( $bonded == 'yes' ) {
		return 'bonded-flag.png';
	}

	return false;
}

add_action( 'template_redirect', 'ybd_animal_redirect' );

/**
 * Handles redirects for old to urls to pets to thier new links
 *
 * @return void
 */
function ybd_animal_redirect() {

	// If the url is a link that looks like the old site's links
	$profile_id = isset($_REQUEST['profile_id']) ? $_REQUEST['profile_id'] : false;

	// If the old link actually has an animal id (Shelterbuddy ID)
	if ( $profile_id ) {
		$wpid = ybd_get_pet_from_animal_id($profile_id);

		if ( !empty($wpid) ) {
			wp_redirect( '/pets/' . $wpid->ID, 301 );
			exit(); 
		} else {
			wp_redirect( '/pets/404', 301 );
			exit(); 
		}
	}


}

/**
 * Adding options page for customizations in wp admin area
 */

if( function_exists('acf_add_options_page') ) {

    acf_add_options_page(array(
        'page_title' 	=> 'Theme General Settings',
        'menu_title'	=> 'Theme Settings',
        'menu_slug' 	=> 'theme-general-settings',
        'capability'	=> 'edit_posts',
        'redirect'		=> false
    ));

    acf_add_options_sub_page(array(
        'page_title' 	=> 'Theme Header Settings',
        'menu_title'	=> 'Header',
        'parent_slug'	=> 'theme-general-settings',
    ));

    acf_add_options_sub_page(array(
        'page_title' 	=> 'Theme Footer Settings',
        'menu_title'	=> 'Footer',
        'parent_slug'	=> 'theme-general-settings',
    ));

}

function ybd_retro_update_saved_searches(){
	// Get all users
	$users = get_users(array('fields'=> 'ID'));
	// Foreach user
	foreach( $users as $user_id ){
		
		$search_meta = get_user_meta($user_id, '_base_search_group', true );

		if ( !empty( $search_meta ) ) {
			foreach($search_meta as &$search){
				// Set the date the search was created to 90 days ago
				$search['_base_created_date'] = strtotime('-90 days');
			}

			$update = update_user_meta($user_id, '_base_search_group', $search_meta );
		}
	}
}