<div class="modal fade modal-centered" tabindex="-1" role="dialog" aria-labelledby="Account Required Modal" id="account-required-modal" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Account Required</h4>
			</div>
			<div class="modal-body">
				<!-- start filters section -->
				<div class="container-fluid text-center">
					<i class="fas fa-heart"></i>
					<p>An account is required to favourite animals, or save searches and receive search alerts.<p>
					<button type="button" class="btn btn-green account-req-button">Login or Create an Account</button>
				</div>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->