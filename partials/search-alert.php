<?php
	parse_str($_SERVER['QUERY_STRING'], $url_vars);
?>
	<?php if ( !empty($url_vars) && !is_post_type_archive('pets') ) { ?>
		<div class="search-alert">
			<i class="fas fa-times"></i>
			<div class="bell">
				<img class="search-alert-bell" src="<?php echo get_stylesheet_directory_uri() . '/img/alarm.png'; ?>" alt="Search Alert Bell">
				<h3>Get Notified!</h3>
				<p>Get E-mail notifications whenever new pets match this search are added.</p>
				<br />
				<?php if (!is_user_logged_in() ) { ?>
					<button class="white-button trigger-login-register-modal">Log in or Register Now</button>
				<?php } else { ?>
					<button class="white-button" onclick="location.href='/profile/#ybd-saved-searches'">Set Up a Search Alert</button>		
				<?php } ?>
			</div>
		</div>
	<?php } ?>