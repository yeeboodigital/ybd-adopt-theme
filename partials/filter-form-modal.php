<?php
global $wp_query;

$empty_option = '<option value="any">Any</option>';

$gender = array('male', 'female');

$compatibility = array(
	'_base_pets_compatibility_available_from_foster' 	=> 'Available from Foster',
	'_base_pets_compatibility_featured_animal' 			=> 'Featured Pet',
	'_base_pets_compatibility_house_trained' 			=> 'House Trained',
	'_base_pets_compatibility_indoor_only' 				=> 'Indoor Only',
	'_base_pets_compatibility_indoor_outdoor' 			=> 'Indoor/Outdoor',
	'_base_pets_compatibility_lived_with_kids' 			=> 'Lived with Kids',
	'_base_pets_compatibility_longterm_resident' 		=> 'Longterm Resident',
	'_base_pets_compatibility_ok_with_cats' 			=> 'OK with Cats',
	'_base_pets_compatibility_ok_with_dogs' 			=> 'OK with Dogs',
	'_base_pets_compatibility_special_fee' 				=> 'Special Fee',
	'_base_pets_compatibility_special_needs' 			=> 'Special Needs',
	'_base_pets_compatibility_staff_pick' 				=> 'Staff Pick',
);

?>

<form method="get" action="" id="modal-filter-form">
	<div class="form-group">
		<div class="row profile-filter-form">
			<div class="col-xs-12 col-sm-6">
				<label for="type">Pet Type</label>
				<div class="select">
					<select class="form-control" name="type" id="saved-search-type">
						<?php echo $empty_option; ?>
						<?php
						
							$args = array(
								'taxonomy' => 'pet_types',
								'hide_empty' => false,
							);

							$pet_type_terms = get_terms($args);

							foreach ($pet_type_terms as $term) {
								$selected = '';
								echo '<option ' . $selected . ' value="' . $term->slug . '">' . $term->name . '</option>';
							}
						?>
					</select>
				</div>
			</div>

			<div class="col-xs-12 col-sm-6">
				<label for="breed">Breed</label>
				<div class="select">
					<select class="form-control" name="breed[]" id="saved-search-breed" multiple>
					</select>
				</div>
			</div>

			<div class="col-xs-12 col-sm-6">
				<label for="weight">Weight</label>
				<div class="select">
					<select class="form-control" name="weight" id="saved-search-weight">
						<?php echo $empty_option; ?>
						<option value="class-1" <?php echo isset($_GET['weight']) && $_GET['weight'] == 'class-1' ? 'selected="selected"' : ''; ?>>Under 7 kg (15 lbs)</option>
						<option value="class-2" <?php echo isset($_GET['weight']) && $_GET['weight'] == 'class-2' ? 'selected="selected"' : ''; ?>>Under 9 kg (20 lbs)</option>
						<option value="class-3" <?php echo isset($_GET['weight']) && $_GET['weight'] == 'class-3' ? 'selected="selected"' : ''; ?>>Under 11.5 kg (25 lbs)</option>
						<option value="class-4" <?php echo isset($_GET['weight']) && $_GET['weight'] == 'class-4' ? 'selected="selected"' : ''; ?>>Under 14 kg (30 lbs)</option>
						<option value="class-5" <?php echo isset($_GET['weight']) && $_GET['weight'] == 'class-5' ? 'selected="selected"' : ''; ?>>Under 20.5 kg (45 lbs)</option>
						<option value="class-6" <?php echo isset($_GET['weight']) && $_GET['weight'] == 'class-6' ? 'selected="selected"' : ''; ?>>21 - 28 kg (46 - 60 lbs)</option>
						<option value="class-7" <?php echo isset($_GET['weight']) && $_GET['weight'] == 'class-7' ? 'selected="selected"' : ''; ?>>28 - 45 kg (61 - 99 lbs)</option>
						<option value="class-8" <?php echo isset($_GET['weight']) && $_GET['weight'] == 'class-8' ? 'selected="selected"' : ''; ?>>Over 45 kg (100 lbs)</option>
					</select>
				</div>
			</div>

			<div class="col-xs-12 col-sm-6">
				<label for="age">Approximate Age</label>
				<div class="select">
					<select class="form-control" name="age" id="saved-search-age">
						<?php echo $empty_option; ?>
						<option value="young" <?php echo isset($_GET['age']) && $_GET['age'] == 'young' ? 'selected="selected"' : ''; ?>>Young (0 - 1yr)</option>
						<option value="young-adult" <?php echo isset($_GET['age']) && $_GET['age'] == 'young-adult' ? 'selected="selected"' : ''; ?>>Young Adult (1 - 3yrs)</option>
						<option value="adult" <?php echo isset($_GET['age']) && $_GET['age'] == 'adult' ? 'selected="selected"' : ''; ?>>Adult (3 - 8yrs)</option>
						<option value="senior" <?php echo isset($_GET['age']) && $_GET['age'] == 'senior' ? 'selected="selected"' : ''; ?>>Senior (8+ yrs)</option>
					</select>
				</div>
			</div>

			<div class="col-xs-12 col-sm-6">
				<label for="gender">Gender</label>
				<div class="select">
					<select class="form-control" name="gender" id="saved-search-gender">
						<?php echo $empty_option; ?>
						<option value="male" <?php echo isset($_GET['gender']) && $_GET['gender'] == 'male' ? 'selected="selected"' : ''; ?>>Male</option>
						<option value="female" <?php echo isset($_GET['gender']) && $_GET['gender'] == 'female' ? 'selected="selected"' : ''; ?>>Female</option>
					</select>
				</div>
			</div>

			<div class="col-xs-12 col-sm-6">
				<label for="compat">Compatibility</label>
				<div class="select">
					<select class="form-control" name="compat[]" id="saved-search-compat" multiple>
						<?php foreach ($compatibility as $val => $title) { ?>
							<option value="<?php echo $val; ?>" <?php echo isset($_GET['compat']) && $_GET['compat'] == $val ? 'selected="selected"' : ''; ?>><?php echo $title; ?></option>
						<?php } ?>
					</select>
				</div>
			</div>

			<div class="col-xs-12 col-sm-6">
				<label for="location">Location</label>
				<div class="select">
					<select class="form-control" name="location[]" id="saved-search-location" multiple>
						<?php $locations = Ybd_Shelterbuddy_Public::ybd_get_shelters(); ?>
						<?php foreach ($locations as $location) { ?>
							<option value="<?php echo $location->post_title; ?>" <?php echo isset($_GET['location']) && $_GET['location'] == $location->post_title ? 'selected="selected"' : ''; ?>><?php echo $location->post_title; ?></option>
						<?php } ?>
					</select>
				</div>
			</div>

			<div class="col-xs-12 col-sm-6">
				<label for="bonded">Bonded</label>
				<div class="select">
					<select class="form-control" name="bonded" id="saved-search-bonded">
						<?php echo $empty_option; ?>
						<option value="no" <?php echo isset($_GET['bonded']) && $_GET['bonded'] == 'no' ? 'selected="selected"' : ''; ?>>No</option>
						<option value="yes" <?php echo isset($_GET['bonded']) && $_GET['bonded'] == 'yes' ? 'selected="selected"' : ''; ?>>Yes</option>
					</select>
				</div>
			</div>
		</div>
	</div>
</form>