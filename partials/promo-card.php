<?php

/**
 * Promo card partial
 *
 * This file is used to markup the public-facing Promo card
 *
 */

$post = get_random_promo_post();
?>
<?php if ( $post ) { ?>
	
	<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 post-<?php echo $post->id; ?>">
		<div class="ybd-sb-promo-container promo-colour-<?php echo $post->post_meta_fields->promo_color[0]; ?>">
			<a href="<?php echo $post->post_meta_fields->button_url[0]; ?>" target="_blank" style="text-decoration:none">
				<h3><?php echo $post->post_meta_fields->promo_headline[0]; ?></h3>
				<div class="ybd-sb-promo-image-container" style="background-image: url('<?php echo str_ireplace('http://', 'https://', $post->img); ?>');"></div>
				<p><?php echo $post->post_meta_fields->promo_body[0]; ?></p>
				<button class="ybd-sb-card-btn"><?php echo $post->post_meta_fields->button_text[0]; ?></button>
			</a>
		</div>
	</div>

<?php } ?>