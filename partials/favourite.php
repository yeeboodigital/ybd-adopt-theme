<?php

global $post;

$logged_in = is_user_logged_in();

$is_favourite = false;

$user_loved_animals = get_user_meta(get_current_user_id(), 'ybd_user_loves', true);

if ( is_array( $user_loved_animals ) ) {
	if ( in_array($post->ID, $user_loved_animals) ){
		$is_favourite = true;
	}
}

?>

<!-- favourite pet stuff here -->
<div class="ybd-favourite text-right">
	<i class="heart fa-heart <?php if ( !$logged_in || !$is_favourite ) { echo 'far'; } else { echo 'fas'; } ?>" data-postid="<?php echo $post->ID; ?>" data-logged_in="<?php echo $logged_in; ?>"></i>
</div>