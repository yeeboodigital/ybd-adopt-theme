<div class="modal fade modal-centered" tabindex="-1" role="dialog" aria-labelledby="Login Register Modal" id="login-register-modal" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title text-center">Login or Register for an Account</h4>
				</div>
				<div class="modal-body">
					<!-- start filters section -->
					<div class="container-fluid">

						<!-- Nav tabs -->
						<ul class="nav nav-tabs" role="tablist">
							<li role="presentation" class="active"><a href="#login-tab" aria-controls="login-tab" role="tab" data-toggle="tab">Login</a></li>
							<li role="presentation"><a href="#register-tab" aria-controls="register-tab" role="tab" data-toggle="tab">Register</a></li>
						</ul>

						<!-- Tab panes -->
						<div class="tab-content">
							<div role="tabpanel" class="tab-pane active" id="login-tab">
								<br />
								<?php login_with_ajax(array('registration' => 0)); ?>
								<br />
							</div>
							<div role="tabpanel" class="tab-pane" id="register-tab">
								<div class="lwa-divs-only">
									<div class="lwa-register">
										<br />
										<form class="registerform" action="https://local.adoptdev.com/wp-login.php?action=register" method="post">
											<p><strong>Register For This Site</strong></p>         
											<div class="lwa-username">
												<input autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" type="text" name="user_login" id="user_login" placeholder="Username" onfocus="if(this.value == 'Username'){this.value = '';}" onblur="if(this.value == ''){this.value = 'Username'}">   
											</div>
											<div class="lwa-email">
												<input autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" type="text" name="user_email" id="user_email" placeholder="E-mail Address" onfocus="if(this.value == 'E-mail Address'){this.value = '';}" onblur="if(this.value == ''){this.value = 'E-mail Address'}">   
											</div>
											<p class="lwa-submit-button">
												<input type="submit" name="wp-submit" id="wp-submit" class="button-secondary" value="Register" tabindex="100">
												A password will be e-mailed to you.<br>
												<input type="hidden" name="login-with-ajax" value="register">
											</p>
											<p class="small">By signing up for an account you are agreeing to receive occasional updates from the BC SPCA. You can unsubscribe at any time.</p>
											<p class="small"><a href="https://www.spca.bc.ca/about-us/core-policies-standards/privacy-policy/">Privacy Policy</a></p>
										</form>
										<br />
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
</div><!-- /.modal -->