<div class="modal fade modal-centered" tabindex="-1" role="dialog" aria-labelledby="Delete Account Modal" id="delete-account-modal" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Delete my pet search profile</h4>
			</div>
			<div class="modal-body">
				<!-- start filters section -->
				<div class="container-fluid text-center max-width-80">
					<p>An account is required to favourite animals, or save searches and receive animal search alert emails.<p>
					<p>By deleting your account you will no longer be able to favourite animals, or receive animal search alert emails. Please be assured that you will continue to receive occasional emails from us regarding our work. To manage those subscriptions, please visit our <a href="http://support.spca.bc.ca/site/UserLogin?=0&NEXTURL=PageNavigator%2Freg_welcome">subscription centre.</a></p>
					<p>If you continue to delete your profile, <strong>all data associated with your account will be removed, and you will be logged out.</strong></p>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				<button type="button" class="btn btn-primary btn-red delete-account-button">Delete Account</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->