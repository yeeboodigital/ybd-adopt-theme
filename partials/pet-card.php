<?php

/**
 * Pet card partial
 *
 * This file is used to markup the public-facing pet card on the main page
 *
 * @link       http://www.yeeboodigital.com
 * @since      1.0.0
 *
 * @package    Ybd_Shelterbuddy
 * @subpackage Ybd_Shelterbuddy/public/partials
 */

// various variables needed for post
$post_id = get_the_ID();
$animalSex = wp_get_post_terms( $post_id, 'pet_sex' );
$animalBreed = wp_get_post_terms( $post_id, 'pet_breed' );
$animalCity = get_post_meta( $post_id, '_base_pets_location_city', true );
$flag_img = ybd_get_flag_img($post_id);


$animalAgeWeeks = (int) get_post_meta( $post_id, '_base_pets_age_weeks', true );
if ( $animalAgeWeeks < 52 ) {
		$animalAge = (int) round($animalAgeWeeks / 4) . ' months old';
} else if ( $animalAgeWeeks >= 52 && $animalAgeWeeks <= 103 ) {
      $animalAge = '1 year old';
} else {
   $animalAge = (int) round( $animalAgeWeeks / 52 ) . ' years old';
}

// Distance is added if a custom query that adds it is run; It doesn't normally exist in the WP post object
// $distance = $post->distance;

$logged_in = is_user_logged_in();
$is_favourite = false;

if ( $logged_in ) {
	$user_loved_animals = get_user_meta(get_current_user_id(), 'ybd_user_loves', true);
	if ( is_array($user_loved_animals) && in_array($post_id, $user_loved_animals) ){
		$is_favourite = true;
	}
}

?>

	<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 post-<?php echo $post_id; ?>">
		<div class="ybd-sb-pet-card-container">
			<a href="<?php the_permalink(); ?>">
				<div class="ybd-sb-pet-image-container" style="background-image: url('<?php if ( has_post_thumbnail() ) { echo get_the_post_thumbnail_url(null, 'medium'); } else { echo get_stylesheet_directory_uri() . '/img/noimg.jpg'; } ?>">
					<?php if ( $flag_img ) { ?>
						<img src="<?php echo get_stylesheet_directory_uri() . '/img/flags/' . $flag_img; ?>" alt="status flag">
					<?php } ?>
				</div>
				<div class="ybd-sb-pet-details-container">
					
					<?php get_template_part('partials/favourite'); ?>
					
					<h3><?php the_title(); ?></h3>
					
					<p><?php if (!empty($animalSex[0]->name)) echo $animalSex[0]->name; ?> | <?php if (!empty($animalBreed[0]->name)) echo $animalBreed[0]->name ?></p>
					
					<div class="row">
						<!-- distance from you -->
						<?php // if ( !empty($distance) ) { echo '<div class="text-left col-xs-6">' . $distance . 'km from you' . '</div>';} ?>
						<?php echo '<div class="text-left col-xs-7">' . $animalCity . '</div>'; ?>
						
						<!-- <div class="<?php echo ( !empty($distance) ? 'text-right col-xs-4' : 'col-xs-12 text-center'); ?>"> -->
						<div class="text-right col-xs-5">
							<?php echo $animalAge; ?>
						</div>
					</div>
				</div>
			</a>
		</div>
    </div>