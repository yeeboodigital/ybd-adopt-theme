<?php
global $wp_query;

$empty_option = '<option value="any">Any</option>';

$compatibility = array(
	'_base_pets_compatibility_featured_animal' 			=> 'Featured Pet',
	'_base_pets_compatibility_house_trained' 			=> 'House Trained',
	'_base_pets_compatibility_indoor_only' 				=> 'Indoor Only',
	'_base_pets_compatibility_indoor_outdoor' 			=> 'Indoor / Outdoor',
	'_base_pets_compatibility_lived_with_kids' 			=> 'Lived with Kids',
	'_base_pets_compatibility_longterm_resident' 		=> 'Longterm Resident',
	'_base_pets_compatibility_ok_with_cats' 			=> 'OK with Cats',
	'_base_pets_compatibility_ok_with_dogs' 			=> 'OK with Dogs',
	'_base_pets_compatibility_special_fee' 				=> 'Special Fee',
	'_base_pets_compatibility_special_needs' 			=> 'Special Needs',
	'_base_pets_compatibility_staff_pick' 				=> 'Staff Pick',
);

parse_str($_SERVER['QUERY_STRING'], $url_vars);

?>

<form method="get" action="" id="filter-form">
	<div class="form-group">
		<div class="row">
			<div class="col-xs-6 col-md-2">
				<label for="breed">Breed</label>
				<div class="select">
					<select class="form-control" id="select-breed" name="breed[]" multiple <?php if ( is_post_type_archive('pets') ){ echo 'disabled';} ?>>
						<?php
							// Otherwise would fetch ALL several thousand species
							if ( !is_post_type_archive('pets') ){
								$post_ids = ybd_get_all_pet_ids($wp_query->query_vars['pet_types']);
								$pet_breed_terms = wp_get_object_terms($post_ids, 'pet_breed');
								
								foreach ($pet_breed_terms as $term) {
									$selected = '';
									if ( isset($_GET['breed']) ) {
										foreach ( $_GET['breed'] as $value ) {
											if ( $value == $term->slug ) {
												$selected = 'selected="selected"';
											}
										}
									}
									echo '<option ' . $selected . ' value="' . $term->slug . '">' . $term->name . ' (' . $term->count . ')</option>';
								}
							}
						?>
					</select>
				</div>
			</div>

			<div class="col-xs-6 col-md-2">
				<label for="weight">Weight</label>
				<div class="select">
					<select class="form-control" id="select-weight" name="weight">
						<?php echo $empty_option; ?>
						<option value="class-1" <?php echo isset($_GET['weight']) && $_GET['weight'] == 'class-1' ? 'selected="selected"' : ''; ?>>Under 7 kg (15 lbs)</option>
						<option value="class-2" <?php echo isset($_GET['weight']) && $_GET['weight'] == 'class-2' ? 'selected="selected"' : ''; ?>>Under 9 kg (20 lbs)</option>
						<option value="class-3" <?php echo isset($_GET['weight']) && $_GET['weight'] == 'class-3' ? 'selected="selected"' : ''; ?>>Under 11.5 kg (25 lbs)</option>
						<option value="class-4" <?php echo isset($_GET['weight']) && $_GET['weight'] == 'class-4' ? 'selected="selected"' : ''; ?>>Under 14 kg (30 lbs)</option>
						<option value="class-5" <?php echo isset($_GET['weight']) && $_GET['weight'] == 'class-5' ? 'selected="selected"' : ''; ?>>Under 20.5 kg (45 lbs)</option>
						<option value="class-6" <?php echo isset($_GET['weight']) && $_GET['weight'] == 'class-6' ? 'selected="selected"' : ''; ?>>21 - 28 kg (46 - 60 lbs)</option>
						<option value="class-7" <?php echo isset($_GET['weight']) && $_GET['weight'] == 'class-7' ? 'selected="selected"' : ''; ?>>28 - 45 kg (61 - 99 lbs)</option>
						<option value="class-8" <?php echo isset($_GET['weight']) && $_GET['weight'] == 'class-8' ? 'selected="selected"' : ''; ?>>Over 45 kg (100 lbs)</option>
					</select>
				</div>
			</div>

			<div class="col-xs-6 col-md-2">
				<label for="age">Approximate Age</label>
				<div class="select">
					<select class="form-control" id="select-age" name="age">
						<?php echo $empty_option; ?>
						<option value="young" <?php echo isset($_GET['age']) && $_GET['age'] == 'young' ? 'selected="selected"' : ''; ?>>Young (0 - 1yr)</option>
						<option value="young-adult" <?php echo isset($_GET['age']) && $_GET['age'] == 'young-adult' ? 'selected="selected"' : ''; ?>>Young Adult (1 - 3yrs)</option>
						<option value="adult" <?php echo isset($_GET['age']) && $_GET['age'] == 'adult' ? 'selected="selected"' : ''; ?>>Adult (3 - 8yrs)</option>
						<option value="senior" <?php echo isset($_GET['age']) && $_GET['age'] == 'senior' ? 'selected="selected"' : ''; ?>>Senior (8+ yrs)</option>
					</select>
				</div>
			</div>

			<div class="col-xs-6 col-md-2">
				<label for="gender">Gender</label>
				<div class="select">
					<select class="form-control" id="select-gender" name="gender">
						<?php echo $empty_option; ?>
						<option value="male" <?php echo isset($_GET['gender']) && $_GET['gender'] == 'male' ? 'selected="selected"' : ''; ?>>Male</option>
						<option value="female" <?php echo isset($_GET['gender']) && $_GET['gender'] == 'female' ? 'selected="selected"' : ''; ?>>Female</option>
					</select>
				</div>
			</div>

			<div class="col-xs-6 col-md-2">
				<label for="compat">Compatibility</label>
				<div class="select">
					<select class="form-control" id="select-compat" name="compat[]" multiple>
						<?php 
							foreach ( $compatibility as $val => $title ) {
								$selected = '';
								if ( isset($_GET['compat']) ) {
									foreach ( $_GET['compat'] as $value ) {
										if ( $value == $val ) {
											$selected = 'selected="selected"';
										}
									}
								}
								echo '<option ' . $selected . ' value="' . $val . '">' . $title . '</option>';
							} 
						?>
					</select>
				</div>
			</div>
			<div class="col-xs-6 col-md-2">
				<label for="location">Location</label>
				<div class="select">
					<select class="form-control" id="select-location" name="location[]" multiple>
					<?php $locations = Ybd_Shelterbuddy_Public::ybd_get_shelters(); ?>
						<?php foreach ( $locations as $location ) {
							$selected = '';
							if ( isset($_GET['location']) ) {
								foreach ( $_GET['location'] as $value ) {
									if ( $value == $location->post_title ) {
										$selected = 'selected="selected"';
									}
								}
							}
							echo '<option ' . $selected . ' value="' . $location->post_title . '">' . $location->post_title . '</option>';
						} ?>
					</select>
				</div>
			</div>
		</div>
		
		<div class="row more-filters">
			<div class="col-xs-6 col-md-2 col-md-offset-3">
				<label for="bonded">Bonded</label>
				<div class="select">
					<select class="form-control" id="select-bonded" name="bonded">
						<?php echo $empty_option; ?>
						<option value="no" <?php echo isset($_GET['bonded']) && $_GET['bonded'] == 'no' ? 'selected="selected"' : ''; ?>>No</option>
						<option value="yes" <?php echo isset($_GET['bonded']) && $_GET['bonded'] == 'yes' ? 'selected="selected"' : ''; ?>>Yes</option>
					</select>
				</div>
			</div>
			<div class="col-xs-6 col-md-2">
				<label for="pet_name">Name</label>
				<input class="form-control" type="text" id="select-pet_name" name="pet_name" value="<?php echo isset($_GET['pet_name']) ? $_GET['pet_name'] : ''; ?>" />
			</div>

			<div class="col-xs-6 col-md-2">
				<label for="animal_id">Animal ID</label>
				<input class="form-control" type="text" id="select-animal_id" name="animal_id" value="<?php echo isset($_GET['animal_id']) ? $_GET['animal_id'] : ''; ?>" />
			</div>
		</div>
		<br />
		<?php if (!is_page('profile')) { ?>
			<div class="row action-buttons">
				<div class="col-xs-12 text-center">
					<button type="submit" name="filter" value="search" class="btn btn-default">Apply Filters</button>
					<?php if (is_user_logged_in()) {
							$context_class = ' trigger-save-search';
						} else {
							$context_class = ' trigger-account-modal';
						} ?>
					<button type="submit" name="search-alert" value="" class="btn btn-default search-alert-button <?php echo $context_class; ?>" data-toggle="popover" data-placement="top" title="Email Search Alerts <i class='fa fa-close'></i>" data-content="Save a search like this to your profile, and receive email alerts to your inbox when new pets that match are added."><span class="bell"></span></button>
					<br />
					<a href="#" class="reset-filters small">Reset Filters</a> &nbsp; &nbsp; <a href="#" class="filters-toggle small" data-more-text="More Filters" data-less-text="Less Filters">More Filters</a>
				</div>
			</div>
		<?php } ?>
	</div>
</form>

<?php
?>