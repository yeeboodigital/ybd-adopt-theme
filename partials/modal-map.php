<div class="modal fade modal-centered" tabindex="-1" role="dialog" aria-labelledby="Map Edit Modal" id="map-edit-modal" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Edit your location</h4>
			</div>
			<div class="modal-body">
				<!-- start filters section -->
				<div class="container-fluid">
					<div class="postal-input">
						<form>
							<div class="form-group">
								<input type="text" class="form-control" id="map-postal-code" placeholder="Postal Code or City" maxlength="100">
								<button type="button" class="btn btn-green update-location">Set Location</button>
							</div>
						</form>
					</div>
					<div class="col-xs-12">
						<div class="location-map" id="location-map">
							<div style="width: 600px; height: 400px;" id="map_canvas"></div>
						</div>
					</div>
				</div>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->