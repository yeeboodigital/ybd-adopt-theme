<div class="modal fade modal-centered" tabindex="-1" role="dialog" aria-labelledby="Search Alert Modal" id="saved-search-modal" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Create an Email Alert</h4>
			</div>
			<div class="modal-body">
				<!-- start filters section -->
				<div class="container-fluid">
					<p class="text-center">Create an email alert to receive notifications when animals meeting your specified criteria come into our shelters. These search parameters will also be saved to your profile as a short cut for searching. For the best search experience, we recommend using as FEW optional fields as possible as the more data entered into the search criteria, the less results you will receive.</p>
					<?php get_template_part('partials/filter', 'form-modal'); ?>
					<p class="text-center">By creating an Email Alert you will receive an email when we find animals that match your search parameters. Check your email inbox to confirm this subscription.</p>
					<p class="text-center"><a href="https://spca.bc.ca/about-us/core-policies-standards/privacy-policy/">Privacy Policy</a></p>
					<br />
					<p class="text-center"><button type="button" class="btn btn-green">Save New Email Alert</button></p>
				</div>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->