<?php

/**
 * Medical Emergency card partial
 *
 * This file is used to markup the public-facing Medical Emergency card
 *
 */

// various variables needed for post

$post = get_random_medical_post();

if ( $post ) {

	$desc = get_post_meta( $post->ID, '_medicalEmergency_description', true );
	$img = get_post_meta( $post->ID, '_medicalEmergency_img_url', true );
	$goal = get_post_meta( $post->ID, '_medicalEmergency_goal', true );
	$raised = get_post_meta( $post->ID, '_medicalEmergency_raised', true );
	$trURL = get_post_meta( $post->ID, '_medicalEmergency_tr_url', true );
	$donate = get_post_meta( $post->ID, '_medicalEmergency_donate_url', true );
} else {
	return;
}

?>

<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 post-<?php echo $post->ID; ?>">
	<div class="ybd-sb-medical-emergency-container">
		<div class="ybd-sb-medical-emergency-banner">
			Medical Emergency <img src="<?php echo get_stylesheet_directory_uri() . '/img/medical-emergency-logo.png'; ?>" alt="Medical Emergency Logo">
		</div>
		<div class="ybd-sb-medical-emergency-image-container" style="background-image: url('<?php echo str_replace('https', 'http', $img); ?>');">
		<!-- background pet image -->
		</div>
		<div class="ybd-sb-medical-emergency-details">
			<h3><?php the_title(); ?></h3>
			<p><?php echo $desc; ?> <a href="<?php echo $trURL; ?>">read more</a></p>
			<div class="row">
				<div class="col-xs-12">
					<div class="therm-outer">
						<div class="therm-inner" style="width: <?php echo ( $raised / $goal ) * 100 . '%'; ?>"></div>
					</div>
				</div>
			</div>
			<div class="row ybd-sb-medical-emergency-amts">
				<div class="col-xs-6">
					<p>Goal: <?php echo '$' . $goal; ?></p>
				</div>
				<div class="col-xs-6">
					<p class="text-right">Raised: <?php echo '$' . $raised; ?></p>
				</div>
			</div>
		</div>
		<a class="ybd-sb-medical-emergency-donate" href="<?php echo $donate; ?>">DONATE NOW</a>
	</div>
</div>