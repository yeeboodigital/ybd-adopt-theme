
<div class="container recently-viewed" style="display:none">
	<br /><br />

	<div class="row" id="recently-viewed-pets">
		<div class="col-xs-12 text-center">
			<h2 class="ybd-sb-h2">Recently Viewed Pets</h2>
		</div>
		<div class="col-xs-12">
			<div class="ybd-recent-carousel dale-carousel owl-carousel owl-theme">
				<!-- ajax filled -->
			</div>
		</div>
	</div>
</div>