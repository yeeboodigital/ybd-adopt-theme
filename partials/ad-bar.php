<?php $ad_link = ybd_get_admin_ad(); ?>

<?php if ( !empty($ad_link['ad_image']) ) { ?>
	<div class="container">
		<div class="row">
			<div class="col-xs-12 text-center">
				<a href="<?php echo $ad_link['ad_link']; ?>"><img src="<?php echo $ad_link['ad_image']; ?>" alt="advertisement" style="max-width:96%" /></a>
			</div>
		</div>
	</div>

	<br /><br />

<?php } ?>