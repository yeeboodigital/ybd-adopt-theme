<?php

/**
 * News and Events
 *
 * This file is used to markup the public-facing News card
 *
 */

	$posts = get_news_events_posts();
?>

<?php foreach($posts as $post) { ?>
	<?php if ($post->type == 'event' ) { ?>
		<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 post-<?php echo $post->id; ?> ">
		<div class="ybd-sb-news-container news-colour-blue">
			<a href="<?php echo $post->link; ?>">
				<div class="ybd-sb-news-inner">
					<div class="ybd-sb-news-image-container" style="background-image: url('<?php echo $post->featured_image; ?>');"></div>
					<h3><?php echo $post->title->rendered; ?></h3>
					<p><?php echo 'Events'; ?></p>
				</div>
			</a>
		</div>
	</div>
	<?php } else { ?>
		<!-- Set the post colour to match the main website's use of red for Ways to help -->
		<?php 
			$cat = $post->category_name;
			$colour = $post->colour;
			if ( $cat == 'Ways to help' ) {
				$colour = 'red';
			}
		?>
		<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 post-<?php echo $post->id; ?> ">
			<div class="ybd-sb-news-container news-colour-<?php echo $colour; ?>">
				<a href="<?php echo $post->link; ?>">
					<div class="ybd-sb-news-inner">
						<div class="ybd-sb-news-image-container" style="background-image: url('<?php echo $post->featured_image; ?>');"></div>
						<h3><?php echo $post->title->rendered; ?></h3>
						<p><?php echo $cat; ?></p>
					</div>
				</a>
			</div>
		</div>	
	<?php } ?>
<?php } ?>