<?php

/**
 * FAQ card partial
 *
 * This file is used to markup the public-facing FAQ card
 *
 */
	// Pass the current pet type if we have it for filtering of FAQs
	global $post;

	$type = null;
	
	$terms = wp_get_post_terms($post->ID, 'pet_types');

	if ( !empty( $terms[0]->slug ) ) {
		$type = $terms[0]->slug;
	}

	$faq_post = get_random_faq_post($terms);

?>

<?php if ( $faq_post ) { ?>
	
	<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 post-<?php echo $faq_post->id; ?>">
		<div class="ybd-sb-faq-container promo-colour-<?php echo $faq_post->promo_color; ?>">
			<a href="<?php echo $faq_post->link; ?>" target="_blank" style="text-decoration:none">
				<h3><?php echo ybd_clip_text($faq_post->title->rendered, 60); ?></h3>
				<p><?php echo ybd_clip_text($faq_post->content->rendered, 150 ); ?></p>
				<button class="ybd-sb-card-btn"><?php echo 'Read FAQ'; ?></button>
			</a>
		</div>
	</div>

<?php } ?>