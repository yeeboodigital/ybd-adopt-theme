<?php $featured_query = ybd_featured_pets(8); if ( $featured_query->have_posts() ) { ?>
	<br />
	<div class="container featured-pets">
		<div class="row" id="featured-pets">
			<div class="col-xs-12 text-center">
				<h2 class="ybd-sb-h2">Featured Pets</h2>
			</div>
			<div class="col-xs-12">
				<div class="ybd-featured-carousel dale-carousel owl-carousel owl-theme">
					<?php while ( $featured_query->have_posts() ) : $featured_query->the_post();
					get_template_part('partials/pet', 'card');
					endwhile;
					wp_reset_postdata();
				?>
				</div>
			</div>
		</div>
	</div>
<?php } ?>
