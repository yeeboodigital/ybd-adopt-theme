<div class="ybd-sb-loc-fav-bar">
	<div class="container">
		<div class="row">

			<?php if (!is_singular('pets') && !is_page('profile')) { ?>

				<div class="col-xs-1 col-sm-1 col-md-1">
					<?php ybd_home_link(); ?> &nbsp;|&nbsp;
					<div class="dropdown favbar-toggle inline">
						<button class="btn dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
							<span class="text"><?php global $wp_query; echo ybd_current_term_name() . ' (' . $wp_query->found_posts . ')'; ?></span>
							<span class="caret"></span>
						</button>
						<ul class="dropdown-menu" aria-labelledby="dropdownMenu2">
							<li class="dropdown-header">Pet Type:</li>
							<li><a href="/pets/">All Pets (<?php echo ybd_get_all_pets_count(); ?>)</a></li>
							<?php $terms = get_terms(array('taxonomy' => 'pet_types', 'hide_empty' => 0)); ?>
							<?php foreach ($terms as $term) { ?>
								<li><a href="/type/<?php echo $term->slug; ?>/"><?php echo $term->name . ' (' . $term->count . ')' ?></a></li>
							<?php } ?>
						</ul>
					</div>
				</div>

				<div class="col-xs-2 col-sm-3 col-md-2">
					<p class="inline">
						<a href="#" class="edit-location" data-toggle="modal" data-target="#map-edit-modal">
							<!-- javascript text -->
						</a>
					</p>
				</div>

				<div class="mobile-hide col-sm-3 col-md-5">
					<?php parse_str($_SERVER['QUERY_STRING'], $url_vars); ?>
					<?php if (!empty($url_vars)) { ?>
						<div class="ybd-favbar-carousel dale-carousel owl-carousel owl-theme">
							<?php foreach ($url_vars as $key => $value) { ?>
								<?php if ( $value !== 'any' && ( $key == 'gender' || $key == 'age' ) ) { ?>
									<div class="chip" data-key="<?php echo $key; ?>">
										<?php echo ucwords(str_replace( array('-','_'), ' ', $value ) ); ?>
										<i class="fas fa-times"></i>
									</div>
								<?php } else if ($key == 'breed') { ?>
									<?php foreach ( $value as $breed_key => $breed_val ) { ?>
										<div class="chip multi" data-name="select-breed" data-value="<?php echo $breed_val; ?>">
											<?php echo ucwords( str_replace( '-', ' ', $breed_val ) ) ?>
											<i class="fas fa-times"></i>
										</div>
									<?php } ?>
								<?php } else if ($key == 'location') { ?>
									<?php foreach ( $value as $key => $location_val ) { ?>
										<div class="chip multi" data-name="select-location" data-value="<?php echo $location_val; ?>">
											<?php echo ucwords( str_replace( '-', ' ', $location_val ) ); ?>
											<i class="fas fa-times"></i>
										</div>
									<?php } ?>
								<?php } else if ($key == 'compat') { ?>
									<?php foreach ( $value as $compat_key => $compat_val ) { ?>
										<div class="chip multi" data-name="select-compat" data-value="<?php echo $compat_val; ?>">
											<?php echo ucwords( str_replace(array('_base_pets_compatibility_','-','_'), ' ', $compat_val) ); ?>
											<i class="fas fa-times"></i>
										</div>
									<?php } ?>
								<?php } else if ($key == 'bonded' && $value != 'any') { ?>
									<div class="chip" data-key="<?php echo $key; ?>">
										<?php echo ucwords($key . ': ' . $value); ?>
										<i class="fas fa-times"></i>
									</div>
								<?php } else if ($key == 'weight' && $value != 'any') { ?>
									<div class="chip" data-key="<?php echo $key; ?>">
										<?php echo ybd_get_weight_class($value); ?>
										<i class="fas fa-times"></i>
									</div>
								<?php } ?>
							<?php } ?>
						</div>
					<?php } ?>
				</div>

				<div class="col-xs-4 col-sm-2 col-md-2 pull-right">
					<div class="fav-list text-right">
						<?php if (!is_singular('pets') && !is_page('profile') && !is_home() && !is_404()) { ?>
							<div class="dropdown sort-toggle inline">
								<button class="btn dropdown-toggle" type="button" id="dropdownMenu3" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
									<span class="text">Sort</span>
									<span class="caret"></span>
								</button>
								<ul class="dropdown-menu" aria-labelledby="dropdownMenu3">
									<li class="dropdown-header">Sort By</li>
									<li class="selected" data-sortby="distance"><a href="#">Distance - nearest</a></li>
									<li data-sortby="time-newest"><a href="#">Time in care - newest</a></li>
									<li data-sortby="time-longest"><a href="#">Time in care - longest</a></li>
									<li data-sortby="age-youngest"><a href="#">Pet age - youngest</a></li>
									<li data-sortby="age-oldest"><a href="#">Pet age - oldest</a></li>
									<li data-sortby="weight-smallest"><a href="#">Pet weight - smallest</a></li>
									<li data-sortby="weight-largest"><a href="#">Pet weight - largest</a></li>
								</ul>
							</div>
						<?php } ?>
						<p class="inline text-right">
							<?php if (is_user_logged_in() && !is_page('profile')) { ?>
								<a href="/profile/"><i class="fas fa-heart"></i></a> <div class="mobile-hide"><?php Ybd_Shelterbuddy_Public::ybd_favourites_link(); ?></div>
							<?php } elseif (is_user_logged_in() && is_page('profile')) { ?>
								<a href="<?php echo wp_logout_url('/profile/'); ?>">Logout</a>
							<?php } elseif (!is_user_logged_in() && !is_page('profile')) { ?>
								<a class="trigger-login-register-modal" href="<?php echo home_url() . '/profile/' ?>">Login</a>
							<?php } ?>
						</p>
					</div>
				</div>
			<?php } ?>
		</div>
	</div>
</div>