<div class="ybd-sb-loc-fav-bar">
	<div class="container">
		<div class="row">

			<div class="col-xs-5 col-sm-3 pull-left col-md-2 vcenter">
				<p class="inline">
					<?php ybd_home_link(); ?> &nbsp;|&nbsp; <a href="/pets/"><span class="mobile-hide">Search</span> <?php echo ybd_get_all_pets_count(); ?> pets &rsaquo;</a>
				</p>
			</div>


			<div class="col-xs-4 col-sm-4 col-md-5">
				<p class="inline">
					<a href="#" class="edit-location" data-toggle="modal" data-target="#map-edit-modal">
						<!-- javascript text -->
					</a>
				</p>
			</div>

			<div class="col-xs-3 col-sm-5 col-md-5 pull-right">
				<div class="fav-list text-right">
					<p class="inline">
						<?php if (is_user_logged_in() && !is_page('profile')) { ?>
							<i class="fas fa-heart"></i> <?php Ybd_Shelterbuddy_Public::ybd_favourites_link(); ?>
						<?php } elseif (is_user_logged_in() && is_page('profile')) { ?>
							<a href="<?php echo wp_logout_url('/profile/'); ?>">Logout</a>
						<?php } elseif (!is_user_logged_in() && !is_page('profile')) { ?>
							<a class="trigger-login-register-modal" href="<?php echo home_url() . '/profile/' ?>">Login</a>
						<?php } ?>
					</p>
				</div>
			</div>
		</div>
	</div>
</div>