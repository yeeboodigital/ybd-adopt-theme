jQuery(document).ready(function() {
    jQuery(".mobile-bottom-link a").on("click",function(t) {
        jQuery(".header-panel, .navbar-collapse").collapse("hide");
    });


    jQuery(".header-nav-container a").on("click", function (t) {
        jQuery(".header-nav-container a").removeClass("active");
        jQuery(".header-panel").collapse("hide");
        jQuery(this).addClass("active");
    });

    jQuery(".mobile-header-nav-container a").on("click", function (t) {
        jQuery(".mobile-header-nav-container a").removeClass("active");
        jQuery(".header-panel, .navbar-collapse").collapse("hide");
        jQuery(this).addClass("active");
    });

    jQuery(".navbar-toggle").on("click", function (t) {
        jQuery(".header-panel").collapse("hide");
        jQuery(".mobile-header-nav-container a").removeClass("active");
    });

    // jQuery(".mobile-bottom-link a").on("click", function (t) {
    //     jQuery(".header-panel, .navbar-collapse").collapse("hide");
    // });

    jQuery(".header-panel").on("hide.bs.collapse", function () {
        jQuery(".header-panel").hasClass("in") ? jQuery(".header-panel").addClass("no-transition") : jQuery(".header-panel").removeClass("no-transition");
    });

    jQuery(".header-panel").on("hidden.bs.collapse", function () {
        jQuery(".header-panel").hasClass("in") ? jQuery(".header-panel").addClass("no-transition") : jQuery(".header-panel").removeClass("no-transition");
    });

    // jQuery(".header-nav-container a").on("click", function (t) {
    //     jQuery(this).hasClass("active") && jQuery(this).removeClass("active");
    // });
});