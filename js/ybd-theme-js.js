(function($) {
    'use strict';

	// Default for Owl Carousels
    var responsize_carousel = {
        0: {
            dots: true,
            items: 1
        },
        768: {
            dots: false,
            items: 2
        },
        992: {
            dots: false,
            items: 3
        },
        1200: {
            dots: false,
            items: 4
        }
	}

	var defaultLocation = {
		city: "Vancouver", 
		province: "BC", 
		latitude: 49.2827291, 
		longitude: -123.12073750000002
	}

    $(document).ready(function() {

		$( window ).resize( function(){
			ybdUpdateDevice();
		}).resize();		

        $(window).on('locationChanged', function() {
			ybd_sb_loadmore_params.current_page = 0;
			ybdAjaxPets(false, ybd_sb_loadmore_params.current_page);
			setLocationText();
			ybdNearestLocation();
        });

        // WordPress adds a 'logged-in' class to the body when a user is logged in.
        var loggedIn = $('body').hasClass('logged-in');

        YbdSetLocationFromIp();

        ybdSetGeoLocation();

        ybdStickyToolbar();

		ybdGetDistanceBetween();

		// Load more pets button on frontpage 
		ybdLoadMorePets();

		// Load more pets button on pet search page 
		ybdLoadMoreQueryPets();
		
        // Initialize the carousel if it exists on the pet profile page (single-pets)
		ybdPetProfileCarousel();

        // Initialize the carousel if it exists on the profile page
		ybdNewsEventsCarousel();

        // Inialize the Featured carousel if it exists on the page
		ybdFeaturedCarousel();

        // Inialize the recently viewed carousel if it exists on the page
        ybdRecentlyViewedCarousel();

        // Inialize the favourites carousel if it exists on the page (profile)
        ybdProfileFavouritesCarousel();

        // Initialize the learn more carousel if it exists on the page (category-pets)
        ybdLearnMoreCarousel();

        // Initialize the favbar carousel if it exists on the page
        ybdFavbarCarousel();

        // Initialize the nearby pets carousel if it exists on the page (category-pets)
        ybdNearbyCarousel();

        filterFormFunctions();

        ybdSavedSearchFunctions();

        ybdFavouriteFunctions(loggedIn);

        ybdModalMapFunctions();
		
		saveRecentlyViewedPet();

		ybdSortToggle();

		ybdDeleteUserAccount();

		ybdLuminateSyncEmail();

		// Links that trigger the login/register modal
        $('.trigger-login-register-modal').on('click', function(e) {
            e.preventDefault();
            ybdTriggerLogin();
		});

		// Links that trigger the favourites/saved search notice modal
        $('.trigger-account-modal').on('click', function(e) {
            e.preventDefault();
			ybdTriggerAccountNotice();

		});

		// The button inside the notice modal that flips over to the login modal
		$('.account-req-button').on('click', function(){
			$('#account-required-modal').modal('hide');
			ybdTriggerLogin();
		});

	});
	
	// Wait till load to fire 
    $(window).load(function() {
		setLocationText();
		ybdNearestLocation();
		ybdAjaxPets(false, ybd_sb_loadmore_params.current_page);
		ybdPetProfileNav();
		setTimeout(function(){
			ybdHijackEmailIcon();
		},0);
		
		// Change the heights on the single pet page of the grey blocks
		$('.desktop .equal-heights, .tablet .equal-heights').each(function(){
			// Cache the highest
			var highestBox = 0;
			
			// Select and loop the elements you want to equalise
			$('.ybd-sb-pet-details-block', this).each( function(){
				
				// If this box is higher than the cached highest then store it
				if ( $(this).height() > highestBox ) {
					highestBox = $(this).height(); 
				}
			
			});  
					
			// Set the height of all those children to whichever was highest 
			$( '.ybd-sb-pet-details-block',this ).height( highestBox );				
		});

		if ( $('body').hasClass('desktop') ) {

			if (!window.localStorage.getItem('email_alert_popover')){
				$('.search-alert-button').popover({ trigger: 'manual', html: true});
				$('.search-alert-button').popover('show');
			} else {
				$('.search-alert-button').popover({ trigger: 'hover', html: true});
			}
	
			$('.popover-title i.fa-close').on('click', function() {
				window.localStorage.setItem('email_alert_popover', 1);
				$('.search-alert-button').popover('hide');
			});
		}

	});

	function ybdHijackEmailIcon(){
		var title = $('.pet-name').text();
		var pet_type = $('.pet-type').text();
		var emailIcon = $('.ybd-sb-pet-share').find('.at-svc-email');
		$('.at-share-btn-elements').prepend(emailIcon);
		emailIcon.removeClass('at-svc-email');
		emailIcon.on('click', function(e){
			location.href = 'mailto:?subject=Meet '+ title +', an adoptable pet from the BC SPCA&body=I found this adorable '+ pet_type +' on adopt.spca.bc.ca and thought you might be interested! ' + escape('\r\n\r\n') + location.protocol + '//' + location.host + location.pathname;
			e.preventDefault();
			e.stopImmediatePropagation();
			return false;
		});	
	}

	/**
	 * Uses ajax to load more pets on the front page. Uses a location-filtered master query to return paginated pets
	 */
    function ybdLoadMorePets() {
        jQuery('.ajax-load-more-button').on('click', function() {
            if (ybd_sb_loadmore_params.canBeLoaded == true) {
				
                var button = jQuery(this);
                var location = null;

                if (typeof(Storage) !== "undefined") {
                    var savedLocation = window.localStorage.getItem('location_new');
					if ( !savedLocation ) {
						location = defaultLocation;
					} else {
						location = JSON.parse(savedLocation);
					}
				}
				
                var data = {
                    'action': 'frontpage_pets',
                    'query': ybd_sb_loadmore_params.posts,
                    'location': location,
                    'count': ybd_sb_loadmore_params.current_page,
				};

                jQuery.ajax({
                    url: '/wp-admin/admin-ajax.php',
                    data: data,
                    method: 'POST',
                    beforeSend: function(xhr) {
						jQuery('.lds-roller').show();
                        button.text('Loading...');
                        // the AJAX call is in process, we shouldn't run it again until complete
                        ybd_sb_loadmore_params.canBeLoaded = false;
					}
				}).done( function(data) {
						jQuery('.lds-roller').hide();
                        if (data) {
							if ( data == 0){
								button.hide();
								return;
							}
							jQuery('#frontpage-query-pets').append(data);
                            button.text('See More Pets');
                            ybd_sb_loadmore_params.canBeLoaded = true;
                            ybd_sb_loadmore_params.current_page++;
                        }
            	}).fail(function(){
						button.hide();
				}).always(function(){
						jQuery('.lds-roller').hide();
				});
            }
        });
	}
	
	/**
	 * Uses ajax to load more pets on the query landing page.
	 */
    function ybdLoadMoreQueryPets() {
        jQuery('.ajax-load-more-query-button').on('click', function() {
            if (ybd_sb_loadmore_params.canBeLoaded == true) {
				
                var button = jQuery(this);
                var location = null;

                if (typeof(Storage) !== "undefined") {
                    var savedLocation = window.localStorage.getItem('location_new');
					if ( !savedLocation ) {
						location = defaultLocation;
					} else {
						location = JSON.parse(savedLocation);
					}
                }

				var sortBy = jQuery('.sort-toggle .dropdown-menu li.selected').data('sortby');

                var data = {
					'paginated': 	true,
                    'action': 		'load_query_pets',
                    'query': 		ybd_sb_loadmore_params.posts,
                    'location': 	location,
					'sortby': 		sortBy,
                    'count': 		ybd_sb_loadmore_params.current_page
                };

                jQuery.ajax({
                    url: '/wp-admin/admin-ajax.php',
                    data: data,
                    method: 'POST',
                    beforeSend: function(xhr) {
						jQuery('.lds-roller').show();
                        button.text('Loading...');
                        // the AJAX call is in process, we shouldn't run it again until complete
                        ybd_sb_loadmore_params.canBeLoaded = false;
                    }
                }).done(function(data){
					jQuery('#query-pets').append(data);
					button.text('See More Pets');
					ybd_sb_loadmore_params.canBeLoaded = true;
					ybd_sb_loadmore_params.current_page++;
					if ( -1 != data.search( 'no_more_posts' ) ){
						button.hide();
					}
				}).fail(function(data){
					button.hide();
				}).always(function(data){
					jQuery('.lds-roller').hide();
				});
            }
        });
    }
	
	// Use ajax to load the search queried pets (gets around WPEngine aggressive caching)
    function ybdAjaxPets(sortBy, count) {
        if ( jQuery( '#query-pets' ).length ) {
			if ( sortBy === false ) {
				sortBy = 'distance';
			}

			var location = null;

            if (typeof(Storage) !== "undefined") {
				var savedLocation = window.localStorage.getItem('location_new');
				if ( !savedLocation ) {
					location = defaultLocation;
				} else {
					location = JSON.parse(savedLocation);
				}
			}

			var params = JSON.parse(ybd_sb_loadmore_params.posts);

			// Don't need these, they'll just mess with our code
			delete params.taxonomy;
			delete params.term;
			ybd_sb_loadmore_params.posts = JSON.stringify(params);

			var data = {
				'paginated':	true,
                'action': 		'load_query_pets',
                'query': 		ybd_sb_loadmore_params.posts,
				'location': 	location,
				'sortby':		sortBy,
				'count': 		count
			};

            jQuery.ajax({
                url: '/wp-admin/admin-ajax.php',
                data: data,
                method: 'POST',
				beforeSend: function(xhr) {
					jQuery('.lds-roller').show();
				}
			}).done(function(data){
				if (data != 0) {
					ybd_sb_loadmore_params.current_page++;
					// Adding localstorage sugar to be able to query for navigation on the animal profile page
					var args = JSON.parse(ybd_sb_loadmore_params.posts);
					if (typeof(Storage) !== "undefined") {
						window.localStorage.setItem('search', JSON.stringify(args));
					}
					jQuery('#query-pets').html(data);
					jQuery('#query-pets').find('a').each( function(){
						var url = jQuery(this).attr('href');
						jQuery(this).attr('href', url+'?from_search=true');
					});
					if ( -1 == data.search( 'no_more_posts' ) ){
						jQuery('.ajax-load-more-query-button').show();
					}
				} else {
					jQuery('.no-pets-found').show();
				}
			}).fail(function(data){
				jQuery('.no-pets-found').show();
			}).always(function(data){
				jQuery('.lds-roller').hide();
			});
        }

		// Frontpage paginated pets
        if (jQuery('#frontpage-query-pets').length) {
			var location = null;
            if (typeof(Storage) !== "undefined") {
				var savedLocation = window.localStorage.getItem('location_new');
				if ( !savedLocation ) {
					location = defaultLocation;
				} else {
					location = JSON.parse(savedLocation);
				}
            }

            var data = {
                'action': 'frontpage_pets',
                'query': ybd_sb_loadmore_params.posts,
                'location': location,
                'count': ybd_sb_loadmore_params.current_page
            };

            jQuery.ajax({
                url: '/wp-admin/admin-ajax.php',
                data: data,
                method: 'POST',
                beforeSend: function(xhr) {
					jQuery('.lds-roller').show();
                    // the AJAX call is in process, we shouldn't run it again until complete
                    ybd_sb_loadmore_params.canBeLoaded = false;
                },
                success: function(data) {
                    if (data) {
						jQuery('.lds-roller').hide();
                        ybd_sb_loadmore_params.current_page++;
                        ybd_sb_loadmore_params.canBeLoaded = true;
                        jQuery('#frontpage-query-pets').html(data);
						if ( -1 == data.search( 'no_more_posts' ) ){
							jQuery('.ajax-load-more-button').show();
						}
					}
                }
            });
		}
		
    }

    function ybdFavouriteFunctions(loggedIn) {
        // All Favouriting.
        // Toggle the inactive state.
        $('body').on('click', '.ybd-favourite', function(e) {
            e.preventDefault();
            if (loggedIn == '1') {
                var heart = $(this).find('i.heart');
                if (heart.hasClass('far')) {
                    heart.removeClass('far').addClass('fas');
                } else {
                    heart.removeClass('fas').addClass('far');
                }

                if ($('body').hasClass('single-pets')) {
                    var addTextDiv = $('.add-to-fav-text');
                    if (addTextDiv.hasClass('is-favourite')) {
                        addTextDiv.removeClass('is-favourite').text(addTextDiv.data('add-text'));
                    } else {
                        addTextDiv.addClass('is-favourite').text(addTextDiv.data('added'));
                    }
                }

                $.ajax({
                    method: 'POST',
                    url: profile_ajax.ajaxurl,
                    data: {
                        action: 'update_favourite',
                        security: profile_ajax.security,
                        post_id: heart.data('postid')
                    },
                    success: function(data) {
                        $('.ybd-fav-link span.fav-count').text(data.trim());
                        // console.log(data);
                    },
                    error: function(err) {
                        console.error(err);
                    }
                });
            } else {
                ybdTriggerAccountNotice();
            }

        });
    }

    function ybdTriggerLogin() {
        jQuery('#login-register-modal').modal('show');
	}
	
    function ybdTriggerAccountNotice() {
        jQuery('#account-required-modal').modal('show');
    }

    /**
     * Have to use JS to get the IP location to get around caching with WPEngine.
     */
    function YbdSetLocationFromIp() {
        jQuery.get(profile_ajax.ajaxurl + '?action=geoip_detect2_get_info_from_current_ip', function(response) {
            if (typeof(Storage) !== "undefined") {
                if (!window.localStorage.getItem('location_new')) {
                    ybdSaveLocation(response.city.names.en, response.most_specific_subdivision.iso_code, response.location.latitude, response.location.longitude);
                    jQuery(window).trigger('locationChanged');
                }
            }
        }).fail(function(err) {
            console.error(err);
        });
    }

    function ybdGetGeolocation() {

        var deferredObject = jQuery.Deferred();

        if (navigator.geolocation) {

            // Success in getting location
            var success = function(position) {
                // console.log(position);
                deferredObject.resolve(position);
            };

            // Error getting current location
            var error = function(err) {
                deferredObject.reject(err);
            };

            var options = {
                enableHighAccuracy: true
            };

            navigator.geolocation.getCurrentPosition(success, error, options);

        } else {
            deferredObject.reject(false);
        }

        return deferredObject.promise();

    }

    function ybdSetGeoLocation() {
        if (typeof(Storage) !== "undefined") {
            if (!window.localStorage.getItem('location_new')) {
                // Ask for geolocation
                ybdGetGeolocation().then(function(res) {
                    // Ask the free Big Data Cloud API for better information about this location
                    ybdReverseGeocode(res.coords.latitude, res.coords.longitude).then(function(data) {
						var place = getCityAndProvince(data.results);
						//  Have to dig down to find the levels of data we need
                        // Save the location
                        ybdSaveLocation(place.city, place.province, place.lat, place.lng);
                        jQuery(window).trigger('locationChanged');
                        // The request to MapQuest failed.
                    }).fail(function(err) {
                        console.error(err);
                    });
                    // User denied location sharing, or there was an issue obtaining the location
                }).fail(function(err) {
                    console.log(err);
                    return false;
                });
            }
        }
	}
	
    function setLocationText() {
		var location = JSON.parse(window.localStorage.getItem('location_new'));
		if ( !location ) {
			return false;
		}
		if (location.province == 'BC') {
			location.province = 'B.C.';
		}
        jQuery('a.edit-location').data('lat', location.latitude).data('lng', location.longitude).html('<span class="mobile-hide">Near</span> ' + location.city + ', ' + location.province + '<span class="caret"></span>');
        jQuery('.location-text').text(location.city + ', ' + location.province);
	}
	
	// Google Geocode API
    function ybdReverseGeocode(lat, lng) {
		return jQuery.get('https://maps.googleapis.com/maps/api/geocode/json?latlng=' + lat + ',' + lng +'&region=CA&key=AIzaSyDGoolx_YuO7tzUR7j1ZepHpH3Ywc6o9u4');
    }
	
	// Google Geocode API
    function ybdGeocode(address) {
		var search_string = address.replace(' ', '+');
		return jQuery.get('https://maps.googleapis.com/maps/api/geocode/json?address=' + address +'&region=CA&key=AIzaSyDGoolx_YuO7tzUR7j1ZepHpH3Ywc6o9u4');
    }

    function filterFormFunctions() {
        var filterForm = jQuery('#filter-form');
        var moreFilters = jQuery('.more-filters');
        var toggleLink = jQuery('.filters-toggle');

        // Reset filters link
        jQuery('.reset-filters').on('click', function(e) {
            e.preventDefault();
            filterForm.find('select').val('any');
            filterForm.find('input').val('');
            filterForm.submit();
        });

        // More filters link
        toggleLink.on('click', function(e) {
            e.preventDefault();
            moreFilters.fadeToggle();
            if (!Cookies.get('filters')) {
				toggleLink.text(toggleLink.data('lessText'));
				// Cookies.set('filters', 'all', { path: '/', expires: 7 });
            } else {
				toggleLink.text(toggleLink.data('moreText'));
				// Cookies.remove('filters', { path: '/' });
            }
		});

		jQuery('.trigger-save-search').on('click', function(e){
			e.preventDefault();
			jQuery( '#saved-search-modal' ).modal('show');
			jQuery( '#saved-search-type' ).change();
		});

		// Multi Select args
		var multi_args = {
			maxHeight: 300,
			nonSelectedText: 'Any',
			inheritClass: true,
			buttonWidth: '100%',
			numberDisplayed: 1,
			includeResetOption: true,
			disableIfEmpty: true,
			// Limit selects to a max of 3 selections, to prevent crazy queries!
			onChange: function(option, checked) {
				console.log('here');
                // Get selected options.
                var selectedOptions = option.parent().find('option:selected');
				console.log();
                if (selectedOptions.length >= 3) {
                    // Disable all other checkboxes.
                    var nonSelectedOptions = option.parent().find('option').filter(function() {
                        return !$(this).is(':selected');
                    });
 
                    nonSelectedOptions.each(function() {
                        var input = $('input[value="' + $(this).val() + '"]');
                        input.prop('disabled', true);
                        input.parent('li').addClass('disabled');
                    });
                } else {
                    // Enable all checkboxes.
                    option.parent().find('option').each(function() {
                        var input = $('input[value="' + $(this).val() + '"]');
                        input.prop('disabled', false);
                        input.parent('li').addClass('disabled');
                    });
                }
            }
		};

		// Single Select args
		var args = {
			maxHeight: 300,
			nonSelectedText: 'Any',
			inheritClass: true,
			buttonWidth: '100%',
			numberDisplayed: 1,
		};

		// Single Selects
		jQuery('#select-weight, #select-gender, #select-age, #select-bonded').multiselect(args);
		jQuery('#saved-search-type, #saved-search-weight, #saved-search-gender, #saved-search-age, #saved-search-bonded').multiselect(args);
		// Multiselects
		jQuery('#select-breed, #select-compat, #select-location').multiselect(multi_args);
		jQuery('#saved-search-breed, #saved-search-compat, #saved-search-location').multiselect(multi_args);
	}
	
    function ybdModalMapFunctions() {
        var map = null;
        var myMarker;
        var myLatlng;

        function initializeGMap(lat, lng) {
            myLatlng = new google.maps.LatLng(lat, lng);

            var myOptions = {
                zoom: 12,
                center: myLatlng,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                zoomControl: true,
                mapTypeControl: false,
                scaleControl: false,
                streetViewControl: false,
                rotateControl: false,
                fullscreenControl: false,
                gestureHandling: 'greedy'
            };

            map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);

            myMarker = new google.maps.Marker({
                position: myLatlng,
                draggable: true,
            });
            myMarker.setMap(map);

            myMarker.addListener('dragend', function(e) {
                map.panTo(e.latLng);
                jQuery('#map-postal-code').val('');
            });

            map.addListener('click', function(e) {
				updateMarkerAndPanTo(e.latLng, map);
				jQuery('#map-postal-code').val('');
            });

            function updateMarkerAndPanTo(latLng, map) {
                myMarker.setPosition(latLng);
                map.panTo(latLng);
            }

        }

        jQuery('button.update-location').on('click', function(e) {
            e.preventDefault();
			// Ask the Google API for better information about this location
			ybdReverseGeocode(myMarker.getPosition().lat(), myMarker.getPosition().lng()).then(function(data) {
				// console.log(data);
				var place = getCityAndProvince(data.results);
				if (place) {
					ybdSaveLocation(place.city, place.province, myMarker.getPosition().lat(), myMarker.getPosition().lng());
					jQuery(window).trigger('locationChanged');
					jQuery('#map-edit-modal').modal('hide');
				}
			}).fail(function(err) {
				console.error(err);
			});
			return false;
        });

		jQuery('#map-postal-code').on('keyup', function(){
			jQuery('button.update-location').addClass('disabled').prop('disabled', true);
		});

        jQuery('#map-postal-code').on('keyup', debounce(function() {
			// Ask the Google API for better information about this location
			ybdGeocode(jQuery(this).val()).then(function(data) {
				// console.log(data);
				if ( data.results.length ) {
					var place = getCityAndProvince(data.results);
					if ( place ) {
						var myLatlng = new google.maps.LatLng(place.lat, place.lng);
						myMarker.setPosition(myLatlng);
						map.panTo(myLatlng);
						jQuery('button.update-location').removeClass('disabled').prop('disabled', false);
					}
				}
			}).fail(function(err) {
				console.error(err);
			});
        }, 350));

        // Trigger click on return key
        jQuery('#map-postal-code').keypress(function(e) {
            if (e.which == 13) {
				if ( !jQuery('button.update-location').hasClass('disabled') ) {
					jQuery('button.update-location').trigger('click');
				}
                return false;
            }
        });

        // Re-init map before show modal
        jQuery('#map-edit-modal').on('shown.bs.modal', function() {
            var button = jQuery('.edit-location');
            initializeGMap(button.data('lat'), button.data('lng'));
            jQuery("#location-map").css("width", "100%");
            jQuery("#map_canvas").css("width", "100%");
            google.maps.event.trigger(map, "resize");
            map.setCenter(myLatlng);
        });

	}
	
	function ybdSortToggle(){
		jQuery('.sort-toggle .dropdown-menu li').on('click', function(){
			if ( !jQuery(this).hasClass('selected') ){
				jQuery('.sort-toggle .dropdown-menu li').removeClass('selected');
				jQuery(this).addClass('selected');
				var sortby = jQuery(this).data('sortby');
				console.log(sortby);
				ybdAjaxPets(sortby, 0);
			}
		});
	}

    function ybdSavedSearchFunctions() {
		var show_all_breeds = false;

		jQuery('select[name="type"]').on('change', function(e) {

			// If it's a saved search modal, then show the full list of breeds for a pet type
			if ( jQuery(this).attr('id') === 'saved-search-type' ){
				show_all_breeds = true;
			}

			var breedDiv = jQuery(this).parents('.form-group').find('select[name="breed[]"]');
			breedDiv.prop('disabled', 'disabled');
			var pet_type = jQuery(this).val();
			// console.log(pet_type);
			jQuery.ajax({
				method: 'POST',
				url: profile_ajax.ajaxurl,
				data: {
					action: 'select_breeds',
					pet_type: pet_type,
					show_all: show_all_breeds
				},
				success: function(data) {
					breedDiv.empty();
					console.log(data);
					jQuery(data).each(function(i, val) {
						var selected = '';
						// console.log(val);
						if ( getQueryVariable('breed') == val.slug ) {
							// console.log(val.slug);
							selected = 'selected="selected"';
						}
						breedDiv.append('<option '+selected+' value="' + val.slug + '">' + val.name + ' (' + val.count + ')</option>');
					});
					breedDiv.multiselect('rebuild');
				},
				error: function(err) {
					console.error(err);
				}
			});
		});

        // Save user saved searches
        jQuery('#saved-search-modal').on('click', 'button.btn-green', function(e) {
            e.preventDefault();

            if (jQuery('#saved-search-type').val() == 'any') {
                alert('You must select a pet type to save a search.');
                return false;
            }

            var loggedIn = jQuery('body').hasClass('logged-in');

            if (loggedIn == '1') {

                var data = {
                    security: profile_ajax.security,
                    action: 'update_saved_searches',
                    url: jQuery('#modal-filter-form').serialize()
                };

                jQuery.post(profile_ajax.ajaxurl, data, function(response) {
					// Success
					// If we're on the Profile Page
					if ( jQuery('.ybd-saved-searches').length ) {
						response = JSON.parse(response);
						// console.log(response);
						var petDetails = '';
						if (response._base_details != null) {
							petDetails = response._base_details;
						}
						var key = jQuery('.ybd-saved-searches').find('ul.list-group li').length;
						var html = '<li class="list-group-item">';
						html += '<a href="#" class="ybd-delete-search pull-right" data-key="' + key + '"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Delete</a>';
						html += '<h4><a href="' + response._base_pretty_url + '">' + response._base_title + '</a><span>' + petDetails + '</span></h4>';
						html += '</li>';

						jQuery('.ybd-saved-searches').find('ul.list-group').append(html);
					}
                }).fail(function(err) {
                    console.error(err);
                });
				jQuery('#saved-search-modal').modal('hide');
			} else {
                alert('You need to have an account and be logged in to delete saved searches.');
            }

        });

        // Delete user saved searches
        jQuery('.ybd-saved-searches').on('click', 'a.ybd-delete-search', function(e) {
            e.preventDefault();

            if (jQuery('body').hasClass('logged-in')) {

				var shouldDelete = confirm('Are you sure you want to delete this search?');

				if ( shouldDelete ) {

					var data = {
						security: profile_ajax.security,
						action: 'update_saved_searches',
						key: jQuery(this).data('key')
					};

					var el = this;

					jQuery.post(profile_ajax.ajaxurl, data, function(response) {
						// Success
						//    console.log(response);		
						jQuery(el).parents('li').fadeOut(220, function() {
							jQuery(this).remove();
							jQuery('.ybd-saved-searches').find('ul.list-group li').each(function(i, val) {
								jQuery('a', this).data('key', i);
							});
						});

					}).fail(function(err) {
						console.error(err);
					});
				}
            } else {
                alert('You need to have an account and be logged in to delete saved searches.');
            }

        });

	}
	
	function ybdDeleteUserAccount(){
		jQuery('#delete-account-modal').on('click', 'button.delete-account-button', function(e) {
			e.preventDefault();
			$.ajax({
				method: 'POST',
				url: profile_ajax.ajaxurl,
				data: {
					action: 'delete_account',
					security: profile_ajax.security
				},
				success: function(data) {
					if ( data.status === 1){
						jQuery('#delete-account-modal').modal('hide');
						window.location.reload(true);
					} else {
						alert(data.message);
					}
				},
				error: function(err) {
					alert('There was an issue deleting your account. Log out, log back in, and retry.');
					console.error(err);
				}
			});
		});
	}

    function ybdStickyToolbar() {
        // Get the navbar
        var navbar = document.getElementsByClassName("ybd-sb-loc-fav-bar")[0];

        // Get the offset position of the navbar
        var sticky = navbar.offsetTop;
        // When the user scrolls the page, execute myFunction 
        window.onscroll = function() {
            if (window.pageYOffset >= sticky) {
                navbar.classList.add("sticky");
            } else {
                navbar.classList.remove("sticky");
            }
        };

    }

	// On single pets viewing, add to recently viewed
	function saveRecentlyViewedPet(){
		if ( jQuery('body.single-pets')[0] ) {
			if (typeof(Storage) !== "undefined") {
				var viewedPets = window.localStorage.getItem('recently_viewed');
				// console.log(viewedPets);
				if ( viewedPets != null ) {
					viewedPets = JSON.parse(viewedPets);
					// Check to make sure the pet isn't already in the array
					if ( jQuery.inArray( ybd_sb_loadmore_params.current_id, viewedPets ) === -1 ) {
						viewedPets.unshift(ybd_sb_loadmore_params.current_id);
					}
				} else {
					viewedPets = [ybd_sb_loadmore_params.current_id];
				}
				// Slice it down to 8
				viewedPets = viewedPets.slice(0, 8);
				window.localStorage.setItem('recently_viewed', JSON.stringify(viewedPets));
            }
		}
	}

    function ybdPetProfileCarousel() {
        // owl carousel on pet-profile page
        if (jQuery(".single-pets").length) {
            jQuery('.ybd-sb-pet-carousel').slick({
                arrows: true,
                dots: true,
                centerMode: true,
                centerPadding: '10px',
                // edgeFriction: .50,
                // variableWidth: true,
                slidesToShow: 3,
                slidesToScroll: 3,
                prevArrow: '<i class="fas fa-chevron-left"></i>',
                nextArrow: '<i class="fas fa-chevron-right"></i>',
                responsive: [{
                    breakpoint: 992,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        infinite: false,
                        dots: true
                    }
                }, ]
            });

			jQuery('.ybd-sb-pet-carousel').find('.expand-icon').magnificPopup({
                type: 'image',
                gallery: { enabled: true }
                // other options
            });
        }
    }

    function ybdFavbarCarousel() {

        // Favbar Carousel
        // Check that the anchor node exists on the page before instatiating the carousel
        if (jQuery('.ybd-favbar-carousel')[0]) {

            var favbarCarousel = jQuery('.ybd-favbar-carousel').owlCarousel({
                margin: 5,
                loop: false,
                nav: false,
                dots: false,
                autoWidth: true,
                items: 1
            });

            jQuery('.chip').not('.multi').on('click', 'i', function() {
                var chip = jQuery(this).parent();
                var index = jQuery(".chip").index(chip);
                var key = chip.data('key');
                jQuery('select[name=' + key + ']').val('any');
                jQuery('input[name=' + key + ']').val('');
                favbarCarousel.trigger('remove.owl.carousel', index).trigger('refresh.owl.carousel');
                setTimeout(function() {
                    jQuery('#filter-form').submit();
                }, 200);
			});
			
            jQuery('.chip.multi').on('click', 'i', function() {
				// Handle removal of the select value from the multiselect
                var chip = jQuery(this).parent();
                var name = chip.data('name');
                var value = chip.data('value');
                jQuery('#'+name).multiselect('deselect', value);

				// Handle removal of the chip from the carousel
				var index = jQuery(".chip").index(chip);
				favbarCarousel.trigger('remove.owl.carousel', index).trigger('refresh.owl.carousel');
				
				// Reload the page
                setTimeout(function() {
                    jQuery('#filter-form').submit();
                }, 100);
            });			

            jQuery(window).load(function() {
                jQuery('.chip').each(function() {
                    var outerWidth = jQuery(this).outerWidth();
                    jQuery(this).width(outerWidth - 20);
                });
                favbarCarousel.trigger('refresh.owl.carousel');

            });
        }

    }

    function ybdProfileFavouritesCarousel() {
        // Profile Owl Carousel
        // Check that the class node exists on the page before instatiating the carousel
        if (jQuery('.ybd-profile-container')[0]) {

            var profileCarousel = jQuery('.ybd-profile-carousel').owlCarousel({
                // loop: true,
                nav: true,
                slideBy: 'page',
                navText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-chevron-right"></i>'],
                responsive: responsize_carousel
            });

            if (jQuery('.ybd-profile-carousel .owl-item').length === 0) {
                jQuery('.ybd-no-animals').fadeIn();
            }

            profileCarousel.on('remove.owl.carousel', function(ev) {
                if (jQuery('.ybd-profile-carousel .owl-item').length === 0) {
                    jQuery('.ybd-no-animals').fadeIn();
                }
            });

            // Remove the unfavourited item from the carousel
            jQuery('.ybd-profile-carousel .ybd-favourite').on('click', 'i', function(e) {
                e.preventDefault();
                var item = jQuery(this).parents('.owl-item');
                var idx = item.index();
                item.animate({
                    opacity: 0,
                }, 300, function() {
                    profileCarousel.trigger('remove.owl.carousel', [idx]);
                    profileCarousel.trigger('refresh.owl.carousel');
                });
            });
        }
    }

    function ybdLearnMoreCarousel() {

        // Learn More Owl Carousel
        // Check that the anchor node exists on the page before instatiating the carousel
        if (jQuery('#learn-more')[0]) {

            var learnMoreCarousel = jQuery('.ybd-learn-more-carousel').owlCarousel({
                loop: true,
                nav: true,
                navText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-chevron-right"></i>'],
                responsive: responsize_carousel
            });
        }

	}

    function ybdNewsEventsCarousel() {

        // News and Events Owl Carousel
        // Check that the anchor node exists on the page before instatiating the carousel
        if (jQuery('#news-events')[0]) {

            var newsEventsCarousel = jQuery('.ybd-news-events-carousel').owlCarousel({
                loop: true,
                nav: true,
                navText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-chevron-right"></i>'],
                responsive: responsize_carousel
            });
        }

	}
	
    function ybdFeaturedCarousel() {

        // Featured Owl Carousel
        // Check that the anchor node exists on the page before instatiating the carousel
        if (jQuery('#featured-pets')[0]) {

            var featuredCarousel = jQuery('.ybd-featured-carousel').owlCarousel({
                loop: true,
                nav: true,
                navText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-chevron-right"></i>'],
                responsive: responsize_carousel
            });
		
			jQuery('.ybd-featured-carousel a').each(function(){
				var url = jQuery(this).attr('href');
				jQuery(this).attr('href', url+ '?from_featured=true');
			});
		
		}

	}
	
    function ybdRecentlyViewedCarousel() {

        // Recently Viewed Pets Owl Carousel
        if (jQuery('#recently-viewed-pets')[0]) {
		// Recently Viewed Pets
            if (typeof(Storage) !== "undefined") {
                var viewedPets = window.localStorage.getItem('recently_viewed');
				if ( viewedPets != null ) {
					viewedPets = JSON.parse(viewedPets);
					// console.log(viewedPets.length);
					if ( viewedPets.length < 1 ) {
						return;
					}
				} else {
					return;
				}
            }

            var data = {
                'action': 'recently_viewed_pets',
                'recently_viewed': viewedPets,
            };

            jQuery.ajax({
                url: '/wp-admin/admin-ajax.php',
                data: data,
                method: 'POST',
                success: function(data) {
					if (data != 0) {
						jQuery('.ybd-recent-carousel').append(data);
						jQuery('.recently-viewed').show();
						var recentCarousel = jQuery('.ybd-recent-carousel').owlCarousel({
							loop: false,
							nav: true,
							navText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-chevron-right"></i>'],
							responsive: responsize_carousel
						});						
						recentCarousel.trigger('refresh.owl.carousel');
                    } else {
						window.localStorage.removeItem('recently_viewed');
					}
                }
            });
		}

    }

	function ybdNearbyCarousel() {
		// Nearby Pets Owl Carousel
		if (jQuery('#other-pets-nearby')[0]) {
			var location = null;
			if (typeof(Storage) !== "undefined") {
				var savedLocation = window.localStorage.getItem('location_new');
				if ( !savedLocation ) {
					return;
				}
				location = JSON.parse(savedLocation);
			}

			var type = jQuery('.nearby-container').data('type');

			var data = {
				'action': 'ajax_nearby_pets',
				'location': location,
				'num': 12,
				'pet_type': type
			};

			jQuery.ajax({
				url: '/wp-admin/admin-ajax.php',
				data: data,
				method: 'POST',
				success: function(data) {
                    if (data) {
						if ( data == 0) {
							return;
						}
						jQuery('.ybd-nearby-carousel').append(data);
						jQuery('.nearby-container').show();
						var nearbyCarousel = jQuery('.ybd-nearby-carousel').owlCarousel({
							loop: true,
							nav: true,
							navText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-chevron-right"></i>'],
							responsive: responsize_carousel
						});						
						nearbyCarousel.trigger('refresh.owl.carousel');
                    }
				}
			});
		}
	}

	function ybdNearestLocation(){
		if (jQuery('.page-profile').length) {
			var location = null;
			if (typeof(Storage) !== "undefined") {
				var savedLocation = window.localStorage.getItem('location_new');
				location = JSON.parse(savedLocation);
			}

			var data = {
				'action': 'nearest_location',
				'location': location,
			};

			jQuery.ajax({
				url: '/wp-admin/admin-ajax.php',
				data: data,
				method: 'POST',
				success: function(data) {
					if (data) {
						data = JSON.parse(data);
						jQuery('a.nearest-link').attr('href', 'https://spca.bc.ca/locations/' + data.slug).text(data.name)
						jQuery('span.nearest-distance').text('('+data.distance +'km away)');
						jQuery('.nearest-address').html(data.address);
						jQuery('.nearest-city').text(data.city + ', B.C.');
						jQuery('.nearest-postal').text(data.postal);
						jQuery('.nearest-tel a').attr('href', 'tel:' + data.phone).text(data.phone);
						jQuery('.nearest-email a').attr('href', 'mailto:' + data.email).text(data.email);
						jQuery('.nearest-map').attr('src', 'https://www.google.com/maps/embed/v1/place?zoom=15&key=AIzaSyBn7NbQS1SfesaAF8En2vhcjDRwN1cg4I8&q=BCSPCA+'+data.esc_postal+'&center='+data.latitude+','+data.longitude);
						jQuery('.ybd-closest-location').show();
					}
				}
			});
		}	
	}

	function ybdPetProfileNav(){
		var savedLocation = null;
		var savedLastSearch = null;

		var context = 'default';
		var contextUrl = '';

		// Frontpage paginated pets
        if (jQuery('body').hasClass('single-pets')) {
            if (typeof(Storage) !== "undefined") {
                savedLocation = JSON.parse(window.localStorage.getItem('location_new'));
                savedLastSearch = JSON.parse(window.localStorage.getItem('search'));
			}
			
			if ( location.search.indexOf('from_featured') != -1 ) {
				console.log('featured in url');
				context = 'featured';
				contextUrl = '?from_featured=true';
			}

			if ( window.location.href.indexOf('from_search') != -1 ) {
				console.log('filtering in url');
				context = 'search';
				contextUrl = '?from_search=true';
			}
			
            var data = {
                'action': 'ybd_nav_links',
				'location':	savedLocation,
				'post_id':	ybd_sb_loadmore_params.current_id,
				'context':	context,
				'query':	savedLastSearch
            };

            jQuery.ajax({
                url: '/wp-admin/admin-ajax.php',
                data: data,
                method: 'POST',
                success: function(data) {
                    if (data) {
						data = JSON.parse(data);
						console.log(data);
						jQuery('.ybd-sb-next-pet a').attr('href', data.next.guid+contextUrl).show();
						jQuery('.ybd-sb-previous-pet a').attr('href', data.prev.guid+contextUrl).show();
                    }
                }
            });
		}		
	}

	// Checks if a user has been added to the survey we use to process the server-side addition of the user to specific groups and interests
	function ybdLuminateSyncEmail(){
		if ($('body').hasClass('logged-in lo-subscribed-false') ){

			luminateExtend.init({
				apiKey: 'open_bcspca',
				path: {
					nonsecure: 'http://support.spca.bc.ca/site/',
					secure: 'https://secure3.convio.net/bcspca/site/'
				}
			});

			// console.log(luminateExtend.global);

			var addUserToLuminate = function() {
				luminateExtend.api.request({
					api: 'CRSurveyAPI',
					callback: updateWPUserCallback,
					requiresAuth: true,
					data: 'method=submitSurvey&survey_id=8962&cons_email=' + ybd_sb_loadmore_params.current_user_email + '&cons_email_opt_in=true&cons_info_component=t&question_14905=1021&question_14905=2241&question_14905=1081&question_14905=1741&question_14905=1244&denySubmit='
				});
			}

			var updateWPUserCallback = function(data) {
				// console.log(data);
				if (!data.errorResponse && data.submitSurveyResponse.success == 'true') {
					$.ajax({
						method: 'POST',
						url: profile_ajax.ajaxurl,
						data: {
							action: 'update_luminate_status',
							security: profile_ajax.security,
							data: 'true'
						},
						success: function (data) {
							if (data.status === 1) {
								// console.log(data);
								var wp_data = {
									'action': 'ybd_update_luminate',
									'user_id': ybd_sb_loadmore_params.current_user_id,
								};
								jQuery.ajax({
									url: '/wp-admin/admin-ajax.php',
									data: wp_data,
									method: 'POST',
									success: function (data) {
										if (data) {
											// console.log(data);
										}
									}
								});
							} else {
								console.error(data);
							}
						},
						error: function (err) {
							console.error(err);
						}
					});
				}
			}

			// Try the routine to add the user to Luminate
			addUserToLuminate();		
		}
	}	

	/**
	 * Uses ajax to determine the distance from the user to the pet's shelter location for use on the animal profile page
	 */
    function ybdGetDistanceBetween() {
		if (jQuery(".single-pets").length) {
			var fromLocation, toLocation;

			fromLocation = jQuery('.pet-profile').data('location');

			if (typeof(Storage) !== "undefined") {
				var savedLocation = window.localStorage.getItem('location_new');
				if ( !savedLocation ) {
					toLocation = defaultLocation;
				} else {
					toLocation = JSON.parse(savedLocation);
				}
			} else {
				return false;
			}

			var data = {
				'action': 			'distance_between',
				'from_location': 	fromLocation,
				'to_location': 		toLocation.latitude + ',' + toLocation.longitude
			};

			jQuery.ajax({
				url: '/wp-admin/admin-ajax.php',
				data: data,
				method: 'POST',
				success: function(data) {
					if (data) {
						// console.log(data);
						jQuery('.distance-away').text('('+data.trim()+'km)');
					}
				}
			});
		}
	}

	// Add a body class for desktop, tablet and smartphone depending on breakpoints to make styling for mobile devices media-query free
	function ybdUpdateDevice(){
		var bodyEl = $( 'body' );

		var phoneBreakPoint = 768;
		var tabletBreakPoint = 1024;

		// smaller than 768? We'll say that's a smartphone
		if ( document.body.clientWidth < phoneBreakPoint && !bodyEl.hasClass( 'smartphone' ) ) {
			bodyEl.addClass( 'smartphone' ).removeClass( 'tablet desktop' );
		// bigger than phoneBreakPoint but smaller than tabletBreakPoint, we'll say that it's a tablet
			} else if ( document.body.clientWidth >= phoneBreakPoint && document.body.clientWidth <= tabletBreakPoint && !bodyEl.hasClass( 'tablet' ) ) {
			bodyEl.addClass( 'tablet' ).removeClass( 'smartphone desktop' );
		// bigger than tabletBreakPoint we'll say it's a desktop
		} else if ( document.body.clientWidth > tabletBreakPoint && !bodyEl.hasClass( 'desktop' )	) {
			bodyEl.addClass( 'desktop' ).removeClass( 'smartphone tablet' );
		}
	}

})(jQuery);

// Helper function to save the current location
function ybdSaveLocation(city, province, latitude, longitude) {
    var location = {
        city: city,
        province: province,
        latitude: latitude,
        longitude: longitude
    };
    var locationToSave = JSON.stringify(location);
    window.localStorage.setItem('location_new', locationToSave);
}

// helper function for keyup throttling
function debounce(func, wait, immediate) {
    var timeout;
    return function() {
        var context = this,
            args = arguments;
        var later = function() {
            timeout = null;
            if (!immediate) func.apply(context, args);
        };
        var callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
    };
};

function getQueryVariable(variable) {
    var query = window.location.search.substring(1);
    var vars = query.split('&');
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split('=');
        if (decodeURIComponent(pair[0]) == variable) {
            return decodeURIComponent(pair[1]);
        }
    }
    // console.log('Query variable %s not found', variable);
}

function getCityAndProvince(results){
	var indice = 0, city = null, province = null;

	for (var i = 0; i < results[0].address_components.length; i++) {
		if (results[0].address_components[i].types[0] == 'locality') {
			city = results[0].address_components[i];
		}
		if (results[0].address_components[i].types[0] == "administrative_area_level_1") {
			province = results[0].address_components[i];
		}
	}

	if ( city != null && province != null ) {
		//city and province data
		var obj = {
			city: city.short_name,
			province: province.short_name,
			lat: results[0].geometry.location.lat,
			lng: results[0].geometry.location.lng
		}
		// console.log(obj);
		return obj;
	}
	return false;
}