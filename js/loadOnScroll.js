// jQuery(function($) {
// 	var canBeLoaded = true, // this param allows to initiate the AJAX call only if necessary
// 	    bottomOffset = 2200; // the distance (in px) from the page bottom when you want to load more posts
 
// 	if ( $('body').hasClass('home') ) {
// 		$(window).scroll(function() {
// 			var data = {
// 				'action': 'loadmore',
// 				'query': ybd_sb_loadmore_params.posts,
// 				'page' : ybd_sb_loadmore_params.current_page
// 			};
// 			if ( $(document).scrollTop() > ( $(document).height() - bottomOffset ) && canBeLoaded == true ) {
// 				$.ajax({
// 					url: '/wp-admin/admin-ajax.php',
// 					data: data,
// 					method: 'POST',
// 					beforeSend: function( xhr ) {
// 						// the AJAX call is in process, we shouldn't run it again until complete
// 						canBeLoaded = false; 
// 					},
// 					success: function( data ) {
// 						if ( data ) {
// 							$('#primary #pets-near-me').append( data );
// 							canBeLoaded = true; // the ajax is completed, now we can run it again
// 							ybd_sb_loadmore_params.current_page++;
// 						}
// 					}
// 				});
// 			}
// 		});
// 	}
// });