<?php

/**
 * The Pet Profile template file
 *
 *
 * @link       http://www.yeeboodigital.com
 * @since      1.0.0
 *
 * @package    Ybd_Shelterbuddy
 * @subpackage Ybd_Shelterbuddy/public/templates
 */

?>

<?php get_header(); ?>

<?php

// various variables needed for post
$post_id = get_the_ID();
$media = get_attached_media('image', $post_id);
$animalType = wp_get_post_terms($post_id, 'pet_types');
$animalBreed = wp_get_post_terms($post_id, 'pet_breed');
$animalColour = get_post_meta($post_id, '_base_pets_colour', true);
$animalSex = wp_get_post_terms($post_id, 'pet_sex');
$animalSize = wp_get_post_terms($post_id, 'pet_size');
$animalLocationID = get_post_meta($post_id, '_base_pets_location_id', true);
$animalWeight = get_post_meta($post_id, '_base_pets_weight', true);
$animalBonded = get_post_meta($post_id, '_base_pets_bonded_with', true);
$animalWeight = get_post_meta($post_id, '_base_pets_weight', true);
$animalLocation = Ybd_ShelterBuddy_Public::get_pet_shelter_location($animalLocationID);

// location meta info
$hours = get_post_meta($animalLocation->ID, '_location_hours', true);
$hours = explode(',', $hours);
$phone = get_post_meta($animalLocation->ID, '_location_phone', true);
$email = get_post_meta($animalLocation->ID, '_location_email', true);
$address = get_post_meta($animalLocation->ID, '_location_address', true);
$city = get_post_meta($animalLocation->ID, '_location_city', true);
$postal = get_post_meta($animalLocation->ID, '_location_postal_code', true);

$latitude = get_post_meta($animalLocation->ID, '_location_latitude', true);
$longitude = get_post_meta($animalLocation->ID, '_location_longitude', true);

$esc_address = str_replace(' ', '+', $address);
$esc_postal = str_replace(' ', '+', $postal);

$animalID  = get_post_meta($post_id, '_base_pets_animal_id', true);

$flag_img = ybd_get_flag_img($post_id);

$animal_video = get_post_meta($post_id, '_base_pets_animal_video', true);

// compatibility properties
$compat = ybd_compatibility_helper($post_id);

// figure out the total days in care
$animalDaysInCareDate = get_post_meta($post_id, '_base_pets_days_in_care', true);
$today = strtotime(date('Y-m-d'));
$compare = strtotime($animalDaysInCareDate);
$animalDaysInCare = ($today - $compare) / 60 / 60 / 24;

$animalAgeWeeks = get_post_meta( $post_id, '_base_pets_age_weeks', true );
if ( $animalAgeWeeks < 52 ) {
		$animalAge = (int) round($animalAgeWeeks / 4) . ' months old';
} else if ( $animalAgeWeeks >= 52 && $animalAgeWeeks <= 103 ) {
      $animalAge = '1 year old';
} else {
   $animalAge = (int) round( $animalAgeWeeks / 52 ) . ' years old';
}

$logged_in = is_user_logged_in();

$is_favourite = false;

$user_loved_animals = get_user_meta(get_current_user_id(), 'ybd_user_loves', true);

if (is_array($user_loved_animals)) {
	if (in_array($post->ID, $user_loved_animals)) {
		$is_favourite = true;
	}
}

$context_url = '';
$prev_text = 'Previous pet';
$next_text = 'Next pet';

// If we arrived at this pet form clicking on featured pets
if (strpos($_SERVER['REQUEST_URI'], 'from_featured')) {
	$context_url = '?from_featured=true';
	// If we arrived at this pet after browsing from search
} else if (strpos($_SERVER['REQUEST_URI'], 'from_search')) {
	$context_url = '?' . $_SERVER['QUERY_STRING'];
	$prev_text = 'Previous pet <span class="mobile-hide">in search</span>';
	$next_text = 'Next pet <span class="mobile-hide">in search</span>';
}

/* Start the Loop */
if (have_posts()) : while (have_posts()) : the_post(); ?>

		<div class="container-fluid pet-profile" data-location="<?php echo $latitude . ',' . $longitude; ?>">
			<div class="row">
				<div class="col-xs-12">
					<div class="ybd-sb-pet-carousel <?php if (count($media) > 1 || count($media) < 4) { echo 'img-count-low'; } ?>" id="owl-onpage">

						<?php if (!empty($animal_video)) { ?>
							<div class="item">
								<div class="ybd-item-container">
									<iframe width="100%" height="100%" src="https://www.youtube.com/embed/<?php echo $animal_video; ?>" frameborder="0" allowfullscreen></iframe>
								</div>
							</div>
						<?php } ?>

						<?php if (!empty($media)) { ?>

							<?php foreach ($media as $img) { ?>
								<div class="item">
									<div class="ybd-item-container" style="background-image:url(<?php echo $img->guid; ?>)">
										<!-- <img src="<?php echo $img->guid; ?>" alt="pet image"> -->
										<?php if ( $flag_img ) { ?>
											<img class="infoster" src="<?php echo get_stylesheet_directory_uri() . '/img/flags/' . $flag_img; ?>" alt="status flag">
										<?php } ?>
										<a class="expand-icon" href="<?php echo $img->guid; ?>" data-mfp-src="<?php echo $img->guid; ?>">
											<!-- trigger for lightbox --></a>
									</div>
								</div>
							<?php } ?>

						<?php } else { ?>

							<div class="item">
								<div class="ybd-item-container" style="background-image:url(<?php echo get_stylesheet_directory_uri() .  '/img/noimg.jpg'; ?>)">
									<!-- background placeholder img -->
								</div>
							</div>

						<?php } ?>
					</div>
				</div>
			</div>
		</div>

		<div class="container">
			<div class="row ybd-sb-pet-nav">
				<div class="col-xs-4 text-left">
					<span class="ybd-sb-previous-pet">
						<a href="#" style="display:none">
							&lsaquo; <?php echo $prev_text; ?>
						</a>
					</span>
				</div>
				<div class="col-xs-4 pull-right text-right">
					<span class="ybd-sb-next-pet">
						<a href="#" style="display:none">
							<?php echo $next_text; ?> &rsaquo;
						</a>
					</span>
				</div>
			</div>

			<div class="row">
				<div class="col-md-8 col-xs-12">
					<div class="row">
						<div class="col-sm-8 col-xs-10">
							<h2 class="ybd-sb-h2">Meet <span class="pet-name"><?php the_title(); ?></span></h2>
						</div>
						<div class="col-sm-4 col-xs-2 ybd-sb-pet-profile-favourite">
							<!-- favourite pet stuff here -->
							<div class="ybd-favourite text-right">
								<span class="add-to-fav-text<?php if ($logged_in && $is_favourite) { echo ' is-favourite'; } ?>" data-added="Added" data-add-text="Add to favourites">
									<?php if ($logged_in && $is_favourite) { echo 'Added'; } else { echo 'Add to favourites'; } ?>
								</span>
								<i class="heart fa-heart <?php if (!$logged_in || !$is_favourite) { echo 'far'; } else { echo 'fas'; } ?>" data-postid="<?php echo $post->ID; ?>" data-logged_in="<?php echo $logged_in; ?>"></i>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 ybd-sb-content">
							<?php $content = get_the_content(); ?>
							<?php if (!empty($content)) {
										the_content();
									} else {
										echo '<p>No description of ' . get_the_title() . ' available at this time. Check back soon!</p>';
									}; ?>
						</div>
						<div class="col-xs-12 ybd-sb-pet-share sb-yb-petdetail-margin">
							<!-- share block -->
							<h3 class="ybd-sb-h3 inline-block">Share This Pet</h3>
							<div class="addthis_inline_share_toolbox inline-block"></div>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
					<!-- donate block -->
					<?php $shopify_ids = get_animal_type_js_ids($animalType[0]->slug); ?>
					<div class="shopify-donate-block-container">
						<div id='product-component-<?php echo $shopify_ids['component']; ?>'></div>
					</div>
					<script type="text/javascript">
						/*<![CDATA[*/
						(function() {
							var scriptURL = 'https://sdks.shopifycdn.com/buy-button/latest/buy-button-storefront.min.js';
							if (window.ShopifyBuy) {
								if (window.ShopifyBuy.UI) {
									ShopifyBuyInit();
								} else {
									loadScript();
								}
							} else {
								loadScript();
							}

							function loadScript() {
								var script = document.createElement('script');
								script.async = true;
								script.src = scriptURL;
								(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(script);
								script.onload = ShopifyBuyInit;
							}

							function ShopifyBuyInit() {
								var client = ShopifyBuy.buildClient({
									domain: 'bc-spca.myshopify.com',
									storefrontAccessToken: '897a09ea6eb7efb7ec2d635f6df9f306',
								});

								ShopifyBuy.UI.onReady(client).then(function(ui) {
									ui.createComponent('product', {
										id: [<?php echo $shopify_ids['id']; ?>],
										node: document.getElementById('product-component-<?php echo $shopify_ids['component']; ?>'),
										moneyFormat: '%24%7B%7Bamount%7D%7D',
										options: {
											"product": {
												"variantId": "all",
												"contents": {
													"imgWithCarousel": false,
													"variantTitle": false,
													"description": false,
													"buttonWithQuantity": false,
													"quantity": false
												},
												"text": {
													"button": "DONATE NOW"
												},
												"styles": {
													"product": {
														"@media (min-width: 601px)": {
															"max-width": "100%",
															"margin-left": "0",
															"margin-bottom": "50px",
														}
													},
													"button": {
														"background-color": "#c23b13",
														":hover": {
															"background-color": "#af3511"
														},
														":focus": {
															"background-color": "#af3511"
														},
														"font-weight": "bold"
													},
													"compareAt": {
														"font-size": "12px"
													}
												}
											},
											"cart": {
												"contents": {
													"button": true
												},
												"text": {
													"total": "Total",
													"button": "COMPLETE DONATION"
												},
												"styles": {
													"button": {
														"background-color": "#c23b13",
														":hover": {
															"background-color": "#af3511"
														},
														":focus": {
															"background-color": "#af3511"
														},
														"font-weight": "normal"
													},
													"footer": {
														"background-color": "#ffffff"
													}
												}
											},
											"modalProduct": {
												"contents": {
													"img": false,
													"imgWithCarousel": true,
													"variantTitle": false,
													"buttonWithQuantity": true,
													"button": false,
													"quantity": false
												},
												"styles": {
													"product": {
														"@media (min-width: 601px)": {
															"max-width": "100%",
															"margin-left": "0px",
															"margin-bottom": "0px"
														}
													},
													"button": {
														"background-color": "#c23b13",
														":hover": {
															"background-color": "#af3511"
														},
														":focus": {
															"background-color": "#af3511"
														},
														"font-weight": "normal"
													}
												}
											},
											"toggle": {
												"styles": {
													"toggle": {
														"background-color": "#c23b13",
														":hover": {
															"background-color": "#af3511"
														},
														":focus": {
															"background-color": "#af3511"
														},
														"font-weight": "normal"
													}
												}
											},
											"productSet": {
												"styles": {
													"products": {
														"@media (min-width: 601px)": {
															"margin-left": "-20px"
														}
													}
												}
											}
										}
									});
								});
							}
						})();
						/*]]>*/
					</script>
				</div>
			</div>

			<br /><br />

			<div class="row equal-heights">
				<div class="col-lg-6 col-md-6 col-xs-12">
					<div class="ybd-sb-pet-details-block">
						<h2 class="ybd-sb-h2">About Me</h2>
						<ul>
							<li><span class="bold">Days in care: </span><?php echo $animalDaysInCare; ?></li>
							<li><span class="bold">Approximate age: </span><?php echo $animalAge; ?></li>
							<li><span class="bold">Pet Type: </span><span class="pet-type"><?php echo $animalType[0]->name; ?></span></li>
							<li><span class="bold">Breed: </span><?php echo $animalBreed[0]->name; ?></li>
							<li><span class="bold">Colour: </span><?php echo esc_html($animalColour); ?></li>
							<?php if (isset($animalSize[0]->name)) { ?>
								<li><span class="bold">Approximate Size: </span><?php echo $animalSize[0]->name; ?></li>
							<?php } ?>
							<li><span class="bold">Weight: </span><?php echo $animalWeight . ' kg'; ?></li>
							<?php if ( isset($animalSex[0]->name)) { ?>
								<li><span class="bold">Gender: </span><?php echo $animalSex[0]->name; ?></li>
							<?php } ?>
							<li><span class="bold">Location: </span><a href="#" data-toggle="modal" data-target="#whatsNext"><?php echo $animalLocation->post_title . ', ' . $city . ' B.C.'; ?></a> <span class="distance-away"></span></li>
							<li><span class="bold">Animal ID: </span><?php echo esc_html($animalID); ?></li>
							<?php if ($animalBonded != 0) { ?>
								<?php $bonded_pet = ybd_get_pet_from_animal_id($animalBonded); ?>
								<li><span class="bold">Bonded With: </span><a href="<?php echo $bonded_pet->guid; ?>"><?php echo $bonded_pet->post_title; ?></a></li>
							<?php } ?>
							<?php if ($animalType[0]->name == 'Cat' || $animalType[0]->name == 'Dog' || $animalType[0]->name == 'Rabbit') { ?>
								<li>Spay or neuter surgery is included with the adoption of all dogs, cats and rabbits.</li>
							<?php } ?>
							<li><a href="#" data-toggle="modal" data-target="#adoptionFees"><strong>Adoption fees<strong></a></li>
						</ul>
					</div>
				</div>
				<div class="col-lg-6 col-md-6 col-xs-12">
					<div class="ybd-sb-pet-details-block ybd-sb-pet-compatibility">
						<h2 class="ybd-sb-h2">Compatibility</h2>
						<span class="small">Important: The history and compatibility of an animal is not always known. If certain compatibility criteria have been identified, they will be listed here or in the animal's description.</span>
						<ul class="ybd-sb-compatibility-list">
							<?php if ($any_are_true = array_search('true', $compat)) { ?>
								<?php if ($compat['staff_pick'] == 'true') : ?>
									<li>
										<span class="ybd-sb-compatibility-icon"><img src="<?php echo get_stylesheet_directory_uri() .  '/img/compat-staff_pick.png'; ?>" alt="Staff Pick"></span>
										<span class="ybd-sb-compatibility-title">Staff Pick</span>
									</li>
								<?php endif; ?>
								<?php if ($compat['special_needs'] == 'true') : ?>
									<li>
										<span class="ybd-sb-compatibility-icon"><img src="<?php echo get_stylesheet_directory_uri() .  '/img/compat-staff_pick.png'; ?>" alt="Special Needs"></span>
										<span class="ybd-sb-compatibility-title">Special Needs</span>
									</li>
								<?php endif; ?>
								<?php if ($compat['special_fee'] == 'true') : ?>
									<li>
										<span class="ybd-sb-compatibility-icon"><img src="<?php echo get_stylesheet_directory_uri() .  '/img/compat-staff_pick.png'; ?>" alt="Special Fee"></span>
										<span class="ybd-sb-compatibility-title">Special Fee</span>
									</li>
								<?php endif; ?>
								<?php if ($compat['featured_animal'] == 'true') : ?>
									<li>
										<span class="ybd-sb-compatibility-icon"><img src="<?php echo get_stylesheet_directory_uri() .  '/img/compat-staff_pick.png'; ?>" alt="Featured Animal"></span>
										<span class="ybd-sb-compatibility-title">Featured Animal</span>
									</li>
								<?php endif; ?>
								<?php if ($compat['indoor_only'] == 'true') : ?>
									<li>
										<span class="ybd-sb-compatibility-icon"><img src="<?php echo get_stylesheet_directory_uri() .  '/img/compat-indoor.png'; ?>" alt="Indoor Only"></span>
										<span class="ybd-sb-compatibility-title">Indoor Only</span>
									</li>
								<?php endif; ?>
								<?php if ($compat['indoor_outdoor'] == 'true') : ?>
									<li>
										<span class="ybd-sb-compatibility-icon"><img src="<?php echo get_stylesheet_directory_uri() .  '/img/compat-indoor_outdoor.png'; ?>" alt="Indoor / Outdoor"></span>
										<span class="ybd-sb-compatibility-title">Indoor / Outdoor</span>
									</li>
								<?php endif; ?>
								<?php if ($compat['house_trained'] == 'true') : ?>
									<li>
										<span class="ybd-sb-compatibility-icon"><img src="<?php echo get_stylesheet_directory_uri() .  '/img/compat-checkmark.png'; ?>" alt="House Trained"></span>
										<span class="ybd-sb-compatibility-title">House Trained</span>
									</li>
								<?php endif; ?>
								<?php if ($compat['ok_with_cats'] == 'true') : ?>
									<li>
										<span class="ybd-sb-compatibility-icon"><img src="<?php echo get_stylesheet_directory_uri() .  '/img/compat-checkmark.png'; ?>" alt="OK With Cats"></span>
										<span class="ybd-sb-compatibility-title">OK With Cats</span>
									</li>
								<?php endif; ?>
								<?php if (!empty($compat['ok_with_dogs']) && $compat['ok_with_dogs'] == 'true') : ?>
									<li>
										<span class="ybd-sb-compatibility-icon"><img src="<?php echo get_stylesheet_directory_uri() .  '/img/compat-checkmark.png'; ?>" alt="OK With Dogs"></span>
										<span class="ybd-sb-compatibility-title">OK With Dogs</span>
									</li>
								<?php endif; ?>
								<?php if (!empty($compat['long_term_resident']) && $compat['long_term_resident'] == 'true') : ?>
									<li>
										<span class="ybd-sb-compatibility-icon"><img src="<?php echo get_stylesheet_directory_uri() .  '/img/compat-checkmark.png'; ?>" alt="Longterm Resident"></span>
										<span class="ybd-sb-compatibility-title">Longterm Resident</span>
									</li>
								<?php endif; ?>
								<?php if ($compat['lived_with_kids'] == 'true') : ?>
									<li>
										<span class="ybd-sb-compatibility-icon"><img src="<?php echo get_stylesheet_directory_uri() .  '/img/compat-checkmark.png'; ?>" alt="Lived With Kids"></span>
										<span class="ybd-sb-compatibility-title">Lived With Kids</span>
									</li>
								<?php endif; ?>
								<?php if ($compat['good_with_kids'] == 'true') : ?>
									<li>
										<span class="ybd-sb-compatibility-icon"><img src="<?php echo get_stylesheet_directory_uri() .  '/img/compat-checkmark.png'; ?>" alt="Good With Kids"></span>
										<span class="ybd-sb-compatibility-title">Good With Kids</span>
									</li>
								<?php endif; ?>
								<?php if ($compat['no_cats'] == 'true') : ?>
									<li>
										<span class="ybd-sb-compatibility-icon"><img src="<?php echo get_stylesheet_directory_uri() .  '/img/compat-warning.png'; ?>" alt="No Cats"></span>
										<span class="ybd-sb-compatibility-title">No Cats</span>
									</li>
								<?php endif; ?>
								<?php if ($compat['no_dogs'] == 'true') : ?>
									<li>
										<span class="ybd-sb-compatibility-icon"><img src="<?php echo get_stylesheet_directory_uri() .  '/img/compat-warning.png'; ?>" alt="No Dogs"></span>
										<span class="ybd-sb-compatibility-title">No Dogs</span>
									</li>
								<?php endif; ?>
								<?php if ($compat['no_small_children'] == 'true') : ?>
									<li>
										<span class="ybd-sb-compatibility-icon"><img src="<?php echo get_stylesheet_directory_uri() .  '/img/compat-warning.png'; ?>" alt="No Small Children"></span>
										<span class="ybd-sb-compatibility-title">No Small Children</span>
									</li>
								<?php endif; ?>
								<?php if ($compat['not_compat_livestock'] == 'true') : ?>
									<li>
										<span class="ybd-sb-compatibility-icon"><img src="<?php echo get_stylesheet_directory_uri() .  '/img/compat-warning.png'; ?>" alt="Not Compatible With Livestock"></span>
										<span class="ybd-sb-compatibility-title">Not Compatible With Livestock</span>
									</li>
								<?php endif; ?>
								<?php if ($compat['not_good_with_strangers'] == 'true') : ?>
									<li>
										<span class="ybd-sb-compatibility-icon"><img src="<?php echo get_stylesheet_directory_uri() .  '/img/compat-warning.png'; ?>" alt="Not Good With Strangers"></span>
										<span class="ybd-sb-compatibility-title">Not Good With Strangers</span>
									</li>
								<?php endif; ?>
							<?php } ?>
						</ul>
					</div>
				</div>
			</div>

			<br /><br /><br /><br />
			
			<div class="container">
				<div class="row">
					<div class="col-xs-12 text-center">
						<h3 class="ybd-sb-h2">Are you ready to adopt <?php the_title(); ?>?</h3>
						<a href="#" class="btn btn-green" data-toggle="modal" data-target="#whatsNext">What's Next?</a>
					</div>
				</div>
			</div>

			<div class="container nearby-container" style="display:none" data-type="<?php echo $animalType[0]->slug; ?>">
			<br /><br /><br /><br />
				<div class="row" id="other-pets-nearby">
					<div class="col-xs-12 text-center">
						<h2 class="ybd-sb-h2">More <?php echo ucwords(ybd_plural_pet_type($animalType[0]->slug)); ?> Near You</h2>
					</div>
					<div class="col-xs-12">
						<div class="ybd-nearby-carousel dale-carousel owl-carousel owl-theme">
							<!-- ajax populated -->
						</div>
					</div>
				</div>
			</div>

			<?php get_template_part('partials/recently', 'viewed-pets'); ?>

			<br /><br />

			<div class="row" id="learn-more">
				<div class="col-xs-12 text-center">
					<h2 class="ybd-sb-h2">Learn More</h2>
				</div>
				<div class="col-xs-12">
					<div class="ybd-learn-more-carousel dale-carousel owl-carousel owl-theme">
						<?php 
							for ( $x = 0; $x <= 8; $x++) { 
								get_template_part('partials/faq', 'card'); 
							}
						?>
					</div>
				</div>
			</div>

			<br /><br />

			<!-- START What's Next modal -->
			<div class="modal fade" id="whatsNext" tabindex="-1" role="dialog" aria-labelledby="whatsNextLabel">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title" id="whatsNextLabel">What's Next?</h4>
						</div>
						<div class="modal-body">
							<div class="container-fluid metadata">
<!--								<div class="row">-->
<!--									<div class="col-xs-12 col-sm-2 text-right">Step 1:</div>-->
<!--									<div class="col-xs-12 col-sm-10 large-text"><p><a href="https://spca.bc.ca/adoption/how-to-adopt/" target="_blank">Learn how to adopt from the BC SPCA</a></p></div>-->
<!--								</div>-->
								<div class="row">
<!--									<div class="col-xs-12 col-sm-2 text-right">Step 2:</div>-->
                                    <div class="col-xs-12 col-sm-10 large-text"><p>To adopt <?php echo get_the_title($post_id); ?> (Animal ID: <?php echo esc_html($animalID); ?>): <a href="https://spca.bc.ca/adoption/how-to-adopt/?_ga=2.123459019.1913543065.1584462249-46241415.1582747516" target="_blank">Visit "How to Adopt" and apply online here.</a></p></div>
								</div>
								<br />
                                <div class="row location-hours">
                                    <div class="col-xs-12 col-sm-4">
                                        <strong><?php echo esc_html($animalLocation->post_title); ?></strong><br />
                                        <?php echo esc_html($address); ?><br />
                                        <?php echo esc_html($city . ', B.C.'); ?><br />
                                        <?php echo esc_html($postal); ?><br />
                                        <a href="tel:1<?php echo esc_html($phone); ?>"><?php echo esc_html('1-'.$phone); ?></a>
                                    </div>
                                    <div class="col-xs-12 col-sm-8">
                                        <strong>Hours of Operation:</strong><br />
                                        *Visits to all shelters and services are by appointment.<br />
                                        Please contact the shelter directly.
                                        <br/><br />
										<?php 
											$slug = strtolower( preg_replace( '/[^a-z]+/i', '-', $animalLocation->post_title ) );
											$slug = explode('-', $slug );
											// ybd_debug_log($slug[0] . '-' . $slug[1]);
											$main_site_location_hours = get_remote_location_data($slug[0] . '-' . $slug[1]);
											
											if ( !empty( $main_site_location_hours ) ){ 
												foreach( $main_site_location_hours as $hours ){ 
													echo $hours->days . ' ' . $hours->hours . '<br />';
												} 
											} 
										?>
									</div>
								</div>
							</div>
							<!-- google map of shelter location -->
							<div class="google-map-container">
								<br />
								<iframe src="https://www.google.com/maps/embed/v1/place?zoom=15&key=AIzaSyBn7NbQS1SfesaAF8En2vhcjDRwN1cg4I8&q=<?php echo 'SPCA' . '+' . $esc_address . '+' . $esc_postal; ?>&center=<?php echo $latitude . ',' . $longitude; ?>" width="100%" height="300" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
							</div>
							<!-- call and email buttons -->
							<div class="modal-contact-container">
								<a href="tel:1<?php echo esc_html($phone); ?>">Call</a>
								<a href="mailto:<?php echo esc_html($email); ?>">Email</a>
							</div>
							<p class="modal-disclaimer">
								Please understand that we are unable to put animals on HOLD over the phone, you must meet the animal and visit us in person to ensure you are the right match! Please also realize, even though this website is live and updates frequently,
								we cannot ensure the animal is still available for adoption when you arrive at the shelter. There is a chance the animal you have an interest in may have been adopted by the time you arrive, or another party might be going through the
								adoption process at that present time.
							</p>
						</div>
					</div>
				</div>
			</div>
			<!-- END What's Next modal -->


			<!-- START Adoption Fees Modal -->
			<div class="modal fade" id="adoptionFees" tabindex="-1" role="dialog" aria-labelledby="AdoptionFees">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title" id="AdoptionFees"><?php echo esc_html($animalLocation->post_title); ?> Adoption Fees</h4>
						</div>
						<div class="modal-body text-center">
							<img src="https://spca.bc.ca/wp-content/uploads/<?php echo $animalLocation->post_name; ?>-adoptionfees.jpg" alt="<?php echo esc_html($animalLocation->post_title); ?> Adoption Fees Chart" />
							<p class="small text-left">*Fees vary by location and are subject to change without notice. Adoption fees are subject to PST (Provincial Sales Tax), which will be added at the time of adoption.<br /><br />All BC SPCA cat/kitten/dog/puppy/rabbit adoptions include a microchip and lifetime registration with the BC Pet Registry.<br /><br />Registration for lifetime protection is $45 for cats/kittens/dogs/puppies and $11.25 for rabbits, which will be added to the adoption fee. For more information please visit <a href="https://bcpetregistry.ca" target="_blank">bcpetregistry.ca</a></p>
						</div>
					</div>
				</div>
			</div>
			<!-- END What's Next modal -->
		</div>

	<?php endwhile; ?>

<?php else : ?>

	<h3 class="no-pets">Sorry, this pet is no longer available.</h3>

<?php endif; ?>

<?php get_footer(); ?>