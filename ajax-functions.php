<?php

/**
 * Check is the user was added to luminate, and update the status
 *
 * @return void
 */
function ybd_ajax_update_luminate(){
	$user_id = $_POST['user_id'];
	$lo_status = update_user_meta($user_id, '_base_user_luminate_is_subscribed', 'true');

	if ( $lo_status !== false ) {
		echo 'lo status is true.';
	}

	die();
}

add_action( 'wp_ajax_ybd_update_luminate', 'ybd_ajax_update_luminate' );

/**
 * Return a number of pets from a larger chunk that are sorted by distance from the user.
 *
 * @param 	integer 	$num 	The number of pets to return
 * @return 	Array				A slice representing the closest of 100 pets that were randomly selected and sorted by distance from the user.
 */
function ybd_ajax_get_nearby_pets(){

	// passed user location info for the query
	$location = (object) $_POST['location'];

	// Set the number of pets to fetch (12)
	$num = 12;

	// If there's a different number to show, override
	if (isset($_POST['num'])){
		$num = $_POST['num'];
	}

	// Get 50 random pets, and we'll sort them by distance from the user.
	$args = array(
		'post_type' 		=> 'pets',
		'posts_per_page' 	=> 50
	);

	if ( isset($_POST['pet_type']) ) {
		$args['tax_query'] = array(
			array(
			'taxonomy' => 'pet_types',
			'field' => 'slug',
			'terms' => $_POST['pet_type']
			)
		);
	}

	$query = new WP_Query( $args );

	if ( count($query->posts) < 4 ) {
		echo 0;
		die();
	}

	shuffle($query->posts);

	foreach ($query->posts as $key => $post ) {
		$distance = Ybd_Shelterbuddy_Public::ybd_get_distance_from_user_to_pet($post->ID, $location);
		if ( !empty( $distance ) ) {
			$post->distance = $distance;	
		} else {
			unset($query->posts[$key]);
		}
	}

	array_values($query->posts);

	// // Sort the array of pet ids according to distance from the cookie location
	usort( $query->posts, function( $a, $b ) {
		return $a->distance < $b->distance ? -1 : 1;
	});


	// Trim down the temp array to x pets
	$query->posts = array_slice($query->posts, 0, $num);
	$query->post_count = $num;

	while( $query->have_posts() ) : $query->the_post();

	get_template_part('partials/pet', 'card');

	endwhile;

	wp_reset_postdata();

	die();

}

add_action( 'wp_ajax_nopriv_ajax_nearby_pets', 'ybd_ajax_get_nearby_pets' );
add_action( 'wp_ajax_ajax_nearby_pets', 'ybd_ajax_get_nearby_pets' );

/**
 * The main load more posts magic, that runs on the homepage and the pet category pages to load in more pets
 *
 * @return void
 */
function ybd_ajax_frontpage_pets() {	

	// passed user location info for the query
	$location = (object) $_POST['location'];

	// ybd_debug_log($location);
	
	// passed arguments for the query
	$args = json_decode( stripslashes( $_POST['query'] ), true );
	$args['posts_per_page'] = 350;

	// delete_transient(sanitize_title($location->city) . '_sorted_posts');

	if ( !empty( $location ) ) {

		if ( false === ( $query = get_transient( sanitize_title($location->city) . '_sorted_posts' ) ) ) {
			$query = new WP_Query( $args );
		
			foreach($query->posts as &$post) {
				$post->distance = Ybd_Shelterbuddy_Public::ybd_get_distance_from_user_to_pet($post->ID, $location );
			}

			usort( $query->posts, function( $a, $b ) {
				return $a->distance < $b->distance ? -1 : 1;
			});

			set_transient( sanitize_title($location->city) . '_sorted_posts', $query, 60 * MINUTE_IN_SECONDS );
			ybd_debug_log('set transient for ' . $location->city . ' pet sorting.');
		}

	} else {
		$query = new WP_Query( $args );
	}

	$num_posts = 9;

	$current_page = $_POST['count'];

	$total_pages = ceil(count($query->posts) / $num_posts);

	// Trim down the array to the next $num_posts pets to show
	$query->posts = array_slice($query->posts, $current_page * $num_posts, $num_posts);
	$query->post_count = count($query->posts);

	$counter = 0;
	
	$random_numbers = ybd_random_numbers_from_range(3, $query->post_count - 1);

	while( $query->have_posts() ) : $query->the_post();

		get_template_part('partials/pet', 'card');

		// random promo post or medical post
		if ( in_array( $counter, $random_numbers ) ) {
			$value = rand(0, 2);
			if ($value != 1) {
				if ( $value == 0 ) {
					get_template_part('partials/faq', 'card');
				} else {
					get_template_part('partials/promo', 'card');
				}
			} else {
				// get_template_part('partials/promo', 'card');
				get_template_part('partials/medical', 'emergency');
			}
		}

		$counter++;

		endwhile;

		if ($current_page == --$total_pages) {
			echo '<!-- no_more_posts -->';
		}
		
	die();
	
}

add_action( 'wp_ajax_nopriv_frontpage_pets', 'ybd_ajax_frontpage_pets' );
add_action( 'wp_ajax_frontpage_pets', 'ybd_ajax_frontpage_pets' );

/**
 * The search query magic, returns pets using ajax that match the query params
 *
 * @return void
 */
function ybd_ajax_load_query_pets() {

	$num_posts = 17;

	$sortBy = $_POST['sortby'];

	// passed user location info for the query
	$location = (object) $_POST['location'];
	
	// passed arguments for the query
	$args = json_decode( stripslashes( $_POST['query'] ), true );
	$args['posts_per_page'] = 350;

	switch ($sortBy) {
		case 'time-newest':
			$args['meta_key'] = '_base_pets_days_in_care';
		 	$args['orderby'] = 'meta_value_num';
			$args['order'] = 'DESC';
			break;
		case 'time-longest':
			$args['meta_key'] = '_base_pets_days_in_care';
			$args['orderby'] = 'meta_value_num';
			$args['order'] = 'ASC';
			break;
		case 'age-youngest':
			$args['meta_key'] = '_base_pets_age_weeks';
		 	$args['orderby'] = 'meta_value_num';
			$args['order'] = 'ASC';			
			break;
		case 'age-oldest':
			$args['meta_key'] = '_base_pets_age_weeks';
		 	$args['orderby'] = 'meta_value_num';
			$args['order'] = 'DESC';			
			break;
		case 'weight-smallest':
			$args['meta_key'] = '_base_pets_weight';
		 	$args['orderby'] = 'meta_value_num';
			$args['order'] = 'ASC';				
			break;
		case 'weight-largest':
			$args['meta_key'] = '_base_pets_weight';
		 	$args['orderby'] = 'meta_value_num';
			$args['order'] = 'DESC';				
			break;
		default:
			// ybd_debug_log($sortBy);
	}		

	// Stringify the arguments
	// $str = json_encode($args);
	// // Make them an md5 so we can have a signature for caching them
	// $md5 = md5($str);

	// if ( false === ( $query = get_transient( $md5 . '_sorted_search' ) ) ) {

		$query = new WP_Query( $args );
		// set_transient( $md5 . '_sorted_search', $query, 60 * MINUTE_IN_SECONDS );
		// ybd_debug_log('Set transient for ' . $md5 . '_sorted_search.');

	// }

	$current_page = $_POST['count'];
	$total_pages = ceil(count($query->posts) / $num_posts);

	if ( !empty( $location ) && $sortBy == 'distance' ) {
		foreach($query->posts as &$post) {
			$post->distance = Ybd_Shelterbuddy_Public::ybd_get_distance_from_user_to_pet($post->ID, $location );
		}

		usort( $query->posts, function( $a, $b ) {
			return $a->distance < $b->distance ? -1 : 1;
		});
	}

	if ( count( $query->posts ) > $num_posts ) {
		// Trim down the array to the next X pets to show
		if ( isset($_POST['paginated']) && $_POST['paginated'] == true ){
			$query->posts = array_slice($query->posts, $current_page * $num_posts, $num_posts);
			$query->post_count = count($query->posts);
		}
	}

	$counter = 0;

	$random_numbers = ybd_random_numbers_from_range(3, $query->post_count - 1);

	while ( $query->have_posts() ) : $query->the_post();

		get_template_part('partials/pet', 'card');

		// random promo post or medical post
		if ( in_array( $counter, $random_numbers ) ) {
			$value = rand(0, 2);
			if ($value != 1) {
				if ( $value == 0 ) {
					get_template_part('partials/faq', 'card');
				} else {
					get_template_part('partials/promo', 'card');
				}
			} else {
				// get_template_part('partials/promo', 'card');
				get_template_part('partials/medical', 'emergency');
			}
		}

		$counter++;

	endwhile;

	if ($current_page == --$total_pages) {
		echo '<!-- no_more_posts -->';
	}
		
	wp_reset_postdata();

	die();
}

add_action( 'wp_ajax_nopriv_load_query_pets', 'ybd_ajax_load_query_pets' );
add_action( 'wp_ajax_load_query_pets', 'ybd_ajax_load_query_pets' );


/**
 * Gets the available breeds from a provided set of post ids (pets). Used to poplate select drop down list on search alerts.
 *
 * @return Object An object with an array of pet terms
 */
function ybd_ajax_contextual_breeds() {
	
	if (isset($_POST['pet_type']) ) {
		if ( isset($_POST['show_all']) && $_POST['show_all'] == true ){
			$terms = get_terms( 'pet_breed', array( 'hide_empty' => false, 'description__like' => $_POST['pet_type']) );
			wp_send_json( $terms );
		}
		// Get all the post ids for the pet type
		$post_ids = ybd_get_all_pet_ids($_POST['pet_type']);
		// Get the terms for the ids
		$pet_breed_terms = wp_get_object_terms($post_ids, 'pet_breed', array( 'hide_empty' => false ));
		// Return the relevant breeds for the pet_type
		wp_send_json($pet_breed_terms);
	}

	die();
}

add_action('wp_ajax_nopriv_select_breeds', 'ybd_ajax_contextual_breeds');
add_action('wp_ajax_select_breeds', 'ybd_ajax_contextual_breeds');

/**
 * Returns pets that a visitor has recently viewed.
 *
 * @return html recently viewed pet card html markup 
 */
function ybd_ajax_recently_viewed() {

	// passed user recently viewed ids for the query
	$recently_viewed_ids = $_POST['recently_viewed'];

	// post__in should automatically rule out pets that don't exist anymore. 
	// The recently viewed pets in localstorage should self-heal over time by being spliced.
	$args = array(
		'post_type' 		=> 'pets',
		'posts_per_page'	=>	12,
		'no_found_rows'		=> true,
		'orderby'			=> 'post__in',
		'post__in'			=> $recently_viewed_ids
	);

	$query = new WP_Query( $args );

	if ( count($query->posts) > 0 ) {

		while( $query->have_posts() ) : $query->the_post();

		get_template_part('partials/pet', 'card');

		endwhile;

		wp_reset_postdata();

	} else {
		echo 0;
	}

	die();
}

add_action( 'wp_ajax_nopriv_recently_viewed_pets', 'ybd_ajax_recently_viewed' );
add_action( 'wp_ajax_recently_viewed_pets', 'ybd_ajax_recently_viewed' );

/**
 * Find the nearest shelter location to a user
 *
 * @return Object	Return an object with information about the nearest shelter location
 */
function ybd_ajax_nearest_location(){
	if (isset($_POST['location'])){
		$location = (object) $_POST['location'];
		$nearest_location = Ybd_Shelterbuddy_Public::ybd_get_shelters_by_distance($location);
		// ybd_debug_log($nearest_location);

		$latitude = get_post_meta($nearest_location->ID, '_location_latitude', true);
		$longitude = get_post_meta($nearest_location->ID, '_location_longitude', true);

		$phone = get_post_meta($nearest_location->ID, '_location_phone', true);
		$email = get_post_meta($nearest_location->ID, '_location_email', true);
		$address = str_replace(',','<br />', get_post_meta($nearest_location->ID, '_location_address', true));
		$city = get_post_meta($nearest_location->ID, '_location_city', true);
		$postal = get_post_meta($nearest_location->ID, '_location_postal_code', true);
		$map_postal = str_replace(' ', '', $postal);	

		$location_data = (object) array(
			'distance'		=> $nearest_location->distance,
			'latitude'		=> $latitude,
			'longitude'		=> $longitude,
			'name'			=> $nearest_location->post_title,
			'slug'			=> $nearest_location->post_name,
			'phone'			=> $phone,
			'email'			=> $email,
			'address'		=> $address,
			'city'			=> $city,
			'postal'		=> $postal,
			'esc_postal'	=> $map_postal,
		);

		echo json_encode($location_data);

	} else {
		echo 'Error: no location provided.';
	}

	die();

}

add_action( 'wp_ajax_nopriv_nearest_location', 'ybd_ajax_nearest_location' );
add_action( 'wp_ajax_nearest_location', 'ybd_ajax_nearest_location' );


function ybd_ajax_get_distance_between(){

	$from_location = explode(',', $_POST['from_location']);
	$to_location = explode(',', $_POST['to_location']);

	$distance = round( Ybd_Shelterbuddy_Public::get_distance_between_locations($from_location[0], $from_location[1], $to_location[0], $to_location[1]) / 1000 );
	
	echo $distance;

	die();
}

add_action( 'wp_ajax_distance_between', 'ybd_ajax_get_distance_between' );
add_action( 'wp_ajax_nopriv_distance_between', 'ybd_ajax_get_distance_between' );

function ybd_ajax_get_nav_links(){
	// What is the passed context of this animal profile
	$query = null;
	$context = $_POST['context'];
	$location = (object) $_POST['location'];
	$post_id = absint($_POST['post_id']);

	// If the passed context is default, we either arrived directly to this pet, 
	// or the visitor is just browsing around (not via featured pets, or via searching for pets)
	if ( !isset($context) || $context == 'default' ) {

		$args = array(
			'post_status'		=> 'publish',
			'post_type' 		=> 'pets',
			'order' 			=> 'ASC',
			'posts_per_page' 	=> -1,
			'no_found_rows'		=> true
		);
	
		// delete_transient(sanitize_title($location->city) . '_sorted_posts');

		if ( !empty( $location ) ) {
			if ( false === ( $query = get_transient( sanitize_title($location->city) . '_sorted_posts' ) ) ) {
				// ybd_debug_log('set transient for ' . $location->city . ' pet sorting.');
				$query = new WP_Query( $args );
			
				foreach($query->posts as &$post) {
					$post->distance = Ybd_Shelterbuddy_Public::ybd_get_distance_from_user_to_pet($post->ID, $location );
				}

				usort( $query->posts, function( $a, $b ) {
					return $a->distance < $b->distance ? -1 : 1;
				});

				set_transient( sanitize_title($location->city) . '_sorted_posts', $query, 60 * MINUTE_IN_SECONDS );
			}
		} else {
			$query = new WP_Query( $args );
		}

	}

	if ($context == 'featured') {
		$query = ybd_featured_pets();
	}

	if ($context == 'search'){
		$args = $_POST['query'];

		$query = new WP_Query( $args );

		if ( !empty( $location ) && !empty($query->posts) ) {
			foreach($query->posts as &$post) {
				$post->distance = Ybd_Shelterbuddy_Public::ybd_get_distance_from_user_to_pet($post->ID, $location );
			}

			usort( $query->posts, function( $a, $b ) {
				return $a->distance < $b->distance ? -1 : 1;
			});
		}		
	}

	// // Now do the magic to find the prev/next posts

	if ( $query->have_posts() ) {
		$nav = ybd_get_adjacent_pets($query->posts, $post_id);
		echo json_encode($nav);
	}

	die();
}

add_action( 'wp_ajax_ybd_nav_links', 'ybd_ajax_get_nav_links' );
add_action( 'wp_ajax_nopriv_ybd_nav_links', 'ybd_ajax_get_nav_links' );

function ybd_ajax_delete_user_account(){

	$status = 0;
	$message = 'There was an issue deleting your account. Log out, log back in, and retry.';

	check_ajax_referer( 'profile_ajax', 'security' );
	
	$user = wp_get_current_user();

	if ( current_user_can( 'manage_options' ) ) {
		wp_send_json( array(
			'status' => 0,
			'message' => 'Administrators cannot delete their own accounts.'
		) );
	}

	$deleted = wp_delete_user( $user->ID );

	if ( $deleted ) {
		$status = 1;
		$message = 'The account was successfully deleted.';
	}

	$data = (object) array(
		'status'	=> $status,
		'message'	=> $message
	);

	wp_send_json($data);

	die();
}

add_action( 'wp_ajax_delete_account', 'ybd_ajax_delete_user_account' );

function ybd_ajax_update_luminate_status(){

	$data = $_POST['data'];

	$status = 0;
	$message = 'There was an issue adding Luminate data to your profile, or there was no change to the status.';

	check_ajax_referer( 'profile_ajax', 'security' );

	if ( !isset( $data ) ) {
		wp_send_json( array(
			'status' 	=> 0,
			'message' 	=> 'Data from luminate online needs to be sent to process this request.'
		) );		
	}
	
	$user = wp_get_current_user();

	$update = update_user_meta($user->ID, '_base_user_luminate_is_subscribed', $data);

	if ( false !== $update ) {
		$status = 1;
		$message = 'The information was successfully added to the user profile.';
	}

	$data = (object) array(
		'status'	=> $status,
		'message'	=> $message
	);

	wp_send_json($data);

	die();
}

add_action( 'wp_ajax_update_luminate_status', 'ybd_ajax_update_luminate_status' );

/**
 * Process saved search delete requests
 *
 * @since    1.0.0
 */
function ybd_update_user_saved_searches() {
	// Check to make sure this user is logged in properly
	check_ajax_referer( 'profile_ajax', 'security' );

	// The current user making the request
	$user_id = get_current_user_id();

	// Get previous searches
	$searches = get_user_meta($user_id, '_base_search_group', true);

	// if there's no previous searches
	if ( !is_array( $searches ) ) {
		$searches = array();
	}

	// If a key is passed, it's delete request, remove the saved search if it exists
	if ( !empty( $searches ) && isset( $_POST['key'] ) ) {
		$key = $_POST['key'];

		array_splice($searches, $key, 1);
		
		update_user_meta($user_id, '_base_search_group', $searches);

		// We're done, bail out
		echo 'deleted saved search ' . $key . ' for user ' . $user_id;
		exit();
	}

	// If there's a url being passed, this is a new search to be saved
	if ( isset( $_POST['url'] ) ) {

		// Setup the pretty url string
		$pretty_url_str = '/type/';
		// Extra pet search details
		$detail_str = '';
		// The title for the search
		$title_str = '';

		// Parse the search string 
		parse_str($_POST['url'], $search);

		// ybd_debug_log($search);

		$type_text = str_replace(array('-', '_'), ' ', $search['type']);

		$breed_text = '';
		
		if ( !empty( $search['breed'] ) ){
			$breed_text .= ': ';
			$breeds = implode(', ', $search['breed']);
			$breeds = str_replace( array('-', '_'), ' ', $breeds );
			$breed_text .= ucwords($breeds);
		}

		// Construct the title string
		$title_str = ucwords( $type_text . $breed_text );

		// Construct the detail string, and remove the type part of the string for use with the pretty url
		foreach ( $search as $key => $value ) {
			if ($key != 'type' && $key != 'breed' && $key != 'weight' && $key != 'location' && $key != 'compat' && $value != 'any' && $value != ''){
				$detail_str .= ' (' . ucwords(str_replace(array('-', '_'), ' ', $key)) . ': ' . ucwords( str_replace(array('_base_pets_compatibility_','-', '_'), ' ', $value)) . ')';
			}

			if ( $key === 'compat') {
				$compats = ucwords(implode(', ', $search['compat']));
				$detail_str .= ' (' . ucwords(str_replace(array('-', '_'), ' ', $key)) . ': '  . ucwords(str_replace(array('_base_pets_compatibility_','-', '_'), ' ', $compats)) . ')';
			}
			
			if ( $key === 'location') {
				$locations = ucwords(implode(', ', $search['location']));
				$detail_str .= ' (' . ucwords(str_replace(array('-', '_'), ' ', $key)) . ': '  . str_replace(array('-', '_'), ' ', $locations) . ')';
			}

			if ($key === 'weight'){
				switch($value) {
					case 'class-1':
						$detail_str .= ' (Under 7 kg)';
					break;
					case 'class-2':
						$detail_str .= ' (Under 9 kg)';
					break;
					case 'class-3':
						$detail_str .= ' (Under 11.5 kg)';
					break;
					case 'class-4':
						$detail_str .= ' (Under 14 kg)';
					break;
					case 'class-5':
						$detail_str .= ' (Under 20.5 kg)';
					break;
					case 'class-6':
						$detail_str .= ' (21 - 28 kg)';
					break;
					case 'class-7':
						$detail_str .= ' (28 - 45 kg)';
					break;
					case 'class-8':
						$detail_str .= ' (Over 45 kg)';
					break;
				}
			}
			// change the type to a url fragment
			if ($key === 'type'){
				$pretty_url_str .= $value . '/';
				unset($search[$key]);
			}
		}

		// final construction of the pretty url
		$pretty_url_str .= '?' . http_build_query($search);
		

		// time() always uses GMT, so we want to make sure we use the timezone setting in WordPress
		$date = new DateTime(null, new DateTimeZone(get_option('timezone_string')));
		$array = array(
			'_base_search_form'		=> $_POST['url'],
			'_base_pretty_url'		=> $pretty_url_str,
			'_base_title'			=> $title_str,
			'_base_created_date'	=> $date->getTimestamp() + $date->getOffset(),
			'_base_date'			=> $date->getTimestamp() + $date->getOffset(),
			'_base_details'			=> $detail_str,
		);
		
		// Add the new search to the existing array of searches
		array_push($searches, $array );

		// update the user's settings 
		update_user_meta($user_id, '_base_search_group', $searches);

		// Send new signup search alert test email
		$user_info = get_userdata($user_id);
		$signup_email = new Ybd_Shelterbuddy_Emails();
		$signup_email->send_alert_signup($user_info->user_email, $pretty_url_str, $title_str, $detail_str);
		
		// Return the new item for display in JavaScript
		echo json_encode($array);
		
		exit();
	}

}

add_action( 'wp_ajax_update_saved_searches', 'ybd_update_user_saved_searches' );