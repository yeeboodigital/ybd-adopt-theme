<?php get_header(); ?>

<br />

<div class="container">
	<!-- start filters section -->
	<?php get_template_part('partials/filter', 'form'); ?>
</div>
<!-- end filters section -->

<!-- <?php get_template_part('partials/css', 'spinner'); ?> -->

<div class="container">
	<div id="primary" class="content-area">
		<div class="no-pets-found" style="display:none">
			<h3 class="no-pets">There are currently no animals available for adoption that meet the specific criteria that you have selected.<br /><br />For better results, try choosing fewer parameters, or set up an email alert instead.</h3>
			<br /><br />
			<?php if (is_user_logged_in()) {
				$context_class = ' trigger-save-search';
			} else {
				$context_class = ' trigger-account-modal';
			} ?>
			<center><button type="submit" name="search-alert" class="btn btn-green search-alert-button <?php echo $context_class; ?>">Create Email Alert</button></center>
		</div>

		<div class="container">
			<div class="row" id="query-pets"><!-- AJAX --></div>
		</div>

		<?php get_template_part('partials/css', 'spinner'); ?>

		<br />

		<button type="submit" class="btn ajax-load-more-query-button ybd-sb-btn-green" style="display:none">See More Pets</button>

		<?php get_template_part('partials/recently', 'viewed-pets'); ?>

		<br /><br />

		<div class="row" id="learn-more">
			<div class="col-xs-12 text-center">
				<h2 class="ybd-sb-h2">Learn More</h2>
			</div>
			<div class="col-xs-12">
				<div class="ybd-learn-more-carousel dale-carousel owl-carousel owl-theme">			
					<?php get_template_part('partials/news', 'card'); ?>
				</div>
			</div>
		</div>

	</div><!-- #primary -->
</div>

<?php

get_footer();
