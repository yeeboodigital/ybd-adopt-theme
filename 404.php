<?php get_header(); ?>

<br />

<div id="primary" class="content-area">
	<div class="container text-center">
		<div class="row">
			<div class="col-xs-offset-3 col-xs-2 col-sm-offset-0 col-sm-4">
				<i class="fas fa-dog"></i>
			</div>

			<div class="col-xs-2 col-sm-4">
				<i class="fas fa-cat"></i>
			</div>
			<div class="col-xs-2 col-sm-4">
				<i class="fas fa-horse"></i>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<br /><br />
				<h3 class="no-pets">Sorry this pet is no longer available. <a href="/">Browse all available pets.</a></h3>

				<br /><br />

				<a class="btn ybd-sb-btn-green" href="/">See More Pets</a>

				<br /><br />
			</div>
		</div>
	</div>
</div>

<?php get_footer();
