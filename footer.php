</div><!-- #content -->

<?php get_template_part('partials/ad', 'bar'); ?>

<div id="ybd-sb-theme-footer">
	<footer class="container-fluid footer-container" role="contentinfo">
		<div class="footer container">
			<div class="col-sm-8">
				<div class="row">
					<div class="menu-footer-menu-container">
						<ul class="nav nav-footer footer-links-container">
							<li id="menu-item-744" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-744">
								<a href="https://secure3.convio.net/bcspca/site/SPageServer?pagename=donate&utm_source=adoption&utm_campaign=donate">Donate</a>
							</li>
							<li id="menu-item-797" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-797">
								<a href="https://spca.bc.ca/about-us/locations/locations-list/">Locations</a>
							</li>
							<li id="menu-item-4114" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4114">
								<a href="https://www.spca.bc.ca/about-us/">About us</a>
							</li>
							<li id="menu-item-755" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-755">
								<a href="https://www.spca.bc.ca/about-us/careers/">Careers</a>
							</li>
							<li id="menu-item-756" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-756">
								<a href="https://www.spca.bc.ca/about-us/contact-us/">Contact</a>
							</li>
							<li id="menu-item-224" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-224">
								<a href="http://support.spca.bc.ca/site/UserLogin?=0&NEXTURL=PageNavigator%2Freg_welcome">Update subscription</a>
							</li>
						</ul>
					</div>
					<!-- #site-navigation -->
					<ul class="nav nav-social social-icons-container">
						<li>
							<a href="https://www.facebook.com/bcspca/" target="_blank">
								<i class="fab fa-2x fa-facebook-square"></i>
							</a>
						</li>
						<li>
							<a href="https://twitter.com/BC_SPCA" target="_blank">
								<i class="fab fa-2x fa-twitter-square"></i>
							</a>
						</li>
						<li>
							<a href="https://www.instagram.com/bcspca" target="_blank">
								<i class="fab fa-2x fa-instagram"></i>
							</a>
						</li>
						<li>
							<a href="https://www.youtube.com/user/bcspcabc?sub_confirmation=1" target="_blank">
								<i class="fab fa-2x fa-youtube-square"></i>
							</a>
						</li>
						<li>
							<a href="mailto:donations@spca.bc.ca" target="_blank">
								<i class="fas fa-2x fa-envelope-square"></i>
							</a>
						</li>
					</ul>
				</div>
				<div class="row">
					<form class="footer-subscribe-form col-md-6 col-sm-11" method="POST" action="https://support.spca.bc.ca/site/Survey">
						<input type="hidden" name="SURVEY_ID" id="SURVEY_ID" value="1221">
						<input type="hidden" name="cons_info_component" id="cons_info_component" value="t">
						<input type="hidden" name="3259_1221_2_12343" value="2001">
						<input type="hidden" name="3259_1221_2_12343" value="1021">
						<input type="hidden" name="3259_1221_2_12343" value="1081">
						<input type="hidden" name="3259_1221_2_12343" value="1341">
						<div style="display:none">
							<label for="denySubmit">Spam Control Text:</label>&nbsp;
							<input type="text" name="denySubmit" id="denySubmit" value="" alt="This field is used to prevent form submission by scripts.">&nbsp;Please leave this field empty
						</div>
						<div class="row">
							<div class="col-md-12">
								<img class="img-responsive hidden" src="<?php echo get_stylesheet_directory_uri() . '/img/logo-footer.jpg'; ?>" alt="BC SPCA Logo">
								<p><i class="fa fa-2x fa-newspaper-o"></i> Subscribe to our newsletter</p>
							</div>
							<div class="col-sm-6">
								<div class="form-group">
									<label for="cons_first_name">First Name</label>
									<input name="cons_first_name" class="form-control" type="text">
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group">
									<label for="cons_last_name">Last Name</label>
									<input name="cons_last_name" class="form-control" type="text">
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label for="cons_email">Email Address</label>
									<input class="form-control" name="cons_email" type="text">
								</div>
							</div>
							<div class="col-md-6">
								<button type="submit" name="ACTION_SUBMIT_SURVEY_RESPONSE" id="ACTION_SUBMIT_SURVEY_RESPONSE" value="Sign Up" class="button btn-green btn-block">Sign Up</button>
							</div>
						</div>
					</form>
				</div>
			</div>
			<div class="col-md-3 col-md-offset-1 col-sm-4 legal-container">
				<img class="img-responsive footer-logo" src="<?php echo get_stylesheet_directory_uri() . '/img/logo-footer.jpg'; ?>" alt="BC SPCA Logo">
				<div class="clearfix"></div>
				<p>&copy; <?php echo date('Y'); ?> The British Columbia Society for the Prevention of Cruelty to Animals (BC SPCA). <a href="https://www.spca.bc.ca/about-us/core-policies-standards/privacy-policy/">Privacy Policy</a></p>
				<p>Our mission is to protect and enhance the quality of life for domestic, farm and wild animals in BC. BC SPCA is a registered charity, tax # BN 11881 9036 RR0001</p>
				<img class="accredited-seal img-responsive" src="<?php echo get_stylesheet_directory_uri() . '/img/seal-accredited.png'; ?>" alt="Accredited by Imagine Canada">
			</div>
		</div>
	</footer>
	<!-- .footer-container -->
</div>

</div><!-- #page -->


<?php wp_footer(); ?>

<?php if ( !is_page('profile') ) { ?>
	<!-- Go to www.addthis.com/dashboard to customize your tools -->
	<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-55e0c2092e7bc60a"></script>
<?php } ?>

<?php get_template_part('partials/modal', 'map'); ?>
<?php get_template_part('partials/modal', 'login-register'); ?>
<?php get_template_part('partials/modal', 'fav-search-notice'); ?>
<?php if ( is_user_logged_in() ) { ?>
<?php get_template_part('partials/modal', 'search-alert'); ?>
<?php } ?>

  <!-- Facebook Pixel Code -->
  <script>
    !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
    n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
    document,'script','https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '875268265919905');
    fbq('track', 'PageView');
  </script>
  <noscript>
    <img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=875268265919905&ev=PageView&noscript=1"/> 
  </noscript>
  <!-- DO NOT MODIFY -->
  <!-- End Facebook Pixel Code -->
  <script>
  window.dataLayer = window.dataLayer || [];
  </script>
  <!-- Google Tag Manager -->
  <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
  new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
  j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
  'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
  })(window,document,'script','dataLayer','GTM-MXW2NB3');</script>
  <!-- End Google Tag Manager -->
  
  <!-- Google Maps -->
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDGoolx_YuO7tzUR7j1ZepHpH3Ywc6o9u4" async defer></script>

</body>

</html>