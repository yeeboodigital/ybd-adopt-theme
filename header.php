<!doctype html>
<html <?php language_attributes(); ?>>

<head>
	<meta charset="<?php bloginfo('charset'); ?>" />
	<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<meta charset="<?php bloginfo('charset'); ?>">
	<title><?php echo ybd_dynamic_title(); ?></title>
	<meta name="title" content="<?php echo ybd_dynamic_title(); ?>">
	<meta name="description" content="The BC SPCA rescues animals from abuse and works to protect and enhance the quality of life for all animals in B.C. Join our fight to end animal cruelty!">
	<link rel="profile" href="https://gmpg.org/xfn/11" />

	<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
	<meta name="msapplication-TileColor" content="#2d89ef">
	<meta name="theme-color" content="#ffffff">
	<meta property="fb:app_id" content="459584311587726">
	<meta property="og:locale" content="en_US">
	<meta property="og:type" content="website">
	<meta property="og:url" content="<?php echo get_the_permalink(); ?>">
	<!-- <meta property="og:site_name" content="BC SPCA"> -->
	<meta property="og:title" content="<?php echo ybd_dynamic_title(); ?>">
	<meta property="og:description" content="<?php if ( is_singular() ) { echo get_the_excerpt(); } else { echo 'The BC SPCA rescues animals from abuse and works to protect and enhance the quality of life for all animals in B.C. Join our fight to end animal cruelty!'; } ?>">
	<?php $first_image = ybd_get_first_attached_image(); ?>
	<meta property="og:image" content="<?php echo $first_image->url; ?>">
	<meta property="og:image:width" content="<?php echo $first_image->width; ?>">
	<meta property="og:image:height" content="<?php echo $first_image->height; ?>">

	<meta name="twitter:card" content="summary">
	<meta name="twitter:creator" content="@bc_spca">
	<meta name="twitter:site" content="@bc_spca">
	<meta name="twitter:title" content="<?php echo ybd_dynamic_title(); ?>">
	<meta name="twitter:description" content="<?php if ( is_singular() ) { echo get_the_excerpt(); } else { echo 'The BC SPCA rescues animals from abuse and works to protect and enhance the quality of life for all animals in B.C. Join our fight to end animal cruelty!'; } ?>">
	<meta name="twitter:image" content="<?php echo $first_image->url; ?>">

	<!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<?php if (is_search()) { ?>
		<meta name="robots" content="noindex, nofollow" />
	<?php } ?>

  <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-2739543-11','auto','adoptTracker');
    ga('adoptTracker.set', 'page', 'adoption-site');
    ga('adoptTracker.send', 'pageview');
  </script>

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<div id="page" class="site">


        <!-- Emergency Alert Starts -->
        <?php
        $emergencyAlertEnabled = (get_field('enable_emergency_alert_message', 'option')) ?? null;
        ?>

        <?php if (($emergencyAlertEnabled == "On") && (( !is_singular( 'donations' ) )) ) :?>

            <?php

            $emergencyCloseable = (get_field('emergency_alert_closeable', 'option')) ?? null;
            $emergencyCloseableRemember = (get_field('emergency_alert_remember_closed_value', 'option')) ?? null;

            $emergencyLink = get_field('emergency_alert_button', 'option') ?? null;
            $emergencyLink_url = $emergencyLink['url'] ?? null;
            $emergencyLink_title = $emergencyLink['title'] ?? null;
            $emergencyButtonColour = get_field('emergency_button_colour','option') ?? null;

            ?>

            <?php if ($emergencyCloseableRemember == "On") :?>

            <script type="text/javascript">

                /**
                 * js.Cookie, https://github.com/js-cookie/js-cookie
                 */
                !function(e){var n;if("function"==typeof define&&define.amd&&(define(e),n=!0),"object"==typeof exports&&(module.exports=e(),n=!0),!n){var t=window.Cookies,o=window.Cookies=e();o.noConflict=function(){return window.Cookies=t,o}}}(function(){function e(){for(var e=0,n={};e<arguments.length;e++){var t=arguments[e];for(var o in t)n[o]=t[o]}return n}function n(e){return e.replace(/(%[0-9A-Z]{2})+/g,decodeURIComponent)}return function t(o){function r(){}function i(n,t,i){if("undefined"!=typeof document){"number"==typeof(i=e({path:"/"},r.defaults,i)).expires&&(i.expires=new Date(1*new Date+864e5*i.expires)),i.expires=i.expires?i.expires.toUTCString():"";try{var c=JSON.stringify(t);/^[\{\[]/.test(c)&&(t=c)}catch(e){}t=o.write?o.write(t,n):encodeURIComponent(String(t)).replace(/%(23|24|26|2B|3A|3C|3E|3D|2F|3F|40|5B|5D|5E|60|7B|7D|7C)/g,decodeURIComponent),n=encodeURIComponent(String(n)).replace(/%(23|24|26|2B|5E|60|7C)/g,decodeURIComponent).replace(/[\(\)]/g,escape);var f="";for(var u in i)i[u]&&(f+="; "+u,!0!==i[u]&&(f+="="+i[u].split(";")[0]));return document.cookie=n+"="+t+f}}function c(e,t){if("undefined"!=typeof document){for(var r={},i=document.cookie?document.cookie.split("; "):[],c=0;c<i.length;c++){var f=i[c].split("="),u=f.slice(1).join("=");t||'"'!==u.charAt(0)||(u=u.slice(1,-1));try{var a=n(f[0]);if(u=(o.read||o)(u,a)||n(u),t)try{u=JSON.parse(u)}catch(e){}if(r[a]=u,e===a)break}catch(e){}}return e?r[e]:r}}return r.set=i,r.get=function(e){return c(e,!1)},r.getJSON=function(e){return c(e,!0)},r.remove=function(n,t){i(n,"",e(t,{expires:-1}))},r.defaults={},r.withConverter=t,r}(function(){})});

                jQuery( document ).ready(function() {

                    if( Cookies.get('emergencyAlertBoxStatus') === 'closed' ){
                        jQuery('#emergencyAlertBoxHeader').hide();
                    }

                    // Hook for close event
                    jQuery('#emergencyAlertBoxHeader').on('closed.bs.alert', function () {
                        Cookies.set('emergencyAlertBoxStatus', 'closed', { expires: 30 })
                    })

                });
            </script>

        <?php elseif($emergencyCloseableRemember == "Session") :?>
            <script type="text/javascript">

                /**
                 * js.Cookie, https://github.com/js-cookie/js-cookie
                 */
                !function(e){var n;if("function"==typeof define&&define.amd&&(define(e),n=!0),"object"==typeof exports&&(module.exports=e(),n=!0),!n){var t=window.Cookies,o=window.Cookies=e();o.noConflict=function(){return window.Cookies=t,o}}}(function(){function e(){for(var e=0,n={};e<arguments.length;e++){var t=arguments[e];for(var o in t)n[o]=t[o]}return n}function n(e){return e.replace(/(%[0-9A-Z]{2})+/g,decodeURIComponent)}return function t(o){function r(){}function i(n,t,i){if("undefined"!=typeof document){"number"==typeof(i=e({path:"/"},r.defaults,i)).expires&&(i.expires=new Date(1*new Date+864e5*i.expires)),i.expires=i.expires?i.expires.toUTCString():"";try{var c=JSON.stringify(t);/^[\{\[]/.test(c)&&(t=c)}catch(e){}t=o.write?o.write(t,n):encodeURIComponent(String(t)).replace(/%(23|24|26|2B|3A|3C|3E|3D|2F|3F|40|5B|5D|5E|60|7B|7D|7C)/g,decodeURIComponent),n=encodeURIComponent(String(n)).replace(/%(23|24|26|2B|5E|60|7C)/g,decodeURIComponent).replace(/[\(\)]/g,escape);var f="";for(var u in i)i[u]&&(f+="; "+u,!0!==i[u]&&(f+="="+i[u].split(";")[0]));return document.cookie=n+"="+t+f}}function c(e,t){if("undefined"!=typeof document){for(var r={},i=document.cookie?document.cookie.split("; "):[],c=0;c<i.length;c++){var f=i[c].split("="),u=f.slice(1).join("=");t||'"'!==u.charAt(0)||(u=u.slice(1,-1));try{var a=n(f[0]);if(u=(o.read||o)(u,a)||n(u),t)try{u=JSON.parse(u)}catch(e){}if(r[a]=u,e===a)break}catch(e){}}return e?r[e]:r}}return r.set=i,r.get=function(e){return c(e,!1)},r.getJSON=function(e){return c(e,!0)},r.remove=function(n,t){i(n,"",e(t,{expires:-1}))},r.defaults={},r.withConverter=t,r}(function(){})});

                jQuery( document ).ready(function() {

                    if( Cookies.get('emergencyAlertBoxStatus') === 'closed' ){
                        jQuery('#emergencyAlertBoxHeader').hide();
                    }

                    // Hook for close event
                    jQuery('#emergencyAlertBoxHeader').on('closed.bs.alert', function () {
                        Cookies.set('emergencyAlertBoxStatus', 'closed')
                    })

                });
            </script>

        <?php endif?>

            <style type="text/css">
                #emergencyAlertBoxHeader{
                    background-color: background: #000000;
                    background: -moz-linear-gradient(top, #000000 0%, #333333 61%);
                    background: -webkit-linear-gradient(top, #000000 0%,#333333 61%);
                    background: linear-gradient(to bottom, #000000 0%,#333333 61%);
                    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#000000', endColorstr='#333333',GradientType=0 );

                    position: -webkit-sticky; /* Safari */
                    position: sticky;
                    top:0;
                }
                #emergencyAlertBoxHeader button.close{
                    position: absolute;
                    top: 0;
                    right: 15px;
                }

                .btn-e-alert{
                    display: inline-block;
                    margin-bottom: 0;
                    font-weight: 400;
                    text-align: center;
                    vertical-align: middle;
                    -ms-touch-action: manipulation;
                    touch-action: manipulation;
                    cursor: pointer;
                    background-image: none;
                    border: 1px solid transparent;
                    white-space: nowrap;
                    padding: 6px 12px;
                    font-size: 18px;
                    line-height: 1.428571429;
                    border-radius: 4px;
                    -webkit-user-select: none;
                    -moz-user-select: none;
                    -ms-user-select: none;
                    user-select: none;
                }

                .btn-red {
                    color: #fff;
                    background-color: #c23b13
                }

                .btn-red.active,
                .btn-red.active.focus,
                .btn-red.active:focus,
                .btn-red.active:hover,
                .btn-red.focus,
                .btn-red:active,
                .btn-red:active.focus,
                .btn-red:active:focus,
                .btn-red:active:hover,
                .btn-red:focus,
                .btn-red:hover,
                .open>.btn-red.dropdown-toggle,
                .open>.btn-red.dropdown-toggle.focus,
                .open>.btn-red.dropdown-toggle:focus,
                .open>.btn-red.dropdown-toggle:hover {
                    color: #fff;
                    background-color: #98caec !important
                }

                .btn-red.active,
                .btn-red:active,
                .open>.btn-red.dropdown-toggle {
                    background-image: none
                }

                .btn-red.disabled.focus,
                .btn-red.disabled:focus,
                .btn-red.disabled:hover,
                .btn-red[disabled].focus,
                .btn-red[disabled]:focus,
                .btn-red[disabled]:hover,
                fieldset[disabled] .btn-red.focus,
                fieldset[disabled] .btn-red:focus,
                fieldset[disabled] .btn-red:hover {
                    background-color: #98caec !important
                }

                .btn-red .badge {
                    color: #c23b13;
                    background-color: #fff
                }
            </style>

            <div class="alert alert-danger alert-dismissible fade in" role="alert" style="padding-top:5px;padding-bottom:5px;margin-bottom: 0;width: 100%;z-index: 500;color: white;text-align: center;border: 0;border-radius: 0;" id="emergencyAlertBoxHeader">
                <?php if ($emergencyCloseable == "On") :?><button type="button" class="close" data-dismiss="alert" aria-label="Close" style="opacity:1;font-size:27px;text-shadow:none;"><span aria-hidden="true">×</span></button><?php endif?>
                <p style="margin-top: 15px;font-family: Lato, sans-serif;font-weight: bold;"><?php the_field('emergency_alert_text','option') ?></p>
                <p style="margin-top: 15px;margin-bottom: 15px;">
                    <a href="<?php echo esc_url($emergencyLink_url); ?>" style="font-size: 20px;text-decoration: none;" class="button btn-e-alert <?php if ($emergencyButtonColour == "Green") : ?>btn-green<?php elseif($emergencyButtonColour == "Red") :?>btn-red<?php endif ?>"><?php echo esc_html($emergencyLink_title);?></a>
                </p>
            </div>

        <?php endif ?>
        <!-- Emergency Alert Ends -->


		<div id="ybd-sb-theme-header">
			<!-- BEGIN location search panel -->
			<div class="collapse container-fluid header-panel location-search-container" id="locationSearchPanel">
				<div class="header-panel-container">
					<div class="col-md-12">
						<button type="button" class="close" data-toggle="collapse" href="#locationSearchPanel" aria-expanded="false" aria-controls="locationSearchPanel" aria-label="Close">
							<span aria-hidden="true">
								<span class="close-text">Close</span> ×</span>
						</button>
					</div>
					<div class="col-md-3 col-md-offset-3 header-form-title">
						<i class="fa fa-map-marker"></i> <span>Search by city<br>or postal code:</span>
					</div>
					<div class="col-md-6">
						<label for="" class="control-label">Search by city or postal code:</label>
						<div class="gmw-form-wrapper gmw-form-wrapper1 gmw-pt-form-wrapper gmw-pt-horizontal-gray-form-wrapper">
							<form class="gmw-form gmw-form-1" name="gmw_form" action="https://www.spca.bc.ca/programs-services/location-search/" method="get">
								<div class="address-locator-wrapper">
									<!-- Address Field -->
									<div id="gmw-address-field-wrapper-1" class="gmw-address-field-wrapper gmw-address-field-wrapper-1 ">
										<input type="text" name="gmw_address[]" id="gmw-address-1" autocomplete="off" class="gmw-address gmw-full-address gmw-address-1  " value="" placeholder="City or Postal Code...">
										<div class="gmw-locator-btn-wrapper gmw-locator-btn-within-wrapper">
											<i id="1" class="fa fa-map-marker gmw-locator-btn-within gmw-locator-button gmw-locate-btn gmw-locator-submit"></i><i id="gmw-locator-btn-loader-1" class="gmw-locator-btn-loader fa fa-refresh fa-spin" alt="Locator image loader" style="display:none;"></i>
										</div>
									</div>
									<!--  locator icon -->
								</div>
								<!-- post types dropdown -->
								<input type="hidden" id="gmw-single-post-type-1" class="gmw-single-post-type gmw-single-post-type-1 " name="gmw_post" value="locations">
								<!-- Display taxonomies/categories -->
								<!--distance values -->
								<select class="gmw-distance-select gmw-distance-select-1" name="gmw_distance">
									<option value="200">Kilometres</option>
									<option value="50">50</option>
									<option value="100">100</option>
									<option value="150">150</option>
									<option value="200">200</option>
								</select>
								<!--distance units-->
								<input type="hidden" name="gmw_units" value="metric">
								<div id="gmw-submit-wrapper-1" class="gmw-submit-wrapper gmw-submit-wrapper-1">
									<input type="hidden" id="gmw-form-id-1" class="gmw-form-id gmw-form-id-1" name="gmw_form" value="1">
									<input type="hidden" id="gmw-page-1" class="gmw-page gmw-page-1" name="paged" value="1">
									<input type="hidden" id="gmw-per-page-1" class="gmw-per-page gmw-per-page-1" name="gmw_per_page" value="25">
									<input type="hidden" id="prev-address-1" class="prev-address prev-address-1" value="">
									<input type="hidden" id="gmw-lat-1" class="gmw-lat gmw-lat-1" name="gmw_lat" value="">
									<input type="hidden" id="gmw-long-1" class="gmw-lng gmw-long-1" name="gmw_lng" value="">
									<input type="hidden" id="gmw-prefix-1" class="gmw-prefix gmw-prefix-1" name="gmw_px" value="pt">
									<input type="hidden" id="gmw-action-1" class="gmw-action gmw-action-1" name="action" value="gmw_post">
									<input type="submit" id="gmw-submit-1" class="gmw-submit gmw-submit-1" value="GO ">
								</div>
							</form>
						</div>
						<div class="row">
							<div class="col-xs-12">
								<p>
									<a href="https://spca.bc.ca/about-us/locations/locations-list/">View all locations</a>
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- END location search panel -->

			<!-- BEGIN site search panel -->
			<div class="collapse container-fluid header-panel site-search-container" id="siteSearchPanel">
				<div class="header-panel-container">
					<div class="col-md-12">
						<button type="button" class="close" data-toggle="collapse" href="#siteSearchPanel" aria-expanded="false" aria-controls="siteSearchPanel" aria-label="Close">
							<span aria-hidden="true"><span class="close-text">Close</span> &times;</span>
						</button>
					</div>
					<div class="col-md-3 col-md-offset-3 header-form-title">
						<i class="fa fa-search"></i> <span>Search our site:</span>
					</div>
					<div class="col-md-6">
						<form class="form-inline" role="search" method="get" action="https://www.spca.bc.ca/">
							<div class="form-group col-xs-7">
								<label for="header_search_desktop" class="control-label">What are you looking for?</label>
								<input type="search" id="header_search_desktop" class="form-control" placeholder="Search &hellip;" value="" name="s" />
							</div>
							<div class="form-group col-xs-2">
								<div>&nbsp;</div>
								<button type="submit" class="button btn-blue">
									GO <i class="fa fa-search"></i>
								</button>
							</div>
						</form>
						<div class="row">
							<div class="col-xs-12">
								<p>
									Search <a href="https://adopt.spca.bc.ca">Adoptable Animals</a> or <a href="https://www.bcpetsearch.com/">Lost & Found Pets</a>
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- END site search panel -->
			<div id="filter-criteria-817283102" class="templateComponent" style="display: inline;"></div>
			<a class="button btn-red btn-block visible-xs mobile-header-donate" href="https://spca.bc.ca/donations/make-a-donation/?utm_source=adoptheader&utm_medium=button&utm_campaign=donate"><span class="">Donate </span><i class="fa fa-heart"></i></a>
			<div class="container">
				<header class="header">
					<!-- BEGIN desktop header nav -->
					<ul class="header-nav-container nav nav-pills pull-right">
						<li class="nav-number hidden-sm hidden-xs">
							<h4>Report Animal Cruelty: <span>1-855-622-7722</span></h4>
						</li>
						<li class="hidden-xs">
							<a class="btn btn-green collapsed panel-toggle" role="button" data-toggle="collapse" href="#siteSearchPanel" aria-expanded="false" data-parent="#page" aria-controls="siteSearchPanel">
								<div class="arrow-up yellow hidden-xs"></div>
								<span class="hidden-xs">Search </span><i class="fa fa-search"></i>
							</a>
						</li>
						<li class="hidden-xs">
							<a class="btn btn-green collapsed panel-toggle" role="button" data-toggle="collapse" href="#locationSearchPanel" aria-expanded="false" data-parent="#page" aria-controls="locationSearchPanel">
								<div class="arrow-up yellow hidden-xs"></div>
								<span class="hidden-xs">Find a Location </span><i class="fa fa-map-marker"></i>
							</a>
						</li>
						<li class="hidden-xs nav-donate">
							<a class="button btn-red" href="https://spca.bc.ca/donations/make-a-donation/?utm_source=adoptheader&utm_medium=button&utm_campaign=donate">
								<span class="">Donate </span><i class="fa fa-heart"></i>
							</a>
						</li>
					</ul>
					<!-- END desktop header nav -->
					<!-- BEGIN mobile header nav -->
					<ul class="visible-xs mobile-header-nav-container nav nav-pills pull-right" id="accordion1">
						<li>
							<a class="collapsed" role="button" data-toggle="collapse" href="#mobileContactPanel" aria-expanded="false" aria-controls="mobileContactPanel"">
                            <i class=" fa fa-phone"></i>
							</a>
						</li>
						<li>
							<a class="collapsed" role="button" data-toggle="collapse" href="#mobileSiteSearchPanel" aria-controls="mobileSiteSearchPanel"">
                            <i class=" fa fa-search"></i>
							</a>
						</li>
						<li>
							<a class="collapsed" role="button" data-toggle="collapse" href="#mobileLocationSearchPanel" aria-controls="mobileLocationSearchPanel"">
                            <i class=" fa fa-map-marker"></i>
							</a>
						</li>
						<li>
							<a class="navbar-toggle collapsed" role="button" data-toggle="collapse" href="#navbar" aria-expanded="false">
								<span class="sr-only">Toggle navigation</span>
								<span class="mobile-menu-closed">
									<i class="fa fa-bars"></i>
								</span>
								<span class="mobile-menu-opened">
									<i class="fa fa-close"></i>
								</span>
							</a>
						</li>
					</ul>
					<!-- END mobile header nav -->
					<a href="https://www.spca.bc.ca/">
						<img class="img-responsive" src="<?php echo get_stylesheet_directory_uri() . '/img/BCSPCA-125-NoTagline.jpg'; ?>" alt="BC SPCA Logo">
					</a>
				</header>
			</div>
			<!-- BEGIN mobile contact  -->
			<div class="collapse container-fluid header-panel mobile-contact-container text-center" id="mobileContactPanel" aria-expanded="false">
				<div class="col-xs-12">
					<div class="header-panel-title">
						<h2>Report Animal Cruelty:</h2>
						<h3>1-855-622-7722</h3>
					</div>
					<p>For all other calls and inquiries<br> <a href="https://spca.bc.ca/#mobileContactPanel">see our contact details.</a></p>
				</div>
			</div>
			<!-- END mobile contact  -->
			<!-- BEGIN mobile site search  -->
			<div class="collapse container-fluid header-panel mobile-site-search-container" id="mobileSiteSearchPanel" aria-expanded="false">
				<div class="col-xs-12">
					<form crole="search" method="get" action="https://www.spca.bc.ca/">
						<div class="form-group col-xs-12">
							<label for="header_search_mobile" class="control-label">What are you looking for?</label>
							<input type="search" id="header_search_mobile" class="form-control" placeholder="Search &hellip;" value="" name="s" />
						</div>
						<div class="form-group col-xs-12">
							<button type="submit" class="button btn-blue btn-block">
								GO <i class="fa fa-search"></i>
							</button>
						</div>
					</form>
					<div class="row">
						<div class="mobile-panel-link-container form-group col-xs-12">
							<p>Search <a href="https://adopt.spca.bc.ca">Adoptable Animals</a><br> or <a href="https://www.bcpetsearch.com/">Lost & Found Pets</a></p>
						</div>
					</div>
				</div>
			</div>
			<!-- END mobile site search  -->
			<!-- BEGIN mobile location search  -->
			<div class="collapse container-fluid header-panel mobile-location-search-container" id="mobileLocationSearchPanel" aria-expanded="false">
				<div class="col-xs-12">
					<div class="header-panel-title">
						<h3>Find a BC SPCA location in your area:</h3>
					</div>
					<div id="gmw_search_container">
						<div class="gmw-form-wrapper gmw-form-wrapper1 gmw-pt-form-wrapper gmw-pt-horizontal-gray-form-wrapper">
							<form class="gmw-form gmw-form-1" name="gmw_form" action="https://www.spca.bc.ca/programs-services/location-search/" method="get">
								<div class="address-locator-wrapper">
									<!-- Address Field -->
									<div id="gmw-address-field-wrapper-1" class="gmw-address-field-wrapper gmw-address-field-wrapper-1 ">
										<input type="text" name="gmw_address[]" id="gmw-address-1" autocomplete="off" class="gmw-address gmw-full-address gmw-address-1  " value="" placeholder="City or Postal Code..." />
										<div class="gmw-locator-btn-wrapper gmw-locator-btn-within-wrapper">
											<i id="1" class="fa fa-map-marker gmw-locator-btn-within gmw-locator-button gmw-locate-btn gmw-locator-submit"></i>
											<i id="gmw-locator-btn-loader-1" class="gmw-locator-btn-loader fa fa-refresh fa-spin" alt="Locator image loader" style="display:none;"></i>
										</div>
									</div>
									<!--  locator icon -->
								</div>
								<!-- post types dropdown -->
								<input type="hidden" id="gmw-single-post-type-1" class="gmw-single-post-type gmw-single-post-type-1 " name="gmw_post" value="locations" />
								<!-- Display taxonomies/categories -->
								<!--distance values -->
								<select class="gmw-distance-select gmw-distance-select-1" name="gmw_distance">
									<option value="200">Kilometres</option>
									<option value="50">50</option>
									<option value="100">100</option>
									<option value="150">150</option>
									<option value="200">200</option>
								</select>
								<!--distance units-->
								<input type="hidden" name="gmw_units" value="metric" />
								<div id="gmw-submit-wrapper-1" class="gmw-submit-wrapper gmw-submit-wrapper-1">
									<input type="hidden" id="gmw-form-id-1" class="gmw-form-id gmw-form-id-1" name="gmw_form" value="1" />
									<input type="hidden" id="gmw-page-1" class="gmw-page gmw-page-1" name="paged" value="1" />
									<input type="hidden" id="gmw-per-page-1" class="gmw-per-page gmw-per-page-1" name="gmw_per_page" value="25" />
									<input type="hidden" id="prev-address-1" class="prev-address prev-address-1" value="" />
									<input type="hidden" id="gmw-lat-1" class="gmw-lat gmw-lat-1" name="gmw_lat" value="" />
									<input type="hidden" id="gmw-long-1" class="gmw-lng gmw-long-1" name="gmw_lng" value="" />
									<input type="hidden" id="gmw-prefix-1" class="gmw-prefix gmw-prefix-1" name="gmw_px" value="pt" />
									<input type="hidden" id="gmw-action-1" class="gmw-action gmw-action-1" name="action" value="gmw_post" />
									<input type="submit" id="gmw-submit-1" class="gmw-submit gmw-submit-1" value="GO &#xf002;" />
								</div>
							</form>
						</div>
					</div>
					<div class="row">
						<div class="form-group mobile-panel-link-container col-xs-12">
							<p><a href="https://spca.bc.ca/about-us/locations/locations-list/">View all locations</a></p>
						</div>
					</div>
				</div>
			</div>
			<!-- END mobile location search  -->
			<!-- BEGIN primary nav -->
			<nav id="navbar" class="primary-nav navbar-collapse collapse navbar-inverse js__navbar" role="navigation" aria-label="Top Menu" aria-expanded="false">
				<div class="container">
					<ul class="nav navbar-nav primary-links">
						<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-326" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-326 dropdown">
							<a title="Adopt" href="<?php echo home_url(); ?>" data-hover="dropdown" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">Adopt <span class="caret"></span></a>
							<ul role="menu" class=" dropdown-menu">
								<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-60" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-60">
									<a title="Adoptable animals" href="<?php echo home_url(); ?>">Adoptable animals</a>
								</li>
								<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-59" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-59">
									<a title="How to adopt" href="https://www.spca.bc.ca/adoption/how-to-adopt/">How to adopt</a>
								</li>
								<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-574" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-574">
									<a title="Adoption fees" href="https://www.spca.bc.ca/adoption/adoption-fees-2/">Adoption fees</a>
								</li>
								<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-325" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-325">
									<a title="Adoption stories" href="https://www.spca.bc.ca/?post_type=adoption_stories">Adoption stories</a>
								</li>
								<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-265" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-265">
									<a title="Submit your adoption story" href="https://www.spca.bc.ca/submit-your-adoption-story/">Submit your adoption story</a>
								</li>
							</ul>
						</li>
						<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-745" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-745 dropdown">
							<a title="Donate" href="https://spca.bc.ca/donations/make-a-donation/?utm_source=menu&utm_medium=link&utm_campaign=donate" data-hover="dropdown" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">Donate <span class="caret"></span></a>
							<ul role="menu" class=" dropdown-menu">
								<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-542" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-542">
									<a title="Make a gift" href="https://spca.bc.ca/donations/make-a-donation/?utm_source=menu&utm_medium=link&utm_campaign=donate">Make a gift</a>
								</li>
								<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-543" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-543">
									<a title="Become a monthly donor" href="https://spca.bc.ca/donations/make-a-donation/?utm_source=menu&utm_medium=link&utm_campaign=donate">Become a monthly donor</a>
								</li>
								<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-544" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-544">
									<a title="Get a copy of your tax receipt" href="http://support.spca.bc.ca/site/ReceiptRequest?utm_source=menu&utm_medium=link&utm_campaign=tax-receipt">Get a copy of your tax receipt</a>
								</li>
								<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-545" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-545">
									<a title="Events &#038; fundraisers" href="https://www.spca.bc.ca/ways-to-help/events-fundraisers/">Events &#038; fundraisers</a>
								</li>
								<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-546" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-546">
									<a title="Medical emergency" href="https://secure3.convio.net/bcspca/site/SPageNavigator/MedicalEmergency/medicalEmergency_landing.html">Medical emergency</a>
								</li>
								<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-546" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-546">
									<a title="Set up or find a memorial page" href="http://support.spca.bc.ca/site/TR?fr_id=2000&amp;pg=entry">Set up or find a memorial page</a>								</li>								
								<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-549" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-549">
									<a title="Host your own fundraiser" href="https://champions.spca.bc.ca/?utm_source=menu&utm_medium=link&utm_campaign=champions">Host your own fundraiser</a>
								</li>
								<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-550" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-550">
									<a title="Leave money in your will" href="https://www.spca.bc.ca/leave-money-in-your-will/">Leave money in your will</a>
								</li>
								<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-550" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-550">
									<a title="Businesses that Give Back" href="https://spca.bc.ca/donate/other-ways-to-give/corporate-partners/">Businesses that Give Back</a>								</li>								
								<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-553" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-553">
									<a title="Other ways to give" href="https://www.spca.bc.ca/other-ways-to-give/">Other ways to give</a>
								</li>
							</ul>
						</li>
						<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-63" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-63">
							<a title="I Need Help With..." href="https://www.spca.bc.ca/i-need-help-with/">I Need Help With&#8230;</a>
						</li>
						<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-70" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-70 dropdown">
							<a title="Programs &amp; Services" href="https://www.spca.bc.ca/programs-services/" data-hover="dropdown" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">Programs &#038; Services <span class="caret"></span></a>
							<ul role="menu" class=" dropdown-menu">
								<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-434" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-434">
									<a title="Cruelty investigations" href="https://www.spca.bc.ca/programs-services/cruelty-investigations/">Cruelty investigations</a>
								</li>
								<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-438" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-438">
									<a title="Leaders in our field" href="https://www.spca.bc.ca/programs-services/leaders-in-our-field/">Leaders in our field</a>
								</li>
								<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-447" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-447">
									<a title="Working for better laws" href="https://www.spca.bc.ca/programs-services/working-for-better-laws/">Working for better laws</a>
								</li>
								<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-433" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-433">
									<a title="Community work" href="https://www.spca.bc.ca/programs-services/community-work/">Community work</a>
								</li>
								<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-442" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-442">
									<a title="Shelter services" href="https://www.spca.bc.ca/programs-services/shelter-services/">Shelter services</a>
								</li>
								<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-445" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-445">
									<a title="Veterinary services" href="https://www.spca.bc.ca/programs-services/veterinary-services/">Veterinary services</a>
								</li>
								<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-431" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-431">
									<a title="Certifications &#038; Accreditation" href="https://www.spca.bc.ca/programs-services/certifications-accreditation/">Certifications &#038; Accreditation</a>
								</li>
								<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-435" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-435">
									<a title="Farm programs" href="https://www.spca.bc.ca/programs-services/farm-programs/">Farm programs</a>
								</li>
								<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-446" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-446">
									<a title="Wildlife rehabilitation" href="https://www.spca.bc.ca/programs-services/wildlife-rehabilitation/">Wildlife rehabilitation</a>
								</li>
								<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-441" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-441">
									<a title="Pet identification registry" href="https://www.spca.bc.ca/programs-services/pet-identification-registry/">Pet identification registry</a>
								</li>
								<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-437" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-437">
									<a title="For kids &amp; teens" href="https://www.spca.bc.ca/programs-services/for-kids-teens/">For kids &amp; teens</a>
								</li>
							</ul>
						</li>
						<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-106" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-106 dropdown">
							<a title="Ways to Help" href="https://www.spca.bc.ca/ways-to-help/" data-hover="dropdown" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">Ways to Help <span class="caret"></span></a>
							<ul role="menu" class=" dropdown-menu">
								<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-502" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-502">
									<a title="Events &#038; fundraisers" href="https://www.spca.bc.ca/ways-to-help/events-fundraisers/">Events &#038; fundraisers</a>
								</li>
								<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-504" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-504">
									<a title="Take action" href="https://www.spca.bc.ca/ways-to-help/take-action/">Take action</a>
								</li>
								<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-505" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-505">
									<a title="Volunteer" href="https://www.spca.bc.ca/ways-to-help/volunteer/">Volunteer</a>
								</li>
								<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-506" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-506">
									<a title="Become a member" href="https://www.spca.bc.ca/ways-to-help/become-a-member/">Become a member</a>
								</li>
								<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-507" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-507">
									<a title="Get updates" href="https://www.spca.bc.ca/ways-to-help/get-updates/">Get updates</a>
								</li>
							</ul>
						</li>
						<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-323" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-323 dropdown">
							<a title="News &amp; Events" href="https://www.spca.bc.ca/?post_type=news" data-hover="dropdown" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">News &#038; Events <span class="caret"></span></a>
							<ul role="menu" class=" dropdown-menu">
								<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-798" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-798">
									<a title="News" href="https://www.spca.bc.ca/news/">News</a>
								</li>
								<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-570" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-570">
									<a title="Events" href="https://www.spca.bc.ca/ways-to-help/events-fundraisers/">Events</a>
								</li>
								<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-567" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-567">
									<a title="Check out our eNewsletters" href="https://www.spca.bc.ca/news-events/enewsletters/">Check out our eNewsletters</a>
								</li>
								<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-566" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-566">
									<a title="Publications" href="https://www.spca.bc.ca/news-events/publications/">Publications</a>
								</li>
								<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-565" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-565">
									<a title="Media room" href="https://www.spca.bc.ca/news-events/media-room/">Media room</a>
								</li>
								<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-571" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-571">
									<a title="Get updates" href="https://www.spca.bc.ca/ways-to-help/get-updates/">Get updates</a>
								</li>
							</ul>
						</li>
						<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-292" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-292">
							<a title="Shop &amp; Gifts" href="https://shop.spca.bc.ca">Shop</a>
							<ul role="menu" class=" dropdown-menu">
								<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-43197" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-43197">
									<a title="Shop" href="https://shop.spca.bc.ca/collections/frontpage">Shop</a>
								</li>
								<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-43198" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-43198">
									<a title="Gift Catalogue" href="https://shop.spca.bc.ca/pages/gift_catalogue">Gift Catalogue</a>
								</li>
								<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-65010" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-65010">
									<a title="Adopt a Kennel" href="https://shop.spca.bc.ca/pages/adopt-a-kennel?utm_source=menu&amp;utm_medium=link&amp;utm_campaign=adoptakennel&amp;_ga=2.94652255.2143607417.1579099140-1617354469.1579099140">Adopt a Kennel</a>
								</li>
							</ul>
						</li>
					</ul>
				</div>

				<ul class="nav-helper-links nav navbar-nav visible-xs">
					<li class="mobile-bottom-link">
						<a class="collapsed" role="button" data-toggle="collapse" href="#mobileContactPanel" aria-expanded="false" aria-controls="mobileContactPanel">
							<i class="fa fa-phone"></i> Animal Cruelty Hotline
						</a>
					</li>
					<li class="mobile-bottom-link">
						<a class="collapsed" role="button" data-toggle="collapse" href="#mobileSiteSearchPanel" aria-expanded="false" aria-controls="mobileSiteSearchPanel">
							<i class="fa fa-search"></i> Search the Site
						</a>
					</li>
					<li class="mobile-bottom-link">
						<a class="collapsed" role="button" data-toggle="collapse" href="#mobileLocationSearchPanel" aria-expanded="false" aria-controls="mobileLocationSearchPanel">
							<i class="fa fa-map-marker"></i> Find a Location
						</a>
					</li>
				</ul>

			</nav>
			<!-- END primary nav -->
		</div>

		<!-- light blue favourites bar -->
		<?php if ( is_singular() || is_home() || is_404()) { ?>
			<?php get_template_part('partials/home', 'favourites-bar'); ?>
		<?php } else { ?>
		<?php get_template_part('partials/favourites', 'bar'); ?>
		<?php } ?>

		<div id="content" class="site-content">