<?php

/**
 * Get remote branch locations to be able to populate shelterbuddy locations with hours data
 *
 * @return Object	An object with an array of WP Post objects
 */
// delete_transient('location_posts');

function get_remote_locations(){
	if ( false === ( $location_posts = get_transient( 'location_posts' ) ) ) {
		$location_posts = Ybd_Shelterbuddy_Public::ybd_get_posts_via_rest( 'https://spca.bc.ca/wp-json/wp/v2/locations?per_page=50' );
		set_transient( 'location_posts', $location_posts, mt_rand(12, 48) * HOUR_IN_SECONDS );
	}

	return $location_posts;
}

/**
 * Given a string, try to match up the correct remote location from the main website with the location in shelterbuddy to provide hours data
 *
 * @param String 	$slug	A slug consisting of a location name (lowercased, dashed)
 * @return Array			An array of hours data if any, false if not
 */
function get_remote_location_data($slug){

	$location_posts = get_remote_locations();

	foreach( $location_posts as $location) {
		// ybd_debug_log($location->slug);
		if ( strpos( $slug, $location->slug ) !== false ) {
			// ybd_debug_log($location->post_meta_fields->hours);
			return $location->post_meta_fields->hours;
		}
	}

	return false;
}

/**
 * Get remote event posts and store them for up to 36 hours in a transient
 *
 * @return Object	An object with an array of WP Post objects
 */
// delete_transient('event_posts');
function get_remote_event_posts(){
	if ( false === ( $event_posts = get_transient( 'event_posts' ) ) ) {
		$event_posts = Ybd_Shelterbuddy_Public::ybd_get_posts_via_rest( 'https://spca.bc.ca/wp-json/wp/v2/event?per_page=90' );
		set_transient( 'event_posts', $event_posts, mt_rand(12, 48) * HOUR_IN_SECONDS );
	}

	foreach( $event_posts as $key => $value) {
		// If the post doesn't have a featured image, remove it
		if ( empty($value->featured_image) ) {
			unset($event_posts[$key]);
		}
	}

	return $event_posts;
}

/**
 * Get remote FAQ posts and store them for x hours in a transient
 *
 * @return Object	An object with an array of WP Post objects
 */
// delete_transient('faq_posts');
function get_remote_faq_posts(){
	if ( false === ( $faq_posts = get_transient( 'faq_posts' ) ) ) {
		$faq_posts = Ybd_Shelterbuddy_Public::ybd_get_posts_via_rest( 'https://spca.bc.ca/wp-json/wp/v2/ufaq?per_page=100&tags_exclude=31027&categories=95,13765,96,3' );
		foreach( $faq_posts as $post ){
			$colours = array('yellow', 'blue', 'green');
			$num = rand(0,2);
			$post->promo_color = $colours[$num];	
		}
		set_transient( 'faq_posts', $faq_posts, mt_rand(12, 48) * HOUR_IN_SECONDS );
	}
	// ybd_debug_log($faq_posts[0]);
	return $faq_posts;
}

/**
 * Gets a random FAQ post, hopefully from transient if it exists, and filter it by a term (pet_type) if passed in
 *
 * @param [type] $terms
 * @return void
 */
function get_random_faq_post($terms = null){

	$posts = get_remote_faq_posts();

	if ( $terms != null ) {
		foreach ( $posts as $key => $value ) {
			$tag_ids = $value->{'ufaq-tag'};

			switch($terms[0]->slug){
				case 'cat':
					if ( in_array(217, $tag_ids) ) {
						break;
					} else {
						unset($posts[$key]);
					}
				case 'dog':
					if ( in_array(218, $tag_ids) ) {
						break;
					} else {
						unset($posts[$key]);
					}
				case 'small-animal':
				case 'small-animal-exotic':
						if ( in_array(31028, $tag_ids) ) {
						break;
					} else {
						unset($posts[$key]);
					}
				case 'horse':
					if ( in_array(5461, $tag_ids) ) {
						break;
					} else {
						unset($posts[$key]);
					}
				case 'farm-animal':
					if ( in_array(5463, $tag_ids) ) {
						break;
					} else {
						unset($posts[$key]);
					}
				case 'rabbit':
					if ( in_array(255, $tag_ids) ) {
						break;
					} else {
						unset($posts[$key]);
					}
				case 'bird':
				case 'bird-exotic':
						if ( in_array(31029, $tag_ids) ) {
						break;
					} else {
						unset($posts[$key]);
					}
					default:
					break;
			}
		}

		// re-index the array
		$posts = array_values($posts);
	}

	$random = 0;
	$length = count( $posts );
	if ( $length > 0 ) {
		$random = mt_rand( 0, $length - 1 );
	}

	$post = $posts[$random];

	return $post;
	
}

/**
 * Get remote promo posts and store them for up to 36 hours in a transient
 *
 * @return Object	An object with an array of WP Post objects
 */
// delete_transient('promo_posts');
function get_remote_promo_posts(){
	if ( false === ( $promo_posts = get_transient( 'promo_posts' ) ) ) {
		$promo_posts = Ybd_Shelterbuddy_Public::ybd_get_posts_via_rest( 'https://spca.bc.ca/wp-json/wp/v2/promos?per_page=90' );
		foreach( $promo_posts as $key => $value ) {
			// Remove all promos that aren't sitewide or homepage
			if ( strpos( $value->title->rendered, 'HOMEPAGE' ) === false && strpos( $value->title->rendered, 'SITEWIDE' ) === false ) {
				unset($promo_posts[$key]);
			}
			// Remove no-show-adopt
			if ( in_array( 31119, $value->tags ) ) {
				unset($promo_posts[$key]);
			}
		}
		$promo_posts = array_values($promo_posts);
		set_transient( 'promo_posts', $promo_posts, mt_rand(12, 48) * HOUR_IN_SECONDS );
	}

	// ybd_debug_log($promo_posts);
	return $promo_posts;
}

/**
 * Gets a random promo post and its promo image, hopefully from transient if it exists
 *
 * @return Object	A WP Post object
 */
function get_random_promo_post(){
	$posts = get_remote_promo_posts();

	$length = count( $posts );
	$random = mt_rand( 0, $length - 1 );

	$post = $posts[$random];
	
	// delete_transient('promo_post_img-' . $post->id);
	if ( false === ( $img_response = get_transient( 'promo_post_img-' . $post->id ) ) ) {
		$img_response = Ybd_Shelterbuddy_Public::ybd_get_posts_via_rest( 'https://spca.bc.ca/wp-json/wp/v2/media/' . $post->post_meta_fields->promo_image[0] );
		if ( !empty( $img_response ) ) {
			set_transient( 'promo_post_img-' . $post->id, $img_response, mt_rand(24, 72) * HOUR_IN_SECONDS );
		}
	}
	if ( !empty( $img_response ) ) {
		$post->img = $img_response->source_url;
	}

	return $post;
}

/**
 * Get a random medical emergency post
 *
 * @return void
 */
function get_medical_posts(){
	$args = array(
		'post_status'		=> 'publish',
		'post_type'			=> 'medical_emergency',
		'posts_per_page'	=> 30
	);

	$query = new WP_Query( $args );

	if (! empty( $query->posts ) ) {
		return $query->posts;
	}

	return false;
}

function get_random_medical_post(){
	$posts = get_medical_posts();

	if ( !empty( $posts ) ){
		$length = count( $posts );
		$random = mt_rand( 0, $length - 1 );

		$post = $posts[$random];

		return $post;
	}
	return false;
}

/**
 * Fetches 8 remote news posts and thier images and sets a transient for up to 48 hours before having to re-fetch them
 *
 * @return Object	An object with an array of post objects
 */
// delete_transient('news_posts');
function get_remote_news_posts($type = null){
	if ( false === ( $news_posts = get_transient( 'news_posts' ) ) ) {
		$news_posts = Ybd_Shelterbuddy_Public::ybd_get_posts_via_rest( 'https://spca.bc.ca/wp-json/wp/v2/news?per_page=90' );

		foreach( $news_posts as $key => $value) {
			// If the post doesn't have a featured image, remove it
			if ( empty($value->featured_image) ) {
				unset($news_posts[$key]);
			}
			// generate random color for card
			$colours = array('yellow', 'blue', 'green');
			$colour = array_rand($colours, 1);
			$value->colour = $colours[$colour];
			
		}

		// Re-index the array
		$news_posts = array_values($news_posts);

		// Save the array
		set_transient( 'news_posts', $news_posts, mt_rand(12, 48) * HOUR_IN_SECONDS );
	}

	// If we're on a pet profile page, filter the news posts that are tagged with the pet type
	if ( $type != null ) {
		// cat 7682
		// dog 7705
		// rabbit 8102
		// small-animal 30453
		// farm-animal 16528
		// bird	7674
		foreach ( $news_posts as $key => $value ) {
			$tag_ids = $value->{'tags'};
			// ybd_debug_log($tag_ids);
			switch($type){
				case 'cat':
					if ( in_array(7682, $tag_ids) ) {
						array_unshift($news_posts, $news_posts[$key]);
						break;
					}
				case 'dog':
					if ( in_array(7705, $tag_ids) ) {
						array_unshift($news_posts, $news_posts[$key]);
						break;
					}
				case 'rabbit':
					if ( in_array(8102, $tag_ids) ) {
						array_unshift($news_posts, $news_posts[$key]);
						break;
					}
				case 'small-animal':
				case 'small-animal-exotic':
					if ( in_array(30453, $tag_ids) ) {
						array_unshift($news_posts, $news_posts[$key]);
						break;
					}
				case 'farm-animal':
				case 'horse':
					if ( in_array(16528, $tag_ids) ) {
						array_unshift($news_posts, $news_posts[$key]);
						break;
					}
				case 'bird-exotic':
					if ( in_array(7674, $tag_ids) ) {
						array_unshift($news_posts, $news_posts[$key]);
						break;
					}
				default:
					break;
			}
		}

		// re-index the array
		$news_posts = array_values($news_posts);
	}

	return $news_posts;
}

function get_news_events_posts(){
	$news_posts = get_remote_news_posts();

	shuffle($news_posts);
	$news_posts = array_slice($news_posts, 0, 12);

	return $news_posts;
}