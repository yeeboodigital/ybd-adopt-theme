<?php 

get_header(); 

// ybd_retro_update_saved_searches();
?>

<!-- User is not logged in -->
<?php if ( !is_user_logged_in() ) { ?>

	<div class="ybd-login-wrap">
		<h2>Login or Register for An Account</h2>
		<?php login_with_ajax(); ?>
	</div>

<!-- We expect users to be logged in viewing this page -->
<?php } else {
	
	// Dev only: if the current user is an admin, they can manually trigger emails or pet syncing
	if ( current_user_can('administrator') ) {
		if ( isset( $_GET['perform_sync'] ) && $_GET['perform_sync'] == 1 ){
			do_action('ybd_background_sync');
		} 

		if ( isset( $_GET['send_alerts'] ) && $_GET['send_alerts'] == 1 ){
			do_action('ybd_background_search_alerts');
		}
	}
	
	$user_loved_animals = get_user_meta($current_user->ID, 'ybd_user_loves', true);
	
	if ( empty($user_loved_animals) ) {
		$user_loved_animals = array(0);
	}

	$args = array(
		'post_type' 		=> 'pets',
		'order' 			=> 'ASC',
		'posts_per_page' 	=> -1,
		'post__in'			=> $user_loved_animals
	);
	
	$query = new WP_Query( $args );

	$current_user = wp_get_current_user();

	// Get the user's info for display on the page
	$user_info = get_userdata($current_user->ID);

	$user_meta = get_user_meta($current_user->ID);

	$luminate_status = 0;

	if ( !isset($_GET['updated_profile']) && !empty($user_meta['_base_user_luminate_is_subscribed'][0]) && $user_meta['_base_user_luminate_is_subscribed'][0] == true ) {
		$luminate_status = 1;
	}

	// ybd_debug_log($user_meta);

	// Generate a nonce for use with updating user data
	$ybd_add_meta_nonce = wp_create_nonce( 'ybd_add_user_meta_form_nonce' );
?>

<br /><br />

	<div class="container ybd-profile-container">
		<div class="row">
			<div class="col-xs-12 col-md-6">
				<div class="ybd-speech-bubble">
					<h2 class="ybd-sb-h2">	
						<?php if (!empty($user_info->first_name)) { ?>
							Welcome back, <?php echo $user_info->first_name . '!'; ?>
						<?php } else { ?>
							Welcome back!
						<?php } ?>
					</h2>
				</div>
			</div>
		</div>
	</div>

	<div class="container">
		<div class="row">
			<div class="col-xs-12 text-center">
				<h2 class="ybd-sb-h2">My Information</h2>
			</div>
			<div class="col-xs-12 text-center">
				<form action="<?php echo esc_url( admin_url( 'admin-post.php' ) ); ?>" method="post" id="ybd_user_meta_form">
					<div class="row row-no-gutters">
						<div class="input-group input-group-lg center-block col-xs-12">
							<input type="hidden" name="action" value="ybd_user_meta_update">
							<input type="hidden" name="ybd_add_user_meta_nonce" value="<?php echo $ybd_add_meta_nonce ?>" />
							
							<div class="col-xs-6 col-md-3">
								<input name="ybd_profile_first_name" value="<?php echo $user_info->first_name; ?>"  type="text" class="form-control" placeholder="First Name" aria-describedby="sizing-addon1">
							</div>
							<div class="col-xs-6 col-md-3">
								<input name="ybd_profile_last_name" value="<?php echo $user_info->last_name; ?>"  type="text" class="form-control" placeholder="Last Name" aria-describedby="sizing-addon1">
							</div>
							<div class="col-xs-6 col-md-3">
								<input name="ybd_profile_email" data-luminate-status="<?php echo $luminate_status; ?>" value="<?php echo $user_info->user_email; ?>"  type="text" class="form-control" placeholder="Email Address" aria-describedby="sizing-addon1">
							</div>
							<div class="col-xs-6 col-md-3">
								<a href="#" class="edit-location location-text" data-toggle="modal" data-target="#map-edit-modal" data-lat="" data-lng="">
									<!-- javascript text -->
								</a>
							</div>
							<div class="col-xs-12 text-center">
								<button class="btn btn-green" style="margin-top:15px;" type="submit">Update</button>
							</div>
						</div>
					<div>
				</form>
			</div>
		</div>
	</div>

	<div class="container">
		<div class="row">
			<div class="col-xs-12 text-center">
				<br /><br /><br />
				<h2 class="ybd-sb-h2">My Favourite Animals</h2>
			</div>
			<div class="col-xs-12">
				<?php if ( $query->have_posts() ) { ?>
					<div class="ybd-profile-carousel dale-carousel owl-carousel owl-theme">

						<?php while ( $query->have_posts() ) : $query->the_post();
							
							echo '<div>';
						
							get_template_part('partials/pet', 'card');
							
							echo '</div>';
							endwhile;
							wp_reset_postdata();
						?>
					</div>
				<?php } ?>
			</div>

			<div class="col-xs-12 ybd-no-animals text-center" style="display:none;">
					<h5>You have no favourite animals.</h5>
			</div>
		</div>
	</div>

	<br /><br /><br />

	<div class="jumbotron" id="ybd-saved-searches">
		<div class="container-fluid ybd-saved-searches">
			<div class="row">
				<div class="col-xs-12 text-center">
					<h2 class="ybd-sb-h2">My Email Alerts</h2>
					<p>To edit an email alert, delete the alert and set up a new one.</p>
					<br />
				</div>
				
				<div class="col-xs-12">
					<?php 
					$prefix = '_base_';
					$entries = get_user_meta(get_current_user_id(), $prefix . 'search_group', true);
					// If we have previously saved searches, display them.
					echo '<ul class="list-group">';
					if (!empty($entries)) {
						foreach( (array) $entries as $key => $entry ) {
							$details = $entry[$prefix . 'details'];
							if ( empty( $details ) ) {
								$details = '';
							}

							echo '<li class="list-group-item">';
							echo '<a href="#" class="ybd-delete-search pull-right" data-key="' .$key . '"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Delete</a>';
							echo '<h4><a href="' . $entry[ $prefix . 'pretty_url'] . '">' . $entry[ $prefix . 'title' ] . '</a><span>' . $details . '</span></h4>';
							echo '</li>';
						} //* end foreach;
					} 		
					echo '</ul>';
					?>
					<br />
				</div>
					<div class="text-center">
						<button type="button" class="btn btn-green" data-toggle="modal" data-target="#saved-search-modal">New Email Alert</button>
					</div>
				</div>
			</div>
		</div>
	</div>


	<br /><br /><br />

<!-- The closest shelter for this user (populated by ajax) -->
	<div class="container ybd-closest-location" style="display:none">
		<div class="row">
			<div class="col-xs-12 text-center">
				<h2 class="ybd-sb-h2">Location Nearest You</h2>
			</div>
			<div class="col-xs-12 col-md-6">
				<h4><strong><a class="nearest-link" href="#loading"><!--ajax--></a></strong> <span class="nearest-distance"><!--ajax--></span></h4>
				<p class="nearest-address"></p>
				<p class="nearest-city"><!--ajax--></p>
				<p class="nearest-postal"><!--ajax--></p>
				<br />
				<p class="nearest-tel"><a href="#"><!--ajax--></a></p>
				<p class="nearest-email"><a href="#"><!--ajax--></a></p>
			</div>
			<div class="col-xs-12 col-md-6 map">
				<iframe class="nearest-map" src="" width="100%" height="250" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 text-center">
			<br />
				<a href="https://spca.bc.ca/about-us/locations/locations-list/" class="btn btn-green">See Other Locations</a>
			</div>
		</div>
	</div>

	<br /><br /><br /><br />

	<div class="container nearby-container" style="display:none">
		<div class="row" id="other-pets-nearby">
			<div class="col-xs-12 text-center">
				<h2 class="ybd-sb-h2">Animals Near Me</h2>
			</div>
			<div class="col-xs-12">
				<div class="ybd-nearby-carousel dale-carousel owl-carousel owl-theme">
					<!-- ajax populated -->
				</div>
			</div>
		</div>
	</div>

	<br /><br />

	<div class="row" id="learn-more">
		<div class="col-xs-12 text-center">
			<h2 class="ybd-sb-h2">Learn More</h2>
		</div>
		<div class="col-xs-12">
			<div class="ybd-learn-more-carousel dale-carousel owl-carousel owl-theme">
				<?php 
					for ( $x = 0; $x <= 8; $x++) { 
						get_template_part('partials/faq', 'card'); 
					}
				?>
			</div>
		</div>
	</div>

	<br /><br />

	<div class="row" id="news-events">
		<div class="col-xs-12 text-center">
			<h2 class="ybd-sb-h2">News & Events</h2>
		</div>
		<div class="col-xs-12">
			<div class="ybd-news-events-carousel dale-carousel owl-carousel owl-theme">
				<?php get_template_part('partials/news', 'events'); ?>
			</div>
		</div>
	</div>

	<br /><br />

	<div class="container">
		<div class="col-xs-12 text-center">
			<a href="#" class="delete-account" data-toggle="modal" data-target="#delete-account-modal">Delete my pet search profile</a>
		</div>
	</div>

<?php } ?>

<br /><br /><br />


<?php get_template_part('partials/modal', 'delete-account'); ?>

<?php get_footer(); ?>